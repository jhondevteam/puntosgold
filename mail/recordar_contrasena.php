<?php
/**
 * User: Andres
 * Date: 18/02/16
 * Time: 01:57 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Usuario
 * @var $empresa \app\models\EmpresaTelecliente
 * @var $config \app\models\Empresa
 */

use yii\helpers\Url;

?>
<tr>
  <td align="center" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                              Hola <?= strtoupper($model->nombres) ?></h3>

                            <div
                                style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                              <table>
                                <thead>
                                <tr>
                                  <td colspan="2">Has Solicitado recordar la contraseña en <?= Yii::$app->name ?>.</td>
                                </tr>
                                <tr>
                                  <td colspan="2"> A continuación le enviamos un link para que recuperes tu
                                    contraseña:
                                  </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                  <td colspan="2">
                                    <?= \yii\bootstrap\Html::a('Recuperar', Url::to(['/site/restarpass', 'token' => $model->password_reset_token], true),
                                      ['class' => 'buttonContent']) ?>
                                  </td>
                                </tr>
                                <p>Gracias por preferirnos.</p>
                                </tbody>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
