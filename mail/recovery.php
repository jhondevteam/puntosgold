<?php
/**
 * User: Andres
 * Date: 18/02/16
 * Time: 01:57 PM
 * @var $this \yii\web\View
 * @var $usuario \app\models\Usuario
 */
?>
<tr>
    <td align="center" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
            <tr>
                <td align="center" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                        <tr>
                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td valign="top" class="textContent">
                                                        <h3 mc:edit="header"
                                                            style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                                                            Este correo electrónico ha sido enviado por solicitud de
                                                            recuperación de contraseña del usuario <?= $usuario->nombres ?> </h3>
                                                        <br>
                                                        <div mc:edit="body"
                                                             style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                    Por favor ingrese a la sigiente url:
                                                            <br><br>
                                                    <?= \yii\helpers\Html::a('RECUPERAR CONTRASEÑA',\yii\helpers\Url::to(['site/restarpass',
                                                      'token'=>$usuario->password_reset_token],true)) ?> <br><br>
                                                            </p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>
