<?php
/**
 * User: Andres
 * Date: 18/02/16
 * Time: 01:57 PM
 * @var $this \yii\web\View
 * @var $model \app\models\ContactForm
 */
?>
<tr>
  <td align="center" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">

                            <div mc:edit="body"
                                 style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                             CORREO ENVIADO POR SOLICITUD DEL USUARIO:
                              <?= $model->name ?> <br>
                              <?= $model->email ?> <br>
                              <?= $model->subject ?> <br>
                              <?= $model->body ?>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
