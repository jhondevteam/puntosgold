<?php
/**
 * @var $accionista \app\models\Accionista
 */
?>

<tr>
  <td align="center" valign="top">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#F8F8F8">
      <tr>
        <td align="center" valign="top">
          <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
            <tr>
              <td align="center" valign="top" width="500" class="flexibleContainerCell">
                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                  <tr>
                    <td align="center" valign="top">
                      <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          <td valign="top" class="textContent">
                            <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">
                              Hola <?= strtoupper($accionista->nombres) ?></h3>

                            <div
                                style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                              <table>
                                <thead>
                                <tr>
                                  <td colspan="2">Has realizado el pago en PidCar.</td>
                                </tr>
                                <tr>
                                  <td colspan="2"> Aqui te enviamos tu certificado de sociedad, puedes escanear el
                                    codigo QR de la
                                    tarjeta adjunta para comprobar en nuestra pagina tu pago y registro.
                                  </td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                  <td colspan="2">
                                    <p>Gracias por preferirnos.</p>
                                  </td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
</tr>
