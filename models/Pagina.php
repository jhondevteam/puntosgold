<?php

namespace app\models;

use app\models\base\Pagina as BasePagina;

/**
 * This is the model class for table "Pagina".
 */
class Pagina extends BasePagina
{
  const TERMINOS_AFILIADOS = "terminos-afiliados";
  const INFO_BANCARIA = "info-bancaria";
  const TERMINO_OFERTA = "terminos-oferta-10";
  const TERMINOS_TAXISTA = 'app-driver';
  const TERMINOS_APP_TAXI = 'app-rider';
  const TERMINOS_OFERTA_PASAJEROS = 'terms-oferta-rider';

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['glue', 'titulo', 'contenido'], 'required'],
        [['contenido'], 'string'],
        [['glue'], 'string', 'max' => 20],
        [['titulo'], 'string', 'max' => 500]
      ]);
  }

}
