<?php

namespace app\models;

use \app\models\base\DriverCategory as BaseDriverCategory;
use yii\helpers\Url;

/**
 * This is the model class for table "driver_category".
 */
class DriverCategory extends BaseDriverCategory
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['name', 'porcent', 'porcent_affiliate'], 'required'],
        [['porcent', 'porcent_affiliate'], 'number'],
        [['name'], 'string', 'max' => 50],
      ]);
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['icon_map'] = function ($model) {
      return Url::to("@web/$model->icon_map", true);
    };
    $fields['icon'] = function ($model) {
      return Url::to("@web/$model->icon", true);
    };
    return $fields;
  }
}
