<?php

namespace app\models;

use \app\models\base\AfiDatos as BaseAfiDatos;

/**
 * This is the model class for table "afi_datos".
 */
class AfiDatos extends BaseAfiDatos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['afiliado_id', 'nacimiento', 'telefono', 'direccion', 'sexo'], 'required'],
            [['afiliado_id'], 'integer'],
            [['nacimiento'], 'safe'],
            [['telefono', 'direccion'], 'string', 'max' => 100],
            [['sexo'], 'string', 'max' => 10],
            [['afiliado_id'], 'unique']
        ]);
    }
	
}
