<?php

namespace app\models;

/**
 * This is the model class for table "departamento".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $pais_id
 * @property integer $activo
 *
 * @property Ciudad[] ciudades
 */
class Departamento extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'departamento';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['nombre', 'pais_id'], 'required'],
      [['pais_id'], 'integer'],
      [['nombre'], 'string', 'max' => 30],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nombre' => 'Nombre',
      'pais_id' => 'Pais ID',
    ];
  }

  public function getCiudades(){
    return $this->hasMany(Ciudad::className(),['departamento_id'=>'id']);
  }
}
