<?php

namespace app\models;

use yii\base\Model;

class NotificacionAfiliado extends Model
{
 public $titulo;
 public $mensaje;
 public $sumary;
 public $params;
 public $url_image;
 
 public $picture;
 
 public function rules()
 {
  return [
   [['titulo', 'mensaje', 'sumary','url_imagen'], 'required'],
   [['picture'],'safe']
  ];
 }
 
 public function attributeLabels()
 {
  return [
   'titulo' => 'Titulo',
   'mensaje' => 'Contenido',
   'sumary' => 'Pie de página',
   'url_image'=>'Imagen'
  ];
 }
}