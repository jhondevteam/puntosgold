<?php
namespace app\models;

use yii\base\Model;

class Newpass extends Model{
    public $password;
    public $password2;
    public $oldpassword;

    public function rules(){
        return[
            [['password','password2','oldpassword'],'required','on'=>'uptpass'],
            ['oldpassword','valoldpassword','on'=>'uptpass'],
            [['password','password2'],'required'],
            ['password2', 'compare', 'compareAttribute'=>'password', 'message'=>"Las contraseñas no coinciden"],
        ];
    }


    public function valOldPassword(){
        /** @var \app\models\Usuario $user */
        $user = \Yii::$app->user->identity;
        if($user->validatePassword($this->oldpassword)){
            return true;
        }else
            $this->addError('oldpassword','La contraseña no coincide con la contraseña actual');
        return false;
    }

    public function attributeLabels(){
        return[
            'oldpassword'=>'Contraseña anterior',
            'password'=>'Repita contraseña',
            'password2'=>'Nueva contraseña'
        ];
    }
}