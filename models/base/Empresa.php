<?php

namespace app\models\base;

/**
 * This is the base model class for table "empresa".
 *
 * @property integer $id
 * @property integer $admin_zona_id
 * @property integer $categoria_id
 * @property string $nombre
 * @property string $url_logo
 * @property string $descripcion
 * @property string $nit
 * @property integer $estado
 * @property string $mode
 * @property double $porcentaje
 * @property double $porcentaje_afiliado
 * @property integer $puntos
 * @property integer $puntos_afiliado
 *
 * @property \app\models\Empleado[] $empleados
 * @property \app\models\AdminZona $adminZona
 * @property \app\models\CategoriaEmpresa $categoria
 * @property \app\models\EmpresaSucursal[] $empresaSucursals
 * @property \app\models\Foto[] $fotos
 * @property \app\models\PagoEmpresa[] $pagoEmpresas
 * @property Video $videos
 */
class Empresa extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['admin_zona_id', 'categoria_id', 'estado', 'puntos', 'puntos_afiliado'], 'integer'],
      [['categoria_id', 'nombre', 'url_logo', 'descripcion', 'nit', 'estado'], 'required'],
      [['descripcion'], 'string'],
      [['porcentaje', 'porcentaje_afiliado'], 'number'],
      [['nombre'], 'string', 'max' => 250],
      [['url_logo'], 'string', 'max' => 200],
      [['nit'], 'string', 'max' => 100],
      [['mode'], 'string', 'max' => 30]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'empresa';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'admin_zona_id' => 'Administrador asigando',
      'categoria_id' => 'Categoria',
      'nombre' => 'Nombre',
      'url_logo' => 'Url Logo',
      'descripcion' => 'Descripcion',
      'nit' => 'Nit',
      'estado' => 'Activa ?',
      'porcentaje' => 'Porcentaje',
      'porcentaje_afiliado' => 'Porcentaje Afiliado',
      'puntos' => 'Puntos',
      'puntos_afiliado' => 'Puntos Afiliado',
      'mode' => 'Modo de pago',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleados()
  {
    return $this->hasMany(\app\models\Empleado::className(), ['empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAdminZona()
  {
    return $this->hasOne(\app\models\AdminZona::className(), ['id' => 'admin_zona_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategoria()
  {
    return $this->hasOne(\app\models\CategoriaEmpresa::className(), ['id' => 'categoria_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresaSucursals()
  {
    return $this->hasMany(\app\models\EmpresaSucursal::className(), ['empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getFotos()
  {
    return $this->hasMany(\app\models\Foto::className(), ['empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getVideos()
  {
    return $this->hasMany(Video::className(), ['empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPagoEmpresas()
  {
    return $this->hasMany(\app\models\PagoEmpresa::className(), ['empresa_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\EmpresaQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EmpresaQuery(get_called_class());
  }
}
