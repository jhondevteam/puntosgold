<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "driver_category".
 *
 * @property integer $id
 * @property string $name
 * @property double $porcent
 * @property double $porcent_affiliate
 * @property string $icon
 * @property string $icon_map
 *
 * @property \app\models\Driver[] $drivers
 * @property \app\models\DriverCategoryState[] $driverCategoryStates
 */
class DriverCategory extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['name', 'porcent', 'porcent_affiliate', 'icon', 'icon_map'], 'required'],
      [['porcent', 'porcent_affiliate'], 'number'],
      [['name'], 'string', 'max' => 50],
      [['icon', 'icon_map'], 'string', 'max' => 500]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'driver_category';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'name' => 'Name',
      'porcent' => 'Porcent',
      'porcent_affiliate' => 'Porcent Affiliate',
      'icon' => 'Icon',
      'icon_map' => 'Icon Map',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDrivers()
  {
    return $this->hasMany(\app\models\Driver::className(), ['driver_category_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverCategoryStates()
  {
    return $this->hasMany(\app\models\DriverCategoryState::className(), ['driver_category_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\DriverCategoryQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DriverCategoryQuery(get_called_class());
  }
}
