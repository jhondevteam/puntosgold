<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "token_compra".
 *
 * @property integer $id
 * @property integer $compra_afiliado_puntos_id
 * @property integer $afiliado_id
 * @property string $token
 * @property string $fecha
 *
 * @property \app\models\CompraAfiliadoPuntos $compraAfiliadoPuntos
 * @property \app\models\Afiliado $afiliado
 */
class TokenCompra extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['compra_afiliado_puntos_id', 'afiliado_id'], 'integer'],
      [['afiliado_id', 'token', 'fecha'], 'required'],
      [['fecha'], 'safe'],
      [['token'], 'string', 'max' => 20]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'token_compra';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'compra_afiliado_puntos_id' => 'Compra Afiliado Puntos ID',
      'afiliado_id' => 'Afiliado ID',
      'token' => 'Token',
      'fecha' => 'Fecha',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliadoPuntos()
  {
    return $this->hasOne(\app\models\CompraAfiliadoPuntos::className(), ['id' => 'compra_afiliado_puntos_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\TokenCompraQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\TokenCompraQuery(get_called_class());
  }
}
