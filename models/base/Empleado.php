<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "empleado".
 *
 * @property integer $id
 * @property integer $empresa_id
 * @property integer $usuario_id
 *
 * @property \app\models\CobroEmpresaEmpleado[] $cobroEmpresaEmpleados
 * @property \app\models\CompraAfiliado[] $compraAfiliados
 * @property \app\models\CompraAfiliadoPuntos[] $compraAfiliadoPuntos
 * @property \app\models\Empresa $empresa
 * @property \app\models\Usuario $usuario
 */
class Empleado extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empresa_id', 'usuario_id'], 'required'],
            [['empresa_id', 'usuario_id'], 'integer'],
            [['usuario_id'], 'unique']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'empleado';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'usuario_id' => 'Usuario ID',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCobroEmpresaEmpleados()
    {
        return $this->hasMany(\app\models\CobroEmpresaEmpleado::className(), ['empleado_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraAfiliados()
    {
        return $this->hasMany(\app\models\CompraAfiliado::className(), ['empleado_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompraAfiliadoPuntos()
    {
        return $this->hasMany(\app\models\CompraAfiliadoPuntos::className(), ['empleado_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(\app\models\Empresa::className(), ['id' => 'empresa_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\EmpleadoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EmpleadoQuery(get_called_class());
    }
}
