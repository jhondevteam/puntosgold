<?php

namespace app\models\base;

/**
 * This is the base model class for table "Pagina".
 *
 * @property integer $id
 * @property string $glue
 * @property string $titulo
 * @property string $contenido
 */
class Pagina extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'glue', 'titulo', 'contenido'], 'required'],
      [['id'], 'integer'],
      [['contenido'], 'string'],
      [['glue'], 'string', 'max' => 20],
      [['titulo'], 'string', 'max' => 500]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'pagina';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'glue' => 'Glue',
      'titulo' => 'Titulo',
      'contenido' => 'Contenido',
    ];
  }
}
