<?php

namespace app\models\base;

/**
 * This is the base model class for table "compra_afiliado".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property integer $empleado_id
 * @property integer $cobro_empresa_id
 * @property integer $pago_empresa_id
 * @property bool $pago_puntos
 * @property string $fecha
 * @property integer $monto
 * @property integer $monto_cobra_empresa
 * @property integer $puntos
 * @property integer $saldo_puntos
 * @property string $comentario
 *
 * @property \app\models\Afiliado $afiliado
 * @property \app\models\Empleado $empleado
 * @property \app\models\CobroEmpresaEmpleado $cobroEmpresa
 */
class CompraAfiliado extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['afiliado_id', 'empleado_id', 'monto', 'monto_cobra_empresa', 'puntos','pago_puntos'], 'required'],
      [['afiliado_id', 'empleado_id', 'cobro_empresa_id', 'monto', 'monto_cobra_empresa', 'puntos'], 'integer'],
      [['fecha'], 'safe'],
      [['comentario'], 'string', 'max' => 500]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'compra_afiliado';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'afiliado_id' => 'Afiliado',
      'empleado_id' => 'Empleado',
      'cobro_empresa_id' => 'Cobro Empresa',
      'fecha' => 'Fecha',
      'monto' => 'Monto',
      'monto_cobra_empresa' => 'Monto Cobra Empresa',
      'puntos' => 'Puntos',
      'comentario' => 'Comentario',
      'saldo_puntos'=>'Saldo en puntos al comprar'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCobroEmpresa()
  {
    return $this->hasOne(\app\models\CobroEmpresaEmpleado::className(), ['id' => 'cobro_empresa_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\CompraAfiliadoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\CompraAfiliadoQuery(get_called_class());
  }
}
