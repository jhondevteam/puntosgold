<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "driver_category_state".
 *
 * @property integer $id
 * @property integer $driver_category_id
 * @property integer $departamento_id
 * @property double $porcent
 * @property double $porcent_affiliate
 *
 * @property \app\models\DriverCategory $driverCategory
 * @property \app\models\Departamento $departamento
 */
class DriverCategoryState extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['driver_category_id', 'departamento_id', 'porcent', 'porcent_affiliate'], 'required'],
      [['driver_category_id', 'departamento_id'], 'integer'],
      [['porcent', 'porcent_affiliate'], 'number'],
      [['driver_category_id', 'departamento_id'], 'unique', 'targetAttribute' => ['driver_category_id', 'departamento_id'], 'message' => 'The combination of Driver Category ID and Departamento ID has already been taken.']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'driver_category_state';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'driver_category_id' => 'Driver Category ID',
      'departamento_id' => 'Departamento ID',
      'porcent' => 'Porcent',
      'porcent_affiliate' => 'Porcent Affiliate',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverCategory()
  {
    return $this->hasOne(\app\models\DriverCategory::className(), ['id' => 'driver_category_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDepartamento()
  {
    return $this->hasOne(\app\models\Departamento::className(), ['id' => 'departamento_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\DriverCategoryStateQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DriverCategoryStateQuery(get_called_class());
  }
}
