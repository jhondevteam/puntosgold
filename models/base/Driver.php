<?php

namespace app\models\base;

use app\models\Departamento;
use Yii;

/**
 * This is the base model class for table "driver".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $driver_category_id
 * @property integer $credits
 * @property double $porcent
 * @property string $car_info
 * @property string $code
 * @property string $active
 * @property int $driver_invite_id
 * @property int $empresa_transportadora_id
 * @property int $departamento_id
 *
 * @property \app\models\Usuario $usuario
 * @property \app\models\DriverCategory $driverCategory
 * @property \app\models\DriverPurchase[] $driverPurchases
 * @property \app\models\DriverTrip[] $driverTrips
 * @property \app\models\Driver $driverInvite
 * @property Departamento $departamento
 */
class Driver extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'driver';
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverCategory()
  {
    return $this->hasOne(\app\models\DriverCategory::className(), ['id' => 'driver_category_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverPurchases()
  {
    return $this->hasMany(\app\models\DriverPurchase::className(), ['driver_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverTrips()
  {
    return $this->hasMany(\app\models\DriverTrip::className(), ['driver_id' => 'id']);
  }

  public function getDriverInvite()
  {
    return $this->hasOne(\app\models\Driver::class, ['id' => 'driver_invite_id']);
  }

  public function getDepartamento(){
    return $this->hasOne(Departamento::className(), ['id' => 'departamento_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\DriverQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DriverQuery(get_called_class());
  }
}
