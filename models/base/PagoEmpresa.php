<?php

namespace app\models\base;

/**
 * This is the base model class for table "pago_empresa".
 *
 * @property integer $id
 * @property integer $empresa_id
 * @property integer $admin_zona_id
 * @property integer $comision_admin_zona
 * @property string $fecha
 * @property string $fecha_pago
 * @property integer $monto
 * @property integer $pagado
 * @property string $url_file
 *
 * @property \app\models\CompraAfiliado[] $compraAfiliados
 * @property \app\models\Empresa $empresa
 * @property \app\models\AdminZona $adminZona
 */
class PagoEmpresa extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'pago_empresa';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'empresa_id' => 'Empresa',
      'admin_zona_id' => 'Admin Zona',
      'fecha' => 'Fecha',
      'fecha_pago' => 'Fecha Pago',
      'monto' => 'Monto',
      'pagado' => 'Pagado',
      'url_file' => 'Url File',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliados()
  {
    return $this->hasMany(\app\models\CompraAfiliado::className(), ['pago_empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliadoPuntos()
  {
    return $this->hasMany(\app\models\CompraAfiliadoPuntos::className(), ['pago_empresa_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresa()
  {
    return $this->hasOne(\app\models\Empresa::className(), ['id' => 'empresa_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAdminZona()
  {
    return $this->hasOne(\app\models\AdminZona::className(), ['id' => 'admin_zona_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\PagoEmpresaQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\PagoEmpresaQuery(get_called_class());
  }
}
