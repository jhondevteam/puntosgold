<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "foto".
 *
 * @property integer $id
 * @property integer $empresa_id
 * @property string $url_foto
 *
 * @property \app\models\Empresa $empresa
 */
class Foto extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['empresa_id', 'url_foto'], 'required'],
      [['empresa_id'], 'integer'],
      [['url_foto'], 'string', 'max' => 100]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'foto';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'empresa_id' => 'empresa',
      'url_foto' => 'Imagen',
      'file'=>'Imegen'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresa()
  {
    return $this->hasOne(\app\models\Empresa::className(), ['id' => 'empresa_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\FotoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\FotoQuery(get_called_class());
  }
}
