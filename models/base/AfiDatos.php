<?php

namespace app\models\base;

/**
 * This is the base model class for table "afi_datos".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property string $nacimiento
 * @property string $telefono
 * @property string $direccion
 * @property string $sexo
 *
 * @property \app\models\Afiliado $afiliado
 */
class AfiDatos extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['afiliado_id', 'nacimiento', 'telefono', 'direccion', 'sexo'], 'required'],
      [['afiliado_id'], 'integer'],
      [['nacimiento'], 'safe'],
      [['telefono', 'direccion'], 'string', 'max' => 100],
      [['sexo'], 'string', 'max' => 10],
      [['afiliado_id'], 'unique']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'afi_datos';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'afiliado_id' => 'Afiliado',
      'nacimiento' => 'Nacimiento',
      'telefono' => 'Telefono',
      'direccion' => 'Direccion',
      'sexo' => 'Sexo',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\AfiDatosQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\AfiDatosQuery(get_called_class());
  }
}
