<?php

namespace app\models\base;

use app\models\Departamento;
use Yii;
use yii\helpers\Json;

/**
 * This is the base model class for table "empresa_transportadora".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $departamento_ids
 * @property double $porcent
 *
 * @property \app\models\Driver[] $drivers
 */
class EmpresaTransportadora extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
       [['nombre', 'departamento_ids', 'porcent'], 'required'],
       [['porcent'], 'number'],
       [['nombre'], 'string', 'max' => 50],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'empresa_transportadora';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
       'id' => 'ID',
       'nombre' => 'Nombre',
       'departamento_ids' => 'departamento Ids',
       'porcent' => 'Porcent',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDrivers()
  {
    return $this->hasMany(\app\models\Driver::className(), ['empresa_transportadora_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\EmpresaTransportadoraQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EmpresaTransportadoraQuery(get_called_class());
  }

  public function afterFind()
  {
    $this->departamento_ids = Json::decode($this->departamento_ids);
    parent::afterFind();
  }

  public function getDetapartamentos($onlynames = false)
  {
    $q = Departamento::find()
       ->where(['id' => $this->departamento_ids]);
    if ($onlynames) {
      return $q->select('nombre')->createCommand()->queryColumn();
    } else {
      return $q->all();
    }
  }
}
