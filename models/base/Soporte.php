<?php

namespace app\models\base;

use sintret\chat\models\Chat;
use Yii;

/**
 * This is the base model class for table "soporte".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property integer $active
 * @property string $fecha
 *
 * @property Chat[] $chats
 * @property \app\models\Usuario $usuario
 */
class Soporte extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'soporte';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'Numero°',
      'usuario_id' => 'Usuario',
      'active' => "Abierto.",
      'fecha' => 'Fecha de creación',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getChats()
  {
    return $this->hasMany(Chat::className(), ['soporte_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\SoporteQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\SoporteQuery(get_called_class());
  }
}
