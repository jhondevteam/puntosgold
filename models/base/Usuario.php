<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "usuario".
 *
 * @property integer $id
 * @property string $documento
 * @property string $nombres
 * @property string $email
 * @property string $foto
 * @property integer $estado
 * @property string $password
 * @property string $password_reset_token
 * @property string $auth_key
 * @property string $access_token
 * @property string $facebook_id
 *
 * @property \app\models\AdminZona $adminZona
 * @property \app\models\Afiliado $afiliado
 * @property \app\models\Driver $driver
 * @property \app\models\DriverTrip[] $driverTrips
 * @property \app\models\Empleado $empleado
 */
class Usuario extends \yii\db\ActiveRecord
{
  public $picture;
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['documento', 'nombres', 'email', 'password', 'auth_key', 'access_token'], 'required'],
      [['documento'], 'string', 'max' => 100],
      [['nombres'], 'string', 'max' => 250],
      [['email', 'foto'], 'string', 'max' => 200],
      [['estado'], 'string', 'max' => 1],
      [['password', 'password_reset_token', 'auth_key', 'access_token'], 'string', 'max' => 500],
      [['facebook_id'], 'string', 'max' => 150],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'usuario';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'documento' => 'Documento',
      'nombres' => 'Nombres',
      'email' => 'Email',
      'foto' => 'Foto',
      'estado' => 'Estado',
      'password' => 'Contraseña',
      'password_reset_token' => 'Password Reset Token',
      'auth_key' => 'Auth Key',
      'access_token' => 'Access Token',
      'facebook_id' => 'Facebook ID',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAdminZona()
  {
    return $this->hasOne(\app\models\AdminZona::className(), ['usuario_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['usuario_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriver()
  {
    return $this->hasOne(\app\models\Driver::className(), ['usuario_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriverTrips()
  {
    return $this->hasMany(\app\models\DriverTrip::className(), ['usuario_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(\app\models\Empleado::className(), ['usuario_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\UsuarioQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\UsuarioQuery(get_called_class());
  }
}
