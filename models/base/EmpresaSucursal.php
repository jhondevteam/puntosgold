<?php

namespace app\models\base;

/**
 * This is the base model class for table "empresa_sucursal".
 *
 * @property integer $id
 * @property integer $empresa_id
 * @property integer $ciudad_id
 * @property string $direccion
 * @property string $descripcion
 * @property string $lng
 * @property string $lat
 *
 * @property \app\models\Empresa $empresa
 * @property \app\models\Ciudad $ciudad
 */
class EmpresaSucursal extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['empresa_id', 'ciudad_id', 'direccion', 'descripcion', 'lng', 'lat'], 'required'],
      [['empresa_id', 'ciudad_id'], 'integer'],
      [['direccion'], 'string', 'max' => 100],
      [['descripcion'], 'string', 'max' => 500],
      [['lng', 'lat'], 'string', 'max' => 50]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'empresa_sucursal';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'empresa_id' => 'Empresa',
      'ciudad_id' => 'Ciudad',
      'direccion' => 'Direccion',
      'descripcion' => 'Descripcion',
      'lng' => 'Longitud',
      'lat' => 'Latitud',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresa()
  {
    return $this->hasOne(\app\models\Empresa::className(), ['id' => 'empresa_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCiudad()
  {
    return $this->hasOne(\app\models\Ciudad::className(), ['id' => 'ciudad_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\EmpresaSucursalQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\EmpresaSucursalQuery(get_called_class());
  }
}
