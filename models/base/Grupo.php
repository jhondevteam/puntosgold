<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "grupo".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property integer $numero_afiliados
 *
 * @property \app\models\Afiliado[] $afiliados
 * @property \app\models\Afiliado $afiliado
 */
class Grupo extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['afiliado_id', 'numero_afiliados'], 'required'],
            [['afiliado_id', 'numero_afiliados'], 'integer']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grupo';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'afiliado_id' => 'Afiliado ID',
            'numero_afiliados' => 'Numero Afiliados',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfiliados()
    {
        return $this->hasMany(\app\models\Afiliado::className(), ['grupo_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfiliado()
    {
        return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\GrupoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\GrupoQuery(get_called_class());
    }
}
