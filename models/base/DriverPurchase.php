<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "driver_purchase".
 *
 * @property integer $id
 * @property integer $driver_id
 * @property string $date_generate
 * @property string $date_confirm
 * @property string $reference
 * @property integer $is_confirm
 * @property integer $amount
 *
 * @property \app\models\Driver $driver
 */
class DriverPurchase extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['driver_id', 'date_generate', 'reference', 'is_confirm', 'amount'], 'required'],
      [['driver_id', 'is_confirm'], 'integer'],
      [['date_generate', 'date_confirm'], 'safe'],
      [['reference'], 'string', 'max' => 50],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'driver_purchase';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'driver_id' => 'Driver ID',
      'date_generate' => 'Date Generate',
      'date_confirm' => 'Date Confirm',
      'reference' => 'Reference',
      'is_confirm' => 'Is Confirm',
      'amount' => 'Monto'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriver()
  {
    return $this->hasOne(\app\models\Driver::className(), ['id' => 'driver_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\DriverPurchaseQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DriverPurchaseQuery(get_called_class());
  }
}
