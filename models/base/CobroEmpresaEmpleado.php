<?php

namespace app\models\base;

/**
 * This is the base model class for table "cobro_empresa_empleado".
 *
 * @property integer $id
 * @property integer $empleado_id
 * @property string $fecha
 * @property integer $monto_cobrar
 * @property integer $monto
 * @property integer $saldo
 *
 * @property \app\models\Empleado $empleado
 * @property \app\models\CompraAfiliado[] $compraAfiliados
 */
class CobroEmpresaEmpleado extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['empleado_id', 'fecha', 'monto', 'saldo','monto_cobrar'], 'required'],
      [['empleado_id', 'monto', 'saldo'], 'integer'],
      [['fecha'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'cobro_empresa_empleado';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'empleado_id' => 'Empleado ID',
      'fecha' => 'Fecha',
      'monto' => 'Monto',
      'saldo' => 'Saldo',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliados()
  {
    return $this->hasMany(\app\models\CompraAfiliado::className(), ['cobro_empresa_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\CobroEmpresaEmpleadoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\CobroEmpresaEmpleadoQuery(get_called_class());
  }
}
