<?php

namespace app\models\base;

/**
 * This is the base model class for table "admin_zona".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $telefonos
 * @property string $direccion
 * @property integer $zona_id
 * @property float $porcentaje
 *
 * @property \app\models\Usuario $usuario
 * @property \app\models\Zona $zona
 * @property \app\models\Empresa[] $empresas
 */
class AdminZona extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['usuario_id', 'telefonos', 'direccion', 'zona_id','porcentaje'], 'required'],
      [['usuario_id', 'zona_id','porcentaje'], 'integer'],
      [['telefonos', 'direccion'], 'string', 'max' => 200],
      [['usuario_id'], 'unique']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'admin_zona';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'usuario_id' => 'Usuario ID',
      'telefonos' => 'Telefonos',
      'direccion' => 'Direccion',
      'zona_id' => 'Zona ID',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getZona()
  {
    return $this->hasOne(\app\models\Zona::className(), ['id' => 'zona_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresas()
  {
    return $this->hasMany(\app\models\Empresa::className(), ['admin_zona_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\AdminZonaQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\AdminZonaQuery(get_called_class());
  }
}
