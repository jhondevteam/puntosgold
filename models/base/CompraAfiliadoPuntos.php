<?php

namespace app\models\base;

/**
 * This is the base model class for table "compra_afiliado_puntos".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property integer $empleado_id
 * @property integer $pago_empresa_id
 * @property string $fecha
 * @property integer $monto
 * @property integer $puntos
 * @property integer $saldo_puntos
 * @property string $comentario
 * @property boolean $confirmado
 *
 * @property \app\models\Afiliado $afiliado
 * @property \app\models\Empleado $empleado
 * @property \app\models\PagoEmpresa $pagoEmpresa
 */
class CompraAfiliadoPuntos extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['afiliado_id', 'empleado_id', 'monto', 'puntos'], 'required'],
      [['afiliado_id', 'empleado_id', 'pago_empresa_id', 'monto', 'puntos'], 'integer'],
      [['fecha'], 'safe'],
      [['comentario'], 'string', 'max' => 500]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'compra_afiliado_puntos';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'afiliado_id' => 'Afiliado',
      'empleado_id' => 'Empleado',
      'pago_empresa_id' => 'Pago Empresa',
      'fecha' => 'Fecha',
      'monto' => 'Monto',
      'puntos' => 'Puntos',
      'comentario' => 'Comentario',
      'saldo_puntos'=>'Saldo en puntos al comprar'
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpleado()
  {
    return $this->hasOne(\app\models\Empleado::className(), ['id' => 'empleado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPagoEmpresa()
  {
    return $this->hasOne(\app\models\PagoEmpresa::className(), ['id' => 'pago_empresa_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\CompraAfiliadoPuntosQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\CompraAfiliadoPuntosQuery(get_called_class());
  }
}
