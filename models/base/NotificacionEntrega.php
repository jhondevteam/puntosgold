<?php

namespace app\models\base;

/**
 * This is the base model class for table "notificacion_entrega".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property integer $sucursal_id
 * @property string $app_token
 * @property string $resultado
 * @property string $fecha
 *
 * @property \app\models\Afiliado $afiliado
 * @property \app\models\EmpresaSucursal $sucursal
 */
class NotificacionEntrega extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['afiliado_id', 'sucursal_id', 'app_token', 'resultado', 'fecha'], 'required'],
      [['afiliado_id', 'sucursal_id'], 'integer'],
      [['fecha'], 'safe'],
      [['app_token', 'resultado'], 'string', 'max' => 5000]
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'notificacion_entrega';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'afiliado_id' => 'Afiliado ID',
      'sucursal_id' => 'Sucursal ID',
      'app_token' => 'App Token',
      'resultado' => 'Resultado',
      'fecha' => 'Fecha',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiliado()
  {
    return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSucursal()
  {
    return $this->hasOne(\app\models\EmpresaSucursal::className(), ['id' => 'sucursal_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\NotificacionEntregaQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\NotificacionEntregaQuery(get_called_class());
  }
}
