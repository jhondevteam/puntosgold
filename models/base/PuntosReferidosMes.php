<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "puntos_referidos_mes".
 *
 * @property integer $id
 * @property integer $afiliado_id
 * @property string $fecha
 * @property integer $puntos
 * @property integer $pagado
 *
 * @property \app\models\Afiliado $afiliado
 */
class PuntosReferidosMes extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['afiliado_id', 'fecha', 'puntos', 'pagado'], 'required'],
            [['afiliado_id', 'puntos', 'pagado'], 'integer'],
            [['fecha'], 'safe']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'puntos_referidos_mes';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'afiliado_id' => 'Afiliado ID',
            'fecha' => 'Fecha',
            'puntos' => 'Puntos',
            'pagado' => 'Pagado',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAfiliado()
    {
        return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'afiliado_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\PuntosReferidosMesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PuntosReferidosMesQuery(get_called_class());
    }
}
