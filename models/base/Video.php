<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "video".
 *
 * @property integer $id
 * @property integer $descripcion
 * @property integer $empresa_id
 * @property string $url_video
 * @property integer $activo
 *
 * @property \app\models\Empresa $empresa
 */
class Video extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['empresa_id', 'url_video', 'activo','descripcion'], 'required'],
            [['empresa_id', 'activo'], 'integer'],
            [['url_video'], 'string', 'max' => 20]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'empresa_id' => 'Empresa ID',
            'url_video' => 'ID de Youtube',
            'activo' => 'Activo',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa()
    {
        return $this->hasOne(\app\models\Empresa::className(), ['id' => 'empresa_id']);
    }
    }
