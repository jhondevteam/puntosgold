<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "categoria_empresa".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $url_banner
 * @property string $url_icon
 * @property integer $activa
 *
 * @property \app\models\Empresa[] $empresas
 */
class CategoriaEmpresa extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categoria_empresa';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'url_banner' => 'Url Banner',
            'activa' => 'Activa',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(\app\models\Empresa::className(), ['categoria_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\CategoriaEmpresaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CategoriaEmpresaQuery(get_called_class());
    }
}
