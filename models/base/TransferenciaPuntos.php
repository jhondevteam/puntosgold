<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "transferencia_puntos".
 *
 * @property integer $id
 * @property integer $de_afiliado_id
 * @property integer $para_afiliado_id
 * @property integer $monto
 * @property string $fecha
 *
 * @property \app\models\Afiliado $deAfiliado
 * @property \app\models\Afiliado $paraAfiliado
 */
class TransferenciaPuntos extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transferencia_puntos';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'de_afiliado_id' => 'De Afiliado ID',
            'para_afiliado_id' => 'Para Afiliado ID',
            'monto' => 'Monto',
            'fecha' => 'Fecha',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeAfiliado()
    {
        return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'de_afiliado_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParaAfiliado()
    {
        return $this->hasOne(\app\models\Afiliado::className(), ['id' => 'para_afiliado_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\TransferenciaPuntosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TransferenciaPuntosQuery(get_called_class());
    }
}
