<?php

namespace app\models\base;

/**
 * This is the base model class for table "afiliado".
 *
 * @property integer $id
 * @property integer $grupo_id
 * @property integer $usuario_id
 * @property string $fecha_registro
 * @property integer $puntos
 * @property string $code_invitacion
 * @property string $app_token
 * @property string $city
 * @property string $sound
 * @property string $app_version
 * @property boolean $promo
 *
 * @property \app\models\AfiDatos $afiDatos
 * @property \app\models\Grupo $grupo
 * @property \app\models\Usuario $usuario
 * @property \app\models\CompraAfiliado[] $compraAfiliados
 * @property \app\models\Grupo[] $grupos
 * @property \app\models\NotificacionEntrega[] $notificacionEntregas
 * @property \app\models\PuntosAfiliadoMes[] $puntosAfiliadoMes
 * @property \app\models\PuntosReferidosMes[] $puntosReferidosMes
 */
class Afiliado extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['grupo_id', 'usuario_id', 'puntos'], 'integer'],
      [['usuario_id', 'fecha_registro', 'code_invitacion'], 'required'],
      [['fecha_registro'], 'safe'],
      [['code_invitacion'], 'string', 'max' => 6],
      [['app_token'], 'string', 'max' => 5000],
      [['city'], 'string', 'max' => 100],
      [['usuario_id'], 'unique']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'afiliado';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'grupo_id' => 'Grupo ID',
      'usuario_id' => 'Usuario ID',
      'fecha_registro' => 'Fecha Registro',
      'puntos' => 'Puntos',
      'code_invitacion' => 'Code Invitacion',
      'app_token' => 'App Token',
      'city' => 'City',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAfiDatos()
  {
    return $this->hasOne(\app\models\AfiDatos::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getGrupo()
  {
    return $this->hasOne(\app\models\Grupo::className(), ['id' => 'grupo_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliados()
  {
    return $this->hasMany(\app\models\CompraAfiliado::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompraAfiliadoPuntos()
  {
    return $this->hasMany(\app\models\CompraAfiliado::className(), ['afiliado_id' => 'id', 'pago_puntos' => 1]);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getGrupos()
  {
    return $this->hasMany(\app\models\Grupo::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getNotificacionEntregas()
  {
    return $this->hasMany(\app\models\NotificacionEntrega::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPuntosAfiliadoMes()
  {
    return $this->hasMany(\app\models\PuntosAfiliadoMes::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getPuntosReferidosMes()
  {
    return $this->hasMany(\app\models\PuntosReferidosMes::className(), ['afiliado_id' => 'id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\AfiliadoQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\AfiliadoQuery(get_called_class());
  }
}
