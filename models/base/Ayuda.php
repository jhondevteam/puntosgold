<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "ayuda".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $url_video
 * @property string $descripcion
 * @property string $destino
 */
class Ayuda extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'destino'], 'required'],
            [['titulo'], 'string', 'max' => 100],
            [['url_video', 'destino'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 500]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ayuda';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'url_video' => 'Url Video',
            'descripcion' => 'Descripcion',
            'destino' => 'Destino',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\AyudaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AyudaQuery(get_called_class());
    }
}
