<?php

namespace app\models\base;

use Yii;

/**
 * This is the base model class for table "driver_trip".
 *
 * @property integer $id
 * @property integer $driver_id
 * @property integer $usuario_id
 * @property integer $price
 * @property integer $credits
 * @property integer $points_generated
 * @property string $date
 * @property integer $is_pay_points
 *
 * @property \app\models\Driver $driver
 * @property \app\models\Usuario $usuario
 */
class DriverTrip extends \yii\db\ActiveRecord
{
  use \mootensai\relation\RelationTrait;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['driver_id', 'usuario_id', 'price', 'credits', 'points_generated', 'date', 'is_pay_points'], 'required'],
      [['driver_id', 'usuario_id', 'price', 'credits', 'points_generated'], 'integer'],
      [['date'], 'safe']
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'driver_trip';
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'driver_id' => 'Driver ID',
      'usuario_id' => 'Usuario ID',
      'price' => 'Price',
      'credits' => 'Credits',
      'points_generated' => 'Points Generated',
      'date' => 'Date',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDriver()
  {
    return $this->hasOne(\app\models\Driver::className(), ['id' => 'driver_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUsuario()
  {
    return $this->hasOne(\app\models\Usuario::className(), ['id' => 'usuario_id']);
  }

  /**
   * @inheritdoc
   * @return \app\models\DriverTripQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new \app\models\DriverTripQuery(get_called_class());
  }
}
