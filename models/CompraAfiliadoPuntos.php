<?php

namespace app\models;

use app\models\base\CompraAfiliadoPuntos as BaseCompraAfiliadoPuntos;
use yii\helpers\Json;

/**
 * This is the model class for table "compra_afiliado_puntos".
 */
class CompraAfiliadoPuntos extends BaseCompraAfiliadoPuntos
{
  public $runAfterSave = true;
  public $runBeforeValidate = true;

  public function beforeValidate()
  {
    if ($this->runBeforeValidate){
      $this->saldo_puntos = $this->afiliado->puntos;
      if($this->getEmpresa()->mode == Empresa::MODE_PORCENTAJE){
        $this->puntos = ($this->monto * $this->empleado->empresa->porcentaje_afiliado)/2;
      }else{
        $this->puntos = ($this->getEmpresa()->puntos_afiliado)/2;
      }
    }
    return parent::beforeValidate();
  }

  public function afterSave($insert, $changedAttributes)
  {
    if ($this->runAfterSave) {
      $afiliado = $this->afiliado;
      $afiliado->puntos += $this->puntos;
      $afiliado->save(false);
      $padres = $this->afiliado->getPadresAfiliados();
      \Yii::info(Json::encode($padres));
      foreach ($padres as $key => $padreAfiliado) {
        $porcent = floatval('0.10');
        if ($key <= 0) {
          $porcent = floatval('0.20');
        }
        if ($key == 0)
          $puntosDelMes = $padreAfiliado->getCurrentPuntosAfiliadosMes();
        else
          $puntosDelMes = $padreAfiliado->getCurrentPuntosReferidosMes();

        $nuevosPuntos = ($this->puntos*2) * $porcent;
        $puntosDelMes->puntos += $nuevosPuntos;
        $puntosDelMes->save(false);
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['efectivo'] = function (){
      return false;
    };
    return $fields;
  }

  public function extraFields()
  {
    return [
      'empleado'=>'empleado',
      'empresa'=>'empresa',
      'cliente'=>'cliente'
    ];
  }

  public function getEmpresa(){
    return $this->empleado->empresa;
  }

  public function getCliente(){
    return $this->afiliado->usuario->nombres;
  }
}
