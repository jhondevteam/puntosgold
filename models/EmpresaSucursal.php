<?php

namespace app\models;

use app\models\base\EmpresaSucursal as BaseEmpresaSucursal;

/**
 * This is the model class for table "empresa_sucursal".
 */
class EmpresaSucursal extends BaseEmpresaSucursal
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['empresa_id', 'ciudad_id', 'direccion', 'descripcion', 'lng', 'lat'], 'required'],
        [['empresa_id', 'ciudad_id'], 'integer'],
        [['direccion'], 'string', 'max' => 100],
        [['descripcion'], 'string', 'max' => 500],
        [['lng', 'lat'], 'string', 'max' => 50]
      ]);
  }

  public function extraFields()
  {
    return [
      'empresa',
      'ciudad'
    ];
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['descripcion'] = function ($model){
      if (($model->descripcion == null)){
        return $model->empresa->descripcion;
      }
      return $model->descripcion;
    };
    return $fields;
  }

}
