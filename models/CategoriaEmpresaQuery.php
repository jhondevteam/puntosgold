<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CategoriaEmpresa]].
 *
 * @see CategoriaEmpresa
 */
class CategoriaEmpresaQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['activa'=>1]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return CategoriaEmpresa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CategoriaEmpresa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}