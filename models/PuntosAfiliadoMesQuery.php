<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PuntosAfiliadoMes]].
 *
 * @see PuntosAfiliadoMes
 */
class PuntosAfiliadoMesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PuntosAfiliadoMes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PuntosAfiliadoMes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}