<?php

namespace app\models;

/**
 * This is the model class for table "ciudad".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $departamento_id
 * @property integer $principal
 *
 * @property EmpresaSucursal[] $empresaSucursals
 * @property Departamento $departamento
 */
class Ciudad extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'ciudad';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['nombre','departamento_id','principal'],'safe','on' => 'buscar'],
      [['nombre', 'departamento_id','principal'], 'required','on' => 'default'],
      [['departamento_id'], 'integer'],
      [['nombre'], 'string', 'max' => 200],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nombre' => 'Nombre',
      'departamento_id' => 'Departamento ID',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getEmpresaSucursals()
  {
    return $this->hasMany(EmpresaSucursal::className(), ['ciudad_id' => 'id']);
  }

  public function getDepartamento()
  {
    return $this->hasOne(Departamento::className(), ['id' => 'departamento_id']);
  }
}
