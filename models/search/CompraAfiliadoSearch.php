<?php

namespace app\models\search;

use app\models\CompraAfiliado;
use app\models\CompraAfiliadoPuntos;
use app\models\Empresa;
use phpDocumentor\Reflection\Types\Null_;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * app\models\search\CompraAfiliadoSearch represents the model behind the search form about `app\models\CompraAfiliado`.
 */
class CompraAfiliadoSearch extends CompraAfiliado
{
  /** @var Empresa */
  public $empresa_id;

  public $fecha_hasta;
  public $pagado;
  public $recaudo;
  /** @var ActiveQuery */
  private $query;

  public $tipo_compra;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'monto', 'monto_cobra_empresa', 'puntos', 'afiliado_id', 'empleado_id', 'pagado', 'recaudo', 'empresa_id','tipo_compra','pago_puntos'], 'integer'],
      [['fecha', 'fecha_hasta', 'comentario'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  public function attributeLabels()
  {
    $fields = parent::attributeLabels();
    $fields['fecha_hasta'] = 'Fecha hasta';
    return $fields;
  }

  public function prepareCompraAfiliado($params)
  {
    $this->query = CompraAfiliado::find();
    $this->load($params);

    //empresa -> empleado -> compra

    if ($this->fecha and $this->fecha_hasta)
      $this->query->andFilterWhere(['between', 'fecha', $this->fecha . " 00:00:00", $this->fecha_hasta . " 23:59:59"]);
    if ($this->empleado_id == null and $this->empresa_id !== null) {
      $empresa = Empresa::findOne($this->empresa_id);
      $empleados = $empresa->getEmpleados()
        ->select('id')->createCommand()->queryColumn();

      $this->query->andWhere(['empleado_id' => $empleados]);
    }
    //utiliza para saber si empleado ya entrego dinero a empresa
    if ($this->recaudo != null and $this->recaudo == '0') {
      $this->query->andWhere(['is', 'cobro_empresa_id', null]);
    } elseif ($this->recaudo != null and $this->recaudo == '1') {
      $this->query->andWhere(['is not', 'cobro_empresa_id', null]);
    }
    //utilizar para saber di la empresa ya pago a puntosdorados
    if ($this->pagado != null and $this->pagado == '0') {
      $this->query->andWhere(['is', 'pago_empresa_id', null]);
    } elseif ($this->pagado != null and $this->pagado == '1') {
      $this->query->andWhere(['is not', 'pago_empresa_id', null]);
    }
    $this->query->andFilterWhere([
      'id' => $this->id,
      'monto' => $this->monto,
      'monto_cobra_empresa' => $this->monto_cobra_empresa,
      'puntos' => $this->puntos,
      'compra_afiliado.afiliado_id' => $this->afiliado_id,
      'empleado_id' => $this->empleado_id,
    ]);
    $this->query->andFilterWhere(['like', 'comentario', $this->comentario]);
  }

  /**
   * @return ActiveDataProvider
   */
  public function search()
  {

    $dataProviderEfectivo = new ActiveDataProvider([
      'query' => $this->query,
      'sort' => ['defaultOrder' => ['id'=>SORT_DESC]]
    ]);
    return $dataProviderEfectivo;
  }

  public function getTotalRecaudar()
  {
    $q = clone $this->query;
    $value = $q
      ->sum('monto_cobra_empresa');
    return ($value != null) ? $value : 0;
  }

  public function getTotalPendientePagar(){
    $q = clone $this->query;
    $value = $q
      ->where(['is','compra_afiliado.pago_empresa_id',null])
      ->andWhere(['compra_afiliado.pago_puntos'=>false])
      ->sum('monto_cobra_empresa');
    return ($value != null) ? $value : 0;
  }

  public function getTotalPendienteVendido()
  {
    $q = clone $this->query;
    $q->where(['is','pago_empresa_id',null]);
    $value = $q->sum('monto');
    return ($value != null) ? $value : 0;
  }

  public function getTotalPendientePuntos()
  {
    $q = clone $this->query;
    return $q->where(['is','pago_empresa_id',null])
      ->andWhere(['pago_puntos'=>true])->sum('monto');
  }

  public function getTotalVendido()
  {
    $q = clone $this->query;
    $value = $q
      ->sum('monto');
    return ($value != null) ? $value : 0;
  }

  public function getTotalPuntos(){
    $q = clone $this->query;
    $value = $q
      ->andWhere(['pago_puntos'=>true])
      ->sum('monto');
    return ($value != null) ? $value : 0;
  }
}
