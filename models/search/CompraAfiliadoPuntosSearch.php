<?php

namespace app\models\search;

use app\models\CompraAfiliadoPuntos;
use app\models\Empresa;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * app\models\search\CompraAfiliadoPuntosSearch represents the model behind the search form about `app\models\CompraAfiliadoPuntos`.
 */
class CompraAfiliadoPuntosSearch extends CompraAfiliadoPuntos
{
  /** @var  Empresa */
  public $empresa_id;
  public $fecha_hasta;
  public $pagado;

  /** @var ActiveQuery */
  private $query;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'afiliado_id', 'empleado_id', 'pago_empresa_id', 'monto', 'puntos', 'pagado'], 'integer'],
      [['fecha', 'comentario', 'fecha_hasta'], 'safe'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   * @param array $params
   */
  public function prepare($params)
  {
    $this->query = CompraAfiliadoPuntos::find();
    $this->load($params, '');
    if ($this->fecha and $this->fecha_hasta)
      $this->query->andFilterWhere(['between', 'fecha', $this->fecha . " 00:00:00", $this->fecha_hasta . " 00:00:00"]);
    if ($this->empleado_id == null) {
      $empleados = $this->empresa_id->getEmpleados()
        ->select('id')->createCommand()->queryColumn();
      $this->query->andWhere(['empleado_id' => $empleados]);
    }

    //se utiliza para saber si la empresa ya arreglo esto con puntos dorados
    if ($this->pagado != null and $this->pagado == '0') {
      $this->query->andWhere(['is', 'pago_empresa_id', null]);
    } elseif ($this->pagado != null and $this->pagado == '1') {
      $this->query->andWhere(['is not', 'pago_empresa_id', null]);
    }
    $this->query->andFilterWhere([
      'id' => $this->id,
      'afiliado_id' => $this->afiliado_id,
      'empleado_id' => $this->empleado_id,
      'pago_empresa_id' => $this->pago_empresa_id,
      'monto' => $this->monto,
      'puntos' => $this->puntos,
    ]);

    $this->query->andFilterWhere(['like', 'comentario', $this->comentario]);
  }

  public function search(){
    $dataProvider = new ActiveDataProvider([
      'query' => $this->query,
    ]);

    return $dataProvider;
  }

  public function getTotalPagar(){
    $q = clone $this->query;
    $value = $q
      ->where(['is','compra_afiliado_puntos.pago_empresa_id',null])
      ->sum('monto');
    return ($value != null)? $value : 0;
  }
}
