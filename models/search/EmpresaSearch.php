<?php

namespace app\models\search;

use app\models\Empresa;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\search\EmpresaSearch represents the model behind the search form about `app\models\Empresa`.
 */
class EmpresaSearch extends Empresa
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'admin_zona_id', 'categoria_id', 'estado'], 'integer'],
      [['nombre', 'descripcion', 'nit'], 'safe'],
      [['porcentaje', 'porcentaje_afiliado'], 'number'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    \Yii::info("Admin zona _id ".$this->admin_zona_id);
    $query = Empresa::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'admin_zona_id' => $this->admin_zona_id,
      'categoria_id' => $this->categoria_id,
      'estado' => $this->estado,
      'porcentaje' => $this->porcentaje,
      'porcentaje_afiliado' => $this->porcentaje_afiliado,
    ]);

    $query->andFilterWhere(['like', 'nombre', $this->nombre])
      ->andFilterWhere(['like', 'descripcion', $this->descripcion])
      ->andFilterWhere(['like', 'nit', $this->nit]);

    return $dataProvider;
  }
}
