<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Afiliado;

/**
 * app\models\search\AfiliadoSearch represents the model behind the search form about `app\models\Afiliado`.
 */
 class AfiliadoSearch extends Afiliado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'grupo_id', 'usuario_id', 'puntos'], 'integer'],
            [['fecha_registro', 'code_invitacion', 'app_token'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Afiliado::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'grupo_id' => $this->grupo_id,
            'usuario_id' => $this->usuario_id,
            'fecha_registro' => $this->fecha_registro,
            'puntos' => $this->puntos,
        ]);

        $query->andFilterWhere(['like', 'code_invitacion', $this->code_invitacion])
            ->andFilterWhere(['like', 'app_token', $this->app_token]);

        return $dataProvider;
    }
}
