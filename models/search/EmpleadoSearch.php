<?php

namespace app\models\search;

use app\models\Empleado;
use app\models\Empresa;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * app\models\search\EmpleadoSearch represents the model behind the search form about `app\models\Empleado`.
 */
class EmpleadoSearch extends Empleado
{
  /** @var  Empresa */
  public $empresa;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['id', 'empresa_id', 'usuario_id'], 'integer'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    \Yii::info("Empresa_id : ".$this->empresa_id);
    $query = Empleado::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    $this->load($params);

    $query->andFilterWhere([
      'id' => $this->id,
      'empresa_id' => $this->empresa_id,
      'usuario_id' => $this->usuario_id,
    ]);

    return $dataProvider;
  }
}
