<?php

namespace app\models;

use app\models\base\Grupo as BaseGrupo;

/**
 * This is the model class for table "grupo".
 */
class Grupo extends BaseGrupo
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['afiliado_id', 'numero_afiliados'], 'required'],
        [['afiliado_id', 'numero_afiliados'], 'integer']
      ]);
  }

}
