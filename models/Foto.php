<?php

namespace app\models;

use app\models\base\Foto as BaseFoto;
use yii\helpers\Url;

/**
 * This is the model class for table "foto".
 */
class Foto extends BaseFoto
{
  public $file;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['file'], 'file', 'extensions' => 'jpg, png, jpeg',
        'checkExtensionByMimeType' => false],
//      [['file'], 'image', 'minWidth' => 800, 'maxWidth' => 800, 'minHeight' => 500, 'maxHeight' => 500],
      [['empresa_id', 'url_foto'], 'required'],
      [['empresa_id'], 'integer'],
      [['url_foto'], 'string', 'max' => 100]
    ];
  }

  public function fields()
  {
    return [
      'url_foto'=>function($model){
        return Url::to("@web/$model->url_foto",true);
      }
    ];
  }
}
