<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DriverTrip]].
 *
 * @see DriverTrip
 */
class DriverTripQuery extends \yii\db\ActiveQuery
{
  /*public function active()
  {
      $this->andWhere('[[status]]=1');
      return $this;
  }*/

  /**
   * @inheritdoc
   * @return DriverTrip[]|array
   */
  public function all($db = null)
  {
    return parent::all($db);
  }

  /**
   * @inheritdoc
   * @return DriverTrip|array|null
   */
  public function one($db = null)
  {
    return parent::one($db);
  }
}