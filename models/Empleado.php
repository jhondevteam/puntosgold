<?php

namespace app\models;

use app\models\base\Empleado as BaseEmpleado;

/**
 * This is the model class for table "empleado".
 */
class Empleado extends BaseEmpleado
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['empresa_id', 'usuario_id'], 'required'],
        [['empresa_id', 'usuario_id'], 'integer'],
        [['usuario_id'], 'unique']
      ]);
  }

  public function getMontoCobrar()
  {
    $value = $this->getCompraAfiliados()
      ->where(['is', 'cobro_empresa_id', null])
      ->sum('monto_cobra_empresa');
    return $value != null ? $value : 0;
  }

  public function getSaldoCobrar(){
    $value = $this->getCobroEmpresaEmpleados()
      ->select('saldo')
      ->orderBy(['id'=>SORT_DESC])
      ->createCommand()
      ->queryScalar();
    return $value !== null? $value : 0;
  }
}
