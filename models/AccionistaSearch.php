<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accionista;

/**
 * AccionistaSearch represents the model behind the search form of `app\models\Accionista`.
 */
class AccionistaSearch extends Accionista
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'acciones', 'total_pagar', 'pagado'], 'integer'],
            [['nombres', 'apellidos', 'correo', 'telefono', 'tipo_doc'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accionista::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'acciones' => $this->acciones,
            'total_pagar' => $this->total_pagar,
            'pagado' => $this->pagado,
        ]);

        $query->andFilterWhere(['like', 'nombres', $this->nombres])
            ->andFilterWhere(['like', 'apellidos', $this->apellidos])
            ->andFilterWhere(['like', 'correo', $this->correo])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'tipo_doc', $this->tipo_doc]);

        return $dataProvider;
    }
}
