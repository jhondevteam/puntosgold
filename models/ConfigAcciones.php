<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config_acciones".
 *
 * @property int $id
 * @property string $clave
 * @property string $valor
 */
class ConfigAcciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'config_acciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['clave'], 'required'],
            [['clave', 'valor'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clave' => 'Clave',
            'valor' => 'Valor',
        ];
    }
}
