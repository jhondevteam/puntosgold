<?php

namespace app\models;

use app\models\base\CategoriaEmpresa as BaseCategoriaEmpresa;
use yii\helpers\Url;

/**
 * This is the model class for table "categoria_empresa".
 */
class CategoriaEmpresa extends BaseCategoriaEmpresa
{
  public $pic;
  public $pic_ico;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['pic','pic_ico'], 'file', 'extensions' => 'jpg,png', 'maxSize' => 1024 * 1024 * 2, 'skipOnEmpty' => false, 'on' => 'default'],
      [['pic','pic_ico'], 'file', 'extensions' => 'jpg,png', 'maxSize' => 1024 * 1024 * 2, 'skipOnEmpty' => true, 'on' => 'update'],
      [['pic'], 'image', 'minWidth' => 640, 'maxWidth' => 640, 'minHeight' => 360, 'maxHeight' => 360],
      [['pic_ico'], 'image', 'minWidth' => 24, 'maxWidth' => 24, 'minHeight' => 41, 'maxHeight' => 41],
      [['nombre', 'url_banner', 'activa'], 'required'],
      [['activa'], 'integer'],
      [['nombre', 'url_banner'], 'string', 'max' => 100]
    ];
  }

  public function fields(){
    $fields = parent::fields();
    $fields['url_banner'] = function($model){
      return Url::to("@web/$model->url_banner",true);
    };
    $fields['url_icon'] = function($model){
      return Url::to("@web/$model->url_icon",true);
    };
    return $fields;
  }
}
