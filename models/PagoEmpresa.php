<?php

namespace app\models;

use app\models\base\PagoEmpresa as BasePagoEmpresa;
use yii\helpers\Url;

/**
 * This is the model class for table "pago_empresa".
 */
class PagoEmpresa extends BasePagoEmpresa
{
  public $file;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['file'], 'file', 'extensions' => 'jpg,png,jpeg'],
      [['fecha', 'empresa_id', 'monto', 'comision_admin_zona'], 'required'],
      [['fecha'], 'safe'],
      [['empresa_id', 'monto', 'admin_zona_id'], 'integer']
    ];
  }

  public function attributeLabels()
  {
    $labels = parent::attributeLabels();
    $labels['file'] = 'Foto del recibo';
    return $labels;
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['fecha_format'] = function (PagoEmpresa $model) {
      return \Yii::$app->formatter->asDate($model->fecha);
    };
    $fields['url_file'] = function (PagoEmpresa $model) {
      if ($this->url_file != null)
        return Url::to("@web/$model->url_file", true);
      return null;
    };
    return $fields;
  }
}
