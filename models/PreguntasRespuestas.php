<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "preguntas_respuestas".
 *
 * @property integer $id
 * @property string $pregunta
 * @property string $respuesta
 */
class PreguntasRespuestas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preguntas_respuestas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pregunta', 'respuesta'], 'required'],
            [['pregunta'], 'string', 'max' => 500],
            [['respuesta'], 'string', 'max' => 5000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'pregunta' => Yii::t('app', 'Pregunta'),
            'respuesta' => Yii::t('app', 'Respuesta'),
        ];
    }
}
