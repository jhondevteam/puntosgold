<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TokenCompra]].
 *
 * @see TokenCompra
 */
class TokenCompraQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TokenCompra[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TokenCompra|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}