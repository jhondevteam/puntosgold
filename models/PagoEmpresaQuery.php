<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PagoEmpresa]].
 *
 * @see PagoEmpresa
 */
class PagoEmpresaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PagoEmpresa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PagoEmpresa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}