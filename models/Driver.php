<?php

namespace app\models;

use app\models\base\Driver as BaseDriver;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "driver".
 */
class Driver extends BaseDriver
{
  public $document;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
       [['usuario_id', 'driver_category_id', 'credits', 'porcent', 'active', 'departamento_id'], 'required', 'on' => 'registro'],
       [['usuario_id', 'driver_category_id', 'credits', 'active', 'empresa_transportadora_id', 'departamento_id'], 'integer'],
       [['porcent'], 'number'],
       [['usuario_id', 'driver_category_id', 'active', 'car_info', 'departamento_id'], 'safe', 'on' => 'search']
    ];
  }


  public function search($params)
  {
    $this->load($params);
    $query = static::find();
    $query->filterWhere([
       'usuario_id' => $this->usuario_id,
       'driver_category_id' => $this->driver_category_id,
       'departamento_id' => $this->departamento_id
    ]);
    return new ActiveDataProvider([
       'query' => $query,
       'pagination' => [
          'pageSize' => 50,
       ]
    ]);
  }

  public function attributeLabels()
  {
    return [
       'id' => 'ID',
       'usuario_id' => 'Usuario',
       'driver_category_id' => 'Categoria',
       'credits' => 'Creditos',
       'porcent' => 'Porcentaje',
    ];
  }
}
