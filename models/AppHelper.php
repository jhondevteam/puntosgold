<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 18/02/19
 * Time: 02:31 PM
 */

namespace app\models;


class AppHelper
{
  public static function obtenerDirectorio($folder)
  {
    $path = 'uploads/';
    if (!file_exists('uploads'))
      mkdir('uploads');
    if (!file_exists("uploads/$folder")) {
      $folders = explode('/', $folder);
      $path = 'uploads/';
      foreach ($folders as $folder) {
        if (!file_exists($path . $folder)) {
          mkdir($path . $folder);
        }
        $path .= $folder . '/';
      }
    } else
      $path .= $folder;
    return $path;
  }
}