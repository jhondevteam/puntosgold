<?php

namespace app\models;

use yii\base\Model;

/**
 * This is the model class for table "archivo".
 */
class Archivo extends Model
{
  public $file;
  public $ruta;
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['file'], 'file', 'extensions' => 'jpg, png, jpeg',
        'checkExtensionByMimeType' => false,'on'=>'fileUP'],
      ['file','safe'],
    ];
  }
}
