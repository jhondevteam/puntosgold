<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EmpresaTransportadora]].
 *
 * @see EmpresaTransportadora
 */
class EmpresaTransportadoraQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EmpresaTransportadora[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EmpresaTransportadora|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}