<?php

namespace app\models;

use \app\models\base\TokenCompra as BaseTokenCompra;

/**
 * This is the model class for table "token_compra".
 */
class TokenCompra extends BaseTokenCompra
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['compra_afiliado_puntos_id', 'afiliado_id'], 'integer'],
        [['afiliado_id', 'token', 'fecha'], 'required'],
        [['fecha'], 'safe'],
        [['token'], 'string', 'max' => 20]
      ]);
  }

}
