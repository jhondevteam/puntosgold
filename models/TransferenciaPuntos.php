<?php

namespace app\models;

use app\models\base\TransferenciaPuntos as BaseTransferenciaPuntos;

/**
 * This is the model class for table "transferencia_puntos".
 */
class TransferenciaPuntos extends BaseTransferenciaPuntos
{
  public $password;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['de_afiliado_id', 'para_afiliado_id', 'monto', 'fecha'], 'required'],
      [['de_afiliado_id', 'para_afiliado_id', 'monto'], 'integer', 'on' => 'default'],
      [['password'],'required','on' => 'validation'],
      [['password'], 'validatePassword', 'on' => 'validation'],
      [['monto'], 'validateMonto', 'on' => 'validation'],
      [['para_afiliado_id'], 'validatePara', 'on' => 'validation'],
      [['fecha', 'password'], 'safe']
    ];
  }

  public function validatePara()
  {
    $afi = Afiliado::findOne(['code_invitacion' => $this->para_afiliado_id]);
    if ($afi == null)
      $this->addError('para_afiliado_id', 'No se encontro afiliado con este codigo');
    else
      $this->para_afiliado_id = $afi->id;
  }

  public function validatePassword()
  {
    \Yii::info('validatePassword');
    $user = \Yii::$app->user->identity;
    if (!$user->validatePassword($this->password))
      $this->addError('password', 'Contraseña incorrecta');
  }

  public function validateMonto(){
    \Yii::info('validatePassword');
    if ($this->monto > $this->deAfiliado->puntos)
      $this->addError('monto','Sus puntos no son suficientes.');
  }

  public function extraFields()
  {
    return ['paraAfiliado'];
  }

  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'de_afiliado_id' => 'De afiliad',
      'para_afiliado_id' => 'Codigo de Afiliado',
      'monto' => 'Monto',
      'fecha' => 'Fecha',
      'password'=>'Contraseña'
    ];
  }
}
