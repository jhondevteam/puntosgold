<?php

namespace app\models;

use app\models\base\PuntosAfiliadoMes as BasePuntosAfiliadoMes;

/**
 * This is the model class for table "puntos_afiliado_mes".
 */
class PuntosAfiliadoMes extends BasePuntosAfiliadoMes
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['afiliado_id', 'fecha', 'puntos', 'pagado'], 'required'],
        [['afiliado_id', 'puntos', 'pagado'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

}
