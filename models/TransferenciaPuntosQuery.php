<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TransferenciaPuntos]].
 *
 * @see TransferenciaPuntos
 */
class TransferenciaPuntosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TransferenciaPuntos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TransferenciaPuntos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}