<?php

namespace app\models;

use app\helper\AppHelper;
use app\models\base\Empresa as BaseEmpresa;
use yii\helpers\Url;

/**
 * This is the model class for table "empresa".
 */
class Empresa extends BaseEmpresa
{

  const MODE_PORCENTAJE = 'Modo de porcentaje';
  const MODE_PPTRANSACCION = 'Modo por transacción';
  public $pic;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
       ['pic', 'file', 'extensions' => 'jpg,png', 'maxSize' => 1024 * 1024 * 2, 'skipOnEmpty' => false, 'on' => 'default'],
       ['pic', 'file', 'extensions' => 'jpg,png', 'maxSize' => 1024 * 1024 * 2, 'skipOnEmpty' => true, 'on' => 'update'],
//      [['pic'], 'image', 'minWidth' => 800, 'maxWidth' => 800, 'minHeight' => 500, 'maxHeight' => 500],
       [['categoria_id', 'nombre', 'descripcion', 'nit', 'estado', 'mode'], 'required'],
       [['admin_zona_id', 'categoria_id', 'estado', 'puntos_afiliado', 'puntos'], 'integer'],
       [['porcentaje', 'porcentaje_afiliado'], 'number'],
       ['porcentaje_afiliado', 'compare', 'compareAttribute' => 'porcentaje', 'operator' => '<=', 'type' => 'number'],
       ['puntos_afiliado', 'compare', 'compareAttribute' => 'puntos', 'operator' => '<=', 'type' => 'number'],
       [['nombre'], 'string', 'max' => 250],
       [['descripcion'], 'string', 'max' => 5000],
       [['nit'], 'string', 'max' => 100]
    ];
  }

  public function beforeValidate()
  {
    if ($this->mode == static::MODE_PORCENTAJE) {
      if (empty($this->porcentaje) or empty($this->porcentaje_afiliado)) {
        $this->addError('porcentaje', 'El porcentaje no puede estar vacio');
      }
      if (empty($this->porcentaje_afiliado)) {
        $this->addError('porcentaje_afiliado', 'El porcentaje para el afiliado no puede estar vacio');
      }
    } else {
      if (empty($this->puntos_afiliado)) {
        $this->addError('puntos_afiliado', 'Puntos para afiliado no debe estar vacio');
      }
      if (empty($this->puntos)) {
        $this->addError('puntos', 'Puntos para Puntos Dorados no debe estar vacio');
      }
    }
    return parent::beforeValidate();
  }

  public function beforeSave($insert)
  {
    $this->porcentaje = AppHelper::getPorcentajeNumber($this->porcentaje);
    $this->porcentaje_afiliado = AppHelper::getPorcentajeNumber($this->porcentaje_afiliado);
    \Yii::info($this->porcentaje . " -> " . $this->porcentaje_afiliado);
    return parent::beforeSave($insert);
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['url_logo'] = function ($model) {
      return Url::to("@web/$model->url_logo", true);
    };
    $fields['info'] = 'info';
    return $fields;
  }

  public function extraFields()
  {
    return [
       'categoria' => 'categoria',
       'fotos' => 'fotos',
       'sucursales' => 'empresaSucursals',
       'videos' => function (Empresa $model) {
         return $this->getVideos()->where(['activo' => 1])->all();
       }
    ];
  }

  public function getInfo()
  {
    if ($this->mode == static::MODE_PORCENTAJE) {
      $puntos = (10000 * $this->porcentaje_afiliado) / 2;
      return "Recibes: " . \Yii::$app->formatter->asCurrency($puntos) . " puntos por cada $10.000 COP en compras";
    }
    return "Recibes: " . \Yii::$app->formatter->asCurrency($this->puntos_afiliado / 2) . " Puntos por Compra";
  }

  public function getImgIndex()
  {
    return str_replace('.', '_grey.', $this->url_logo);
  }

  public function toEdit()
  {
    $this->porcentaje_afiliado = AppHelper::getNumberPorcentaje($this->porcentaje_afiliado);
    $this->porcentaje = AppHelper::getNumberPorcentaje($this->porcentaje);
  }

  /**
   * @return CompraAfiliado[]|array las ventas pendientes por pagar a puntos dorados
   */
  public function getVentasPagar()
  {
    $emps = $this->getEmpleados()
       ->select('id')
       ->createCommand()
       ->queryColumn();
    return CompraAfiliado::find()
       ->where(['empleado_id' => $emps])
       ->andWhere(['is', 'pago_empresa_id', null])
       ->all();
  }

  /**
   * @return mixed el total vendido por la empresa que aun no se paga a puntos dorados
   * @throws \yii\db\Exception
   */
  public function getPendienteTotalVendido()
  {
    $emps = $this->getEmpleados()
       ->select('id')
       ->createCommand()
       ->queryColumn();
    $value = CompraAfiliado::find()
       ->where(['empleado_id' => $emps])
       ->andWhere(['is', 'pago_empresa_id', null])
       ->sum('monto');
    return $value != null ? $value : 0;
  }

  /**
   * @return PagoEmpresa|array|null|\yii\db\ActiveRecord
   */
  public function getPagoPendiente()
  {
    return $this->getPagoEmpresas()
       ->orderBy(['id' => SORT_DESC])
       ->where(['pagado' => 0])
       ->orWhere(['pagado' => 2])
       ->one();
  }

  public function getImagen()
  {
    return Url::to("@web/$this->url_logo", true);
  }

  public function getGanaciaAdminstrador()
  {
    $gananciaAdmin = 0;
    if ($this->adminZona)
      if ($this->mode == Empresa::MODE_PORCENTAJE) {
        $saldo = $this->getSaldoPagar();
        $dif = $this->porcentaje - $this->porcentaje_afiliado;
        $ganancia = $saldo * $dif;

        $porcentajeAdministrador = $this->adminZona->getPorcentajeFormat();
        $gananciaAdmin = $ganancia * $porcentajeAdministrador;
        \Yii::info($saldo . " -> " . $ganancia . " -> " . $porcentajeAdministrador . " -> " . $gananciaAdmin);
      } else {
        $ganancia = $this->getDiferenciaPuntos();
        $porcentajeAdministrador = $this->adminZona->getPorcentajeFormat();
        $gananciaAdmin = $ganancia * $porcentajeAdministrador;
        \Yii::info($ganancia . " -> " . $porcentajeAdministrador . " -> " . $gananciaAdmin);
      }
    return $gananciaAdmin;
  }

  /**
   * @return mixed total que la empresa debe pagar a puntos dorados
   */
  public function getSaldoPagar()
  {
    $emps = $this->getEmpleados()
       ->select('id')
       ->createCommand()
       ->queryColumn();
    return CompraAfiliado::find()
       ->where(['empleado_id' => $emps])
       ->andWhere(['is', 'pago_empresa_id', null])
       ->sum('monto_cobra_empresa');
  }

  public function getDiferenciaPuntos()
  {
    $emps = $this->getEmpleados()
       ->select('id')
       ->createCommand()
       ->queryColumn();
    $value = CompraAfiliado::find()
       ->where(['empleado_id' => $emps])
       ->andWhere(['is', 'pago_empresa_id', null])
       ->sum('(monto_cobra_empresa-(puntos*2))');
    return $value != null ? $value : 0;
  }

  /**
   * @return mixed el total de puntos aun pendientes por descontar de la empresa
   * ya que son compras con puntos
   * @throws \yii\db\Exception
   */
  public function getPendienteTotalPuntosPagar()
  {
    $emps = $this->getEmpleados()
       ->select('id')
       ->createCommand()
       ->queryColumn();
    $value = CompraAfiliado::find()->where(['empleado_id' => $emps])
       ->andWhere(['is', 'pago_empresa_id', null])
       ->andWhere(['pago_puntos' => true])
       ->sum('monto');
    return $value != null ? $value : 0;
  }
}
