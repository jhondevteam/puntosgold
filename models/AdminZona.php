<?php

namespace app\models;

use app\models\base\AdminZona as BaseAdminZona;

/**
 * This is the model class for table "admin_zona".
 */
class AdminZona extends BaseAdminZona
{
  public function rules()
  {
    return parent::rules();
  }

  public function getPorcentajeFormat(){
    if ($this->porcentaje < 10){
      return floatval("0.0$this->porcentaje");
    }else
      return floatval("0.$this->porcentaje");
  }
}
