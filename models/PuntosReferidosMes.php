<?php

namespace app\models;

use \app\models\base\PuntosReferidosMes as BasePuntosReferidosMes;

/**
 * This is the model class for table "puntos_referidos_mes".
 */
class PuntosReferidosMes extends BasePuntosReferidosMes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['afiliado_id', 'fecha', 'puntos', 'pagado'], 'required'],
            [['afiliado_id', 'puntos', 'pagado'], 'integer'],
            [['fecha'], 'safe']
        ]);
    }
	
}
