<?php

namespace app\models;

use app\models\base\NotificacionEntrega as BaseNotificacionEntrega;

/**
 * This is the model class for table "notificacion_entrega".
 */
class NotificacionEntrega extends BaseNotificacionEntrega
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['afiliado_id', 'sucursal_id', 'app_token', 'resultado', 'fecha'], 'required'],
        [['afiliado_id', 'sucursal_id'], 'integer'],
        [['fecha'], 'safe'],
        [['app_token', 'resultado'], 'string', 'max' => 5000]
      ]);
  }

}
