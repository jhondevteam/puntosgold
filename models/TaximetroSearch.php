<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Taximetro;

/**
 * TaximetroSearch represents the model behind the search form of `app\models\Taximetro`.
 */
class TaximetroSearch extends Taximetro
{
  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['id', 'con_unidades', 'arranque', 'minima', 'distancia_mts', 'distancia_precio', 'segundos', 'segundos_precio', 'recargo_aeropuerto', 'recargo_terminal', 'recargo_nocturno', 'recargo_puerta_puerta', 'precio_unidad'], 'integer'],
      [['ciudades_ids'], 'safe'],
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }
  
  /**
   * Creates data provider instance with search query applied
   *
   * @param array $params
   *
   * @return ActiveDataProvider
   */
  public function search($params)
  {
    $query = Taximetro::find();
    
    // add conditions that should always apply here
    
    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);
    
    $this->load($params);
    
    if (!$this->validate()) {
      // uncomment the following line if you do not want to return any records when validation fails
      // $query->where('0=1');
      return $dataProvider;
    }
    
    // grid filtering conditions
    $query->andFilterWhere([
      'id' => $this->id,
      'con_unidades' => $this->con_unidades,
      'arranque' => $this->arranque,
      'minima' => $this->minima,
      'distancia_mts' => $this->distancia_mts,
      'distancia_precio' => $this->distancia_precio,
      'segundos' => $this->segundos,
      'segundos_precio' => $this->segundos_precio,
      'recargo_aeropuerto' => $this->recargo_aeropuerto,
      'recargo_terminal' => $this->recargo_terminal,
      'recargo_nocturno' => $this->recargo_nocturno,
      'recargo_puerta_puerta' => $this->recargo_puerta_puerta,
      'precio_unidad' => $this->precio_unidad,
    ]);
    
    $query->andFilterWhere(['like', 'ciudades_ids', $this->ciudades_ids]);
    
    return $dataProvider;
  }
}
