<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "testimonios".
 *
 * @property integer $id
 * @property string $titulo
 * @property string $descripcion
 * @property string $nombre
 * @property string $url_video
 */
class Testimonios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testimonios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['titulo', 'descripcion', 'nombre','url_video'], 'required'],
            [['titulo', 'nombre'], 'string', 'max' => 250],
            [['descripcion'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'titulo' => Yii::t('app', 'Titulo'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }
}
