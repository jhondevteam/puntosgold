<?php

namespace app\models;

use app\models\base\Ayuda as BaseAyuda;

/**
 * This is the model class for table "ayuda".
 */
class Ayuda extends BaseAyuda
{
  const APP_AFILIADO = "App de Afiliados";
  const APP_VENDEDOR = "App de Vendedores";
  const WEB_EMPRESA = "Administracion de Empresas";
  const APP_TAXI_DRIVER = "App taxi driver";
  const APP_TAXI = "App Taxi";

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['titulo', 'descripcion', 'destino'], 'required'],
        [['titulo'], 'string', 'max' => 100],
        [['url_video', 'destino'], 'string', 'max' => 30],
        [['descripcion'], 'string', 'max' => 500]
      ]);
  }

  public static function getArrayConst(){
    return [
      static::APP_AFILIADO => static::APP_AFILIADO,
      static::APP_VENDEDOR => static::APP_VENDEDOR,
      static::WEB_EMPRESA => static::WEB_EMPRESA,
      static::APP_TAXI => static::APP_TAXI,
      static::APP_TAXI_DRIVER => static::APP_TAXI_DRIVER
    ];
  }
}
