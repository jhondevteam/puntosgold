<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompraAfiliado]].
 *
 * @see CompraAfiliado
 */
class CompraAfiliadoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CompraAfiliado[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompraAfiliado|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}