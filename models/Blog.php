<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $usuario_id
 * @property string $titulo
 * @property string $texto
 * @property string $url_img
 * @property string $fecha
 *
 * @property Usuario $usuario
 */
class Blog extends \yii\db\ActiveRecord
{
 public $picture;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usuario_id', 'titulo', 'texto', 'url_img', 'fecha'], 'required'],
            [['usuario_id'], 'integer'],
            [['texto'], 'string'],
            [['fecha','picture'], 'safe'],
            [['titulo'], 'string', 'max' => 500],
            [['url_img'], 'string', 'max' => 250],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'url_img' => 'Url Img',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
}
