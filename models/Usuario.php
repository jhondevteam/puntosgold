<?php

namespace app\models;

use app\models\base\Usuario as BaseUsuario;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "usuario".
 */
class Usuario extends BaseUsuario implements IdentityInterface
{
  public $picture;

  public static function findByUsername($username)
  {
    return static::find()->where(['email' => $username])
       ->orWhere(['documento' => $username])->one();
  }

  /**
   * Finds an identity by the given ID.
   * @param string|int $id the ID to be looked for
   * @return IdentityInterface the identity object that matches the given ID.
   * Null should be returned if such an identity cannot be found
   * or the identity is not in an active state (disabled, deleted, etc.)
   */
  public static function findIdentity($id)
  {
    return static::findOne($id);
  }

  /**
   * Finds an identity by the given token.
   * @param mixed $token the token to be looked for
   * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
   * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
   * @return Usuario the identity object that matches the given token.
   * Null should be returned if such an identity cannot be found
   * or the identity is not in an active state (disabled, deleted, etc.)
   */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['access_token' => $token]);
  }

  public function rules()
  {
    return [
       [['documento', 'nombres', 'email', 'password', 'auth_key', 'access_token'], 'required'],
       [['estado'], 'integer'],
       [['documento'], 'string', 'max' => 100],
       [['nombres'], 'string', 'max' => 250],
       [['email', 'foto'], 'string', 'max' => 200],
       [['picture'], 'safe'],
       [['password', 'password_reset_token', 'auth_key', 'access_token'], 'string', 'max' => 500],
       [['documento', 'email'], 'uniqueAfiliado', 'on' => 'afiRegister'],
       [['documento', 'email'], 'uniqueEmpleado', 'on' => 'empRegister'],
    ];
  }

  public function uniqueAfiliado()
  {
    $q = Usuario::find()
       ->innerJoinWith('afiliado')
       ->where(['documento' => $this->documento, 'email' => $this->documento]);
    if ($q != null)
      $this->addError('documento', 'Ya existe afiliado con este documento');
  }

  public function uniqueEmpleado()
  {
    $q = Usuario::find()
       ->innerJoinWith('empleado')
       ->where(['email' => $this->email]);
    if ($q->one() != null and $this->isNewRecord)
      $this->addError('email', 'Correo ya registrado');
    else {
      $val = $q->andWhere(['!=', 'usuario.id', $this->id])->one();
      if ($val != null)
        $this->addError('email', 'Correo ya registrado');
    }
  }

  public function validatePassword($password)
  {
    return \Yii::$app->security->validatePassword($password, $this->password);
  }

  /**
   * Returns an ID that can uniquely identify a user identity.
   * @return string|int an ID that uniquely identifies a user identity.
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * Returns a key that can be used to check the validity of a given identity ID.
   *
   * The key should be unique for each individual user, and should be persistent
   * so that it can be used to check the validity of the user identity.
   *
   * The space of such keys should be big enough to defeat potential identity attacks.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @return string a key that is used to check the validity of a given identity ID.
   * @see validateAuthKey()
   */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
   * Validates the given auth key.
   *
   * This is required if [[User::enableAutoLogin]] is enabled.
   * @param string $authKey the given auth key
   * @return bool whether the given auth key is valid.
   * @see getAuthKey()
   */
  public function validateAuthKey($authKey)
  {
    return $authKey == $this->auth_key;
  }

  public function generateKeys()
  {
    $this->auth_key = \Yii::$app->security->generateRandomString();
    $this->access_token = \Yii::$app->security->generateRandomString();
  }

  public function setPassword($password)
  {
    $this->password = \Yii::$app->security->generatePasswordHash($password);
  }

  public function fields()
  {
    return [
       'nombres', 'documento', 'email', 'access_token',
       'foto' => function ($model) {
         return Url::to("@web/$model->foto", true);
       }
    ];
  }

  public function getUrlFoto(){
    return Url::to("@web/$this->foto", true);
  }

  /**
   * @return \yii\rbac\Role
   */
  public function getRole()
  {
    $assing = \Yii::$app->authManager->getRolesByUser($this->id);
    $keys = array_keys($assing);
    \Yii::info($keys);
    if (isset($keys[0])) {
      return $assing[$keys[0]]->description;
    }
    return null;
  }

  public function getUsername()
  {
    return $this->nombres;
  }
}
