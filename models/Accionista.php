<?php

namespace app\models;

use Da\QrCode\QrCode;
use yii\helpers\Url;

/**
 * This is the model class for table "accionista".
 *
 * @property int $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $correo
 * @property string $telefono
 * @property string $tipo_doc
 * @property string $documento
 * @property int $acciones
 * @property int $total_pagar
 * @property int $pagado
 * @property string $info_pago
 * @property string $fecha_registro
 * @property string $reference
 */
class Accionista extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'accionista';
  }
  
  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['nombres', 'apellidos', 'correo', 'documento'], 'required'],
      [['acciones', 'total_pagar', 'pagado'], 'integer'],
      [['nombres', 'apellidos', 'correo', 'tipo_doc'], 'string', 'max' => 100],
      [['telefono', 'documento'], 'string', 'max' => 50],
      [['tipo_doc', 'documento'], 'unique', 'targetAttribute' => ['tipo_doc', 'documento']],
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'nombres' => 'Nombres',
      'apellidos' => 'Apellidos',
      'correo' => 'Correo',
      'telefono' => 'Telefono',
      'tipo_doc' => 'Tipo Doc',
      'documento' => 'Documento',
      'acciones' => 'Acciones',
      'total_pagar' => 'Total Pagar',
      'pagado' => 'Pagado',
    ];
  }
  
  public function afterFind()
  {
    if ($this->info_pago !== null) {
      $this->info_pago = \yii\helpers\Json::decode($this->info_pago);
    }
    parent::afterFind();
  }
  
  public function getQr(){
    $qr = new QrCode(Url::to(['pid-car/certificado','id' => $this->id], true));
    $qr->setSize(250)
      ->setMargin(5)
      ->useForegroundColor(51, 153, 255);
    return $qr->writeDataUri();
  }
}
