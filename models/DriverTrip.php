<?php

namespace app\models;

use app\models\base\DriverTrip as BaseDriverTrip;

/**
 * This is the model class for table "driver_trip".
 */
class DriverTrip extends BaseDriverTrip
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
       [
          [['driver_id', 'usuario_id', 'price', 'credits', 'points_generated', 'date'], 'required'],
          [['driver_id', 'usuario_id', 'price', 'credits', 'points_generated'], 'integer'],
          [['date'], 'safe']
       ]);
  }

  public function beforeValidate()
  {
    $porcent = $this->driver->driverCategory->porcent;
    $porcent_affiliate = $this->driver->driverCategory->porcent_affiliate;
    $this->credits = intval($this->price * $porcent);
    $this->points_generated = intval($this->price * $porcent_affiliate);
    return parent::beforeValidate();
  }

  public function updateValues()
  {
    $this->driver->credits -= $this->credits;
    if ($this->is_pay_points) {
      $this->driver->credits += $this->price;
      $this->usuario->afiliado->puntos -= $this->price;
    }
    $this->driver->save();
    $afiliado = $this->usuario->afiliado;
    $afiliado->puntos += $this->points_generated;
    $afiliado->save(false);
    return $this->driver->credits;
  }
}
