<?php

namespace app\models;

use app\models\base\Afiliado as BaseAfiliado;
use mysqli;
use yii\helpers\Url;

/**
 * This is the model class for table "afiliado".
 */
class Afiliado extends BaseAfiliado
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(
      parent::rules(),
      [
        [['grupo_id', 'usuario_id', 'puntos'], 'integer'],
        [['usuario_id', 'fecha_registro', 'code_invitacion'], 'required'],
        [['fecha_registro'], 'safe'],
        [['code_invitacion'], 'string', 'max' => 6],
        [['app_token'], 'string', 'max' => 1000],
      ]
    );
  }

  public function creaGrupoInicial()
  {
    $grupo = new Grupo([
      'afiliado_id' => $this->id,
      'numero_afiliados' => 0
    ]);
    $grupo->save(false);
  }

  /**
   * @return Grupo
   */
  public function getGrupoAsignado()
  {
    /** @var Grupo $last */
    $last = $this->getGrupos()
      ->orderBy(['id' => SORT_DESC])
      ->one();
    if ($last !== null and $last->numero_afiliados < 5) {
      return $last;
    } else {
      $grupo = new Grupo([
        'afiliado_id' => $this->id,
        'numero_afiliados' => 0
      ]);
      $grupo->save();
      return $grupo;
    }
  }
  public function getNombresGrupo()
  {
    /** @var Grupo $last */
    $last = $this->getGrupos()
      ->orderBy(['id' => SORT_DESC])
      ->one();
    $afiliado_id = $last->afiliado_id;
    $nombres = array();

    $mysqli = new mysqli('localhost', 'puntosgold', 'PuntosGold2021*', 'puntosgold');

    $query = $mysqli->query("SELECT * FROM grupo where afiliado_id =$afiliado_id");
    while ($valores = mysqli_fetch_array($query)) {
      $query2 = $mysqli->query("  SELECT * FROM afiliado WHERE grupo_id =$valores[id]");

      while ($valores2 = mysqli_fetch_array($query2)) {

        $query3 = $mysqli->query("  SELECT * FROM usuario WHERE id =$valores2[usuario_id]");

        while ($valores3 = mysqli_fetch_array($query3)) {
          $nombres []= $valores3['nombres'];

        }
      }
    }
    return $nombres;
  }

  /**
   * @return Afiliado[]
   * retorna los 4 afiliados padres del objeto
   */
  public function getPadresAfiliados()
  {
    $afiliados = [];
    if ($this->grupo) {
      $firtGrupo = $this->grupo->afiliado->getGrupos()->orderBy('id')->one();
      $afiliados[] = $this->grupo->afiliado; //1
      if ($firtGrupo->id == $this->grupo_id) {
        if ($afiliados[0]->grupo) {
          $afiliados[] = $afiliados[0]->grupo->afiliado; //2
          if ($afiliados[1]->grupo) {
            $afiliados[] = $afiliados[1]->grupo->afiliado; //3
            if ($afiliados[2]->grupo) {
              $afiliados[] = $afiliados[2]->grupo->afiliado; //4
            }
          }
        }
      }
    }
    return $afiliados;
  }

  /**
   * @return PuntosAfiliadoMes|array|null|\yii\db\ActiveRecord
   */
  public function getCurrentPuntosAfiliadosMes()
  {
    $model = $this->getPuntosAfiliadoMes()
      ->where(['fecha' => date('Y-m-01')])
      ->one();
    if ($model == null) {
      $model = new PuntosAfiliadoMes([
        'afiliado_id' => $this->id,
        'fecha' => date('Y-m-01'),
        'puntos' => 0,
        'pagado' => 0
      ]);
      $model->save();
    }
    return $model;
  }

  /**
   * @return PuntosReferidosMes|array|null|\yii\db\ActiveRecord
   */
  public function getCurrentPuntosReferidosMes()
  {
    $model = $this->getPuntosReferidosMes()
      ->where(['fecha' => date('Y-m-01')])
      ->one();
    if ($model == null) {
      $model = new PuntosReferidosMes([
        'afiliado_id' => $this->id,
        'fecha' => date('Y-m-01'),
        'puntos' => 0,
        'pagado' => 0
      ]);
      $model->save();
    }
    return $model;
  }

  public function getTotalPuntosConsumidos()
  {
    $compras = $this->getCompraAfiliados()
      ->sum('puntos');
    if ($compras != null) return $compras;
    return 0;
  }

  public function fields()
  {
    $fields = parent::fields();
    $fields['urlShare'] = function (Afiliado $model) {
      return Url::to(['/site/ref', 'id' => $model->id], true);
    };
    return $fields;
  }

  public function getUrlShare()
  {
    return Url::to(['/site/ref', 'id' => $this->id], true);
  }

  public function extraFields()
  {
    return ['datos' => 'afiDatos', 'usuario'];
  }
}
