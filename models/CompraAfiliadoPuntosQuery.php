<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CompraAfiliadoPuntos]].
 *
 * @see CompraAfiliadoPuntos
 */
class CompraAfiliadoPuntosQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return CompraAfiliadoPuntos[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CompraAfiliadoPuntos|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}