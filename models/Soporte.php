<?php

namespace app\models;

use app\models\base\Soporte as BaseSoporte;

/**
 * This is the model class for table "soporte".
 */
class Soporte extends BaseSoporte
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['usuario_id', 'active', 'fecha'], 'required'],
        [['usuario_id', 'active'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

  public function extraFields()
  {
    return [
      'chats'=>'chatsLimit'
    ];
  }

  public function getChatsLimit(){
    return $this->getChats();
  }
}
