<?php

namespace app\models;

use app\models\base\DriverCategoryState as BaseDriverCategoryState;

/**
 * This is the model class for table "driver_category_state".
 */
class DriverCategoryState extends BaseDriverCategoryState
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
       [
          [['driver_category_id', 'departamento_id', 'porcent', 'porcent_affiliate'], 'required'],
          [['driver_category_id', 'departamento_id'], 'integer'],
          [['porcent', 'porcent_affiliate'], 'number'],
          [['driver_category_id', 'departamento_id'], 'unique', 'targetAttribute' => ['driver_category_id', 'departamento_id'], 'message' => 'The combination of Driver Category ID and Departamento ID has already been taken.']
       ]);
  }

}
