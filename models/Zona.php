<?php

namespace app\models;

/**
 * This is the model class for table "zona".
 *
 * @property integer $id
 * @property integer $ciudad_id
 * @property string $nombre
 *
 * @property AdminZona[] $adminZonas
 * @property Ciudad $ciudad
 */
class Zona extends \yii\db\ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'zona';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['ciudad_id', 'nombre'], 'required'],
      [['ciudad_id'], 'integer'],
      [['nombre'], 'string', 'max' => 50],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'ciudad_id' => 'Ciudad ID',
      'nombre' => 'Nombre',
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getAdminZonas()
  {
    return $this->hasMany(AdminZona::className(), ['zona_id' => 'id']);
  }

  public function getCiudad(){
    return $this->hasOne(Ciudad::className(),['id'=>'ciudad_id']);
  }
}
