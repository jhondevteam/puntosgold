<?php

namespace app\models;

use app\helper\AppHelper;
use phpDocumentor\Reflection\Types\Null_;
use Yii;
use yii\bootstrap\Html;

/**
 * This is the model class for table "texto".
 *
 * @property integer $id
 * @property string $glue
 * @property string $text
 */
class Texto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'texto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['glue'], 'required'],
            [['glue'], 'string', 'max' => 100],
            [['text'], 'string', 'max' => 2000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'glue' => Yii::t('app', 'Glue'),
            'text' => Yii::t('app', 'Text'),
        ];
    }

    public static function printText($glue,$tag){
      $model = static::findOne(['glue'=>$glue]);
      if ($model != null and Yii::$app->user->can(AppHelper::SUPER_ADMIN)){
        return Html::tag($tag,$model->text,['class'=>'text_edit','contenteditable'=>true,'data-id'=>$model->id,'data-glue'=>$glue]);
      }elseif ( Yii::$app->user->can(AppHelper::SUPER_ADMIN)){
        return Html::tag($tag,"Ingrese un texto",['data-glue'=>$glue,'class'=>'text_edit','contenteditable'=>true,]);
      }elseif ($model != null)
        return Html::tag($tag,$model->text);
      return Html::tag($tag,'');
    }
}
