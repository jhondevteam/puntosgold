<?php

namespace app\models;

use app\models\base\CobroEmpresaEmpleado as BaseCobroEmpresaEmpleado;

/**
 * This is the model class for table "cobro_empresa_empleado".
 */
class CobroEmpresaEmpleado extends BaseCobroEmpresaEmpleado
{
  /**
   * @inheritdoc
   */
  public function rules()
  {
    return array_replace_recursive(parent::rules(),
      [
        [['empleado_id', 'fecha', 'monto', 'saldo','monto_cobrar'], 'required'],
        [['empleado_id', 'monto', 'saldo'], 'integer'],
        [['fecha'], 'safe']
      ]);
  }

}
