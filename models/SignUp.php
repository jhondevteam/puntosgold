<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 6/06/17
 * Time: 12:24 AM
 */

namespace app\models;


use app\helper\AppHelper;
use Yii;
use yii\base\Model;

class SignUp extends Model
{
  public $nombres;
  public $documento;
  public $email;
  public $password;
  public $coderef;
  public $file;
  public $facebook_id;

  public $driver_category_id;
  public $access_token;
  public $state_id;

  public $codetaxi;

  public $empresa_transportadora_id;

  public function rules()
  {
    return [
       [['nombres', 'documento', 'email', 'password'], 'required'],
       [['access_token'], 'string', 'max' => 250],
       [['file', 'facebook_id', 'coderef', 'codetaxi'], 'safe'],
       [['driver_category_id', 'access_token', 'state_id'], 'required', 'on' => 'driverRegister'],
       ['empresa_transportadora_id', 'safe', 'on' => 'driverRegister']
    ];
  }

  /**
   * @return array|bool
   * @throws \yii\base\Exception
   */
  public function signUp()
  {
    if ($this->validate()) {
      $usuario = new Usuario([
         'documento' => $this->documento,
         'email' => $this->email,
         'nombres' => $this->nombres,
         'facebook_id' => $this->facebook_id,
      ]);
      $usuario->loadDefaultValues(true);
      $usuario->setPassword($this->password);
      $usuario->generateKeys();
      Yii::info($this->access_token);
      if (!empty($this->access_token))
        $usuario->access_token = $this->access_token;
      if ($this->file !== null) {
        $this->file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $this->file));
        $usuario->foto = AppHelper::obtenerDirectorio('img/perfil/') .
           Yii::$app->security->generateRandomString(20) . ".jpg";
      }
      if ($usuario->save()) {
        if ($this->file !== null)
          file_put_contents($usuario->foto, $this->file);
        $referido = Afiliado::findOne(['code_invitacion' => $this->codetaxi]);
        if ($referido == null) {
          $referido = Afiliado::find()
             ->joinWith(['grupos'])
             ->where(['grupo.numero_afiliados' => 0])
             ->orderBy('fecha_registro')
             ->one();
        }
        $grupo_id = null;
        if ($referido) {
          $grupo_id = $referido->getGrupoAsignado();
          $grupo_id->numero_afiliados++;
          $grupo_id->save(false);
          $referido->puntos += 100;
          $referido->save();
        }
        $code_invitacion = AppHelper::getCodeInvitacion();
        while ($this->existCode($code_invitacion)) {
          $code_invitacion = AppHelper::getCodeInvitacion();
        }
        $afiliado = new Afiliado([
           'grupo_id' => $grupo_id != null ? $grupo_id->id : null,
           'puntos' => 100,
           'code_invitacion' => $code_invitacion,
           'fecha_registro' => date('Y-m-d H:i:s'),
           'usuario_id' => $usuario->id
        ]);
        $afiliado->save(false);
        return [
           'user' => $usuario,
           'afiliado' => $afiliado
        ];
      } else
        Yii::info($usuario->getErrors());
    }
    return false;
  }

  private function existCode($code)
  {
    return Afiliado::findOne(['code_invitacion' => $code]) != null;
  }

  public function checkUser()
  {
    $user = Usuario::findOne(['documento' => $this->documento, 'email' => $this->email]);
    if ($user != null && $user->afiliado) {
      $user->access_token = $this->access_token;
      $user->save(false);
      return $user;
    }
    return false;
  }

  public function signUpDriver()
  {
    if ($this->validate()) {
      $tr = Yii::$app->db->beginTransaction();
      $usuario = new Usuario([
         'documento' => $this->documento,
         'email' => $this->email,
         'nombres' => $this->nombres,
         'facebook_id' => $this->facebook_id,
         'access_token' => $this->access_token,
         'auth_key' => Yii::$app->security->generateRandomString(50),
      ]);
      $usuario->loadDefaultValues(true);
      $usuario->setPassword($this->password);
      if ($usuario->save()) {
        $driverCategoryState = DriverCategoryState::find()
           ->where(['departamento_id' => $this->state_id, 'driver_category_id' => $this->driver_category_id])
           ->one();
        $invitador = Driver::findOne(['code' => $this->codetaxi]);
        $driver = new Driver([
           'usuario_id' => $usuario->id,
           'driver_category_id' => $this->driver_category_id,
           'credits' => 5000,
           'code' => AppHelper::getCodeInvitacion(),
           'active' => 0,
           'departamento_id' => $this->state_id
        ]);
        if (!empty($this->empresa_transportadora_id) && $this->empresa_transportadora_id != 'null') {
          $driver->empresa_transportadora_id = $this->empresa_transportadora_id;
        }
        if ($invitador != null)
          $driver->driver_invite_id = $invitador->id;
        if ($driverCategoryState != null) {
          $driver->porcent = $driverCategoryState->porcent;
        } else {
          $driver->porcent = $driver->driverCategory->porcent;
        }
        if ($driver->save(false)) {
          $tr->commit();
        } else {
          $tr->rollBack();
          Yii::info($driver->getErrors());
          return false;
        }
        return [
           'user' => $usuario,
           'driver' => $driver
        ];
      }
    }
    return false;
  }

  public function attributeLabels()
  {
    return [
       'coderef' => 'Codigo de referido',
       'documento' => 'Numero de documento',
       'email' => 'Correo',
       'password' => 'Contraseña'
    ];
  }
}