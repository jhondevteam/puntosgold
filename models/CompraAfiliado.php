<?php

namespace app\models;

use \app\models\base\CompraAfiliado as BaseCompraAfiliado;

/**
 * This is the model class for table "compra_afiliado".
 */
class CompraAfiliado extends BaseCompraAfiliado
{
  public $runAfterSave = true;

  public function beforeValidate()
  {
    if ($this->runAfterSave){
      $this->saldo_puntos = $this->afiliado->puntos;
      if($this->getEmpresa()->mode == Empresa::MODE_PORCENTAJE){
        $this->puntos = intval(($this->monto * $this->empleado->empresa->porcentaje_afiliado)/2);
        $this->monto_cobra_empresa = intval($this->monto * $this->empleado->empresa->porcentaje);
      }else{
        $this->puntos = intval(($this->getEmpresa()->puntos_afiliado)/2);
        $this->monto_cobra_empresa = $this->getEmpresa()->puntos;
      }
    }
    return parent::beforeValidate();
  }

  public function afterSave($insert, $changedAttributes)
  {
    if ($this->runAfterSave) {
      $afiliado = $this->afiliado;
      $afiliado->puntos += $this->puntos;
      $afiliado->save(false);
      $padres = $this->afiliado->getPadresAfiliados();
      foreach ($padres as $key => $padreAfiliado) {
        $porcent = floatval('0.10');
        if ($key == 0) {
          $porcent = floatval('0.20');
        }
        if ($key == 0)
          $puntosDelMes = $padreAfiliado->getCurrentPuntosAfiliadosMes();
        else
          $puntosDelMes = $padreAfiliado->getCurrentPuntosReferidosMes();

        $nuevosPuntos = ($this->puntos*2) * $porcent;
        $puntosDelMes->puntos += $nuevosPuntos;
        $puntosDelMes->save(false);
        \Yii::info("Nuevos puntos para : " . $padreAfiliado->id . " - " . $nuevosPuntos);
      }
    }
    parent::afterSave($insert, $changedAttributes);
  }

  public function extraFields()
  {
    return [
      'empleado'=>'empleado',
      'empresa'=>'empresa',
      'cliente'=>'cliente'
    ];
  }

  public function getEmpresa(){
    return $this->empleado->empresa;
  }

  public function getCliente(){
    return $this->afiliado->usuario->nombres;
  }
}
