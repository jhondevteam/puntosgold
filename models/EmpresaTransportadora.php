<?php

namespace app\models;

use \app\models\base\EmpresaTransportadora as BaseEmpresaTransportadora;
use yii\helpers\Json;

/**
 * This is the model class for table "empresa_transportadora".
 */
class EmpresaTransportadora extends BaseEmpresaTransportadora
{
  public function beforeValidate()
  {
    if (is_array($this->departamento_ids))
      $this->departamento_ids = Json::encode($this->departamento_ids);
    return parent::beforeValidate();
  }
}
