Object.defineProperty(Array.prototype, 'chunk', {
  value: function(chunkSize) {
    var array = this;
    return [].concat.apply([],
       array.map(function(elem, i) {
         return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
       })
    );
  }
});

$(function () {
  var database = firebase.database();
  database.ref('drivers').once('value').then(function (value) {
    var data = value.val();
    console.log(data);
    var keys = Object.keys(data);
    var post = [];
    console.log(keys.length);
    keys.forEach(function (value1) {
      post.push({
        uid: value1,
        telefono: data[value1].phoneNumber,
        correo: data[value1].email,
        nombre: data[value1].name,
        contrasena: data[value1].password
      });
    });
    var posts = post.chunk(100);
    console.log(JSON.stringify(post));
    posts.forEach(function (post) {
      console.log(post.length);
      $.post('update-telefono', $.param({data: post}), function () {

      });
    });
  });
});