/**
 * Created by Andres santos on 12/05/2017.
 */
$(document).ready(function () {
  var editableBlocks = $('.text_edit');
  editableBlocks.each(function (index, el) {
    CKEDITOR.inline(el, {
      on: {
        blur: function (event) {
          var data = event.editor.getData();
          var glue = $(el).data('glue');
          var url = baseUrl+"/text/update?glue=" + glue;
          $.ajax({
            type: "POST",
            url: url,
            data: $.param({Texto: {text: data}})
          });
        }
      }
    });
  });
});

