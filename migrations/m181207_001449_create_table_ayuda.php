<?php

use yii\db\Migration;

class m181207_001449_create_table_ayuda extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ayuda}}', [
            'id' => $this->primaryKey(),
            'titulo' => $this->string()->notNull(),
            'url_video' => $this->string(),
            'descripcion' => $this->string()->notNull(),
            'destino' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%ayuda}}');
    }
}
