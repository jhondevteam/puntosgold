<?php

use yii\db\Migration;

class m181207_001449_create_table_compra_afiliado_puntos extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%compra_afiliado_puntos}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'empleado_id' => $this->integer()->notNull(),
            'pago_empresa_id' => $this->integer(),
            'fecha' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'monto' => $this->integer()->notNull(),
            'puntos' => $this->integer()->notNull(),
            'saldo_puntos' => $this->integer()->notNull()->defaultValue('0'),
            'comentario' => $this->string(),
            'confirmado' => $this->tinyInteger()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%compra_afiliado_puntos}}', 'afiliado_id');
        $this->createIndex('cobro_empresa_id', '{{%compra_afiliado_puntos}}', 'pago_empresa_id');
        $this->createIndex('empleado_id', '{{%compra_afiliado_puntos}}', 'empleado_id');
    }

    public function down()
    {
        $this->dropTable('{{%compra_afiliado_puntos}}');
    }
}
