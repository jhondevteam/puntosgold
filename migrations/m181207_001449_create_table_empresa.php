<?php

use yii\db\Migration;

class m181207_001449_create_table_empresa extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%empresa}}', [
            'id' => $this->primaryKey(),
            'admin_zona_id' => $this->integer(),
            'categoria_id' => $this->integer()->notNull(),
            'nombre' => $this->string()->notNull(),
            'url_logo' => $this->string()->notNull(),
            'descripcion' => $this->text()->notNull(),
            'nit' => $this->string()->notNull(),
            'estado' => $this->tinyInteger()->notNull(),
            'mode' => $this->string()->notNull()->defaultValue(''),
            'porcentaje' => $this->float(),
            'porcentaje_afiliado' => $this->float(),
            'puntos' => $this->integer(),
            'puntos_afiliado' => $this->integer(),
            'nivel_importancia' => $this->tinyInteger()->notNull()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('admin_zona_id', '{{%empresa}}', 'admin_zona_id');
        $this->createIndex('categoria_id', '{{%empresa}}', 'categoria_id');
    }

    public function down()
    {
        $this->dropTable('{{%empresa}}');
    }
}
