<?php

use yii\db\Migration;

class m181207_001450_create_table_video extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%video}}', [
            'id' => $this->primaryKey(),
            'descripcion' => $this->string()->notNull(),
            'empresa_id' => $this->integer()->notNull(),
            'url_video' => $this->string()->notNull(),
            'activo' => $this->tinyInteger()->notNull(),
        ], $tableOptions);

        $this->createIndex('empresa_id', '{{%video}}', 'empresa_id');
        $this->addForeignKey('video_ibfk_1', '{{%video}}', 'empresa_id', '{{%empresa}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%video}}');
    }
}
