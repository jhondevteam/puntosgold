<?php

use yii\db\Migration;

class m181207_001447_create_table_afiliado extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%afiliado}}', [
            'id' => $this->primaryKey(),
            'grupo_id' => $this->integer(),
            'usuario_id' => $this->integer()->notNull(),
            'fecha_registro' => $this->date()->notNull(),
            'puntos' => $this->integer()->defaultValue('0'),
            'code_invitacion' => $this->string()->notNull(),
            'app_token' => $this->string(),
            'city' => $this->string(),
            'sound' => $this->tinyInteger()->notNull()->defaultValue('1'),
        ], $tableOptions);

        $this->createIndex('grupo_id', '{{%afiliado}}', 'grupo_id');
        $this->createIndex('usuario_id_2', '{{%afiliado}}', 'usuario_id', true);
        $this->createIndex('usuario_id', '{{%afiliado}}', 'usuario_id');
        $this->addForeignKey('afiliado_ibfk_1', '{{%afiliado}}', 'usuario_id', '{{%usuario}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('afiliado_ibfk_2', '{{%afiliado}}', 'grupo_id', '{{%grupo}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable('{{%afiliado}}');
    }
}
