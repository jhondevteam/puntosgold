<?php

use yii\db\Migration;

class m181207_001449_create_table_departamento extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%departamento}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'pais_id' => $this->integer()->notNull(),
            'activo' => $this->tinyInteger()->notNull()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%departamento}}');
    }
}
