<?php

use yii\db\Migration;

class m181207_001450_create_table_preguntas_respuestas extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%preguntas_respuestas}}', [
            'id' => $this->primaryKey(),
            'pregunta' => $this->string()->notNull(),
            'respuesta' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%preguntas_respuestas}}');
    }
}
