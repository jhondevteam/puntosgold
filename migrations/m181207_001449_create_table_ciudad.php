<?php

use yii\db\Migration;

class m181207_001449_create_table_ciudad extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ciudad}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'departamento_id' => $this->integer()->notNull(),
            'principal' => $this->tinyInteger()->notNull()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('departamento_id', '{{%ciudad}}', 'departamento_id');
    }

    public function down()
    {
        $this->dropTable('{{%ciudad}}');
    }
}
