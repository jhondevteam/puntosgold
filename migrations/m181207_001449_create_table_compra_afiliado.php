<?php

use yii\db\Migration;

class m181207_001449_create_table_compra_afiliado extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%compra_afiliado}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'empleado_id' => $this->integer()->notNull(),
            'cobro_empresa_id' => $this->integer(),
            'pago_empresa_id' => $this->integer(),
            'pago_puntos' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'fecha' => $this->dateTime()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'monto' => $this->integer()->notNull(),
            'monto_cobra_empresa' => $this->integer()->notNull(),
            'puntos' => $this->integer()->notNull(),
            'saldo_puntos' => $this->integer()->notNull()->defaultValue('0'),
            'comentario' => $this->string(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%compra_afiliado}}', 'afiliado_id');
        $this->createIndex('cobro_empresa_id', '{{%compra_afiliado}}', 'cobro_empresa_id');
        $this->createIndex('empleado_id', '{{%compra_afiliado}}', 'empleado_id');
        $this->createIndex('pago_empresa_id', '{{%compra_afiliado}}', 'pago_empresa_id');
        $this->addForeignKey('compra_afiliado_ibfk_1', '{{%compra_afiliado}}', 'afiliado_id', '{{%afiliado}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('compra_afiliado_ibfk_2', '{{%compra_afiliado}}', 'empleado_id', '{{%empleado}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('compra_afiliado_ibfk_3', '{{%compra_afiliado}}', 'cobro_empresa_id', '{{%cobro_empresa_empleado}}', 'id', 'SET NULL', 'SET NULL');
        $this->addForeignKey('compra_afiliado_ibfk_4', '{{%compra_afiliado}}', 'pago_empresa_id', '{{%pago_empresa}}', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        $this->dropTable('{{%compra_afiliado}}');
    }
}
