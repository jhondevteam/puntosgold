<?php

use yii\db\Migration;

class m181207_001451_create_table_zona extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%zona}}', [
            'id' => $this->primaryKey(),
            'ciudad_id' => $this->integer()->notNull(),
            'nombre' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%zona}}');
    }
}
