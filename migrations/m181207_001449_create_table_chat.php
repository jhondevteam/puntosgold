<?php

use yii\db\Migration;

class m181207_001449_create_table_chat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%chat}}', [
            'id' => $this->primaryKey(),
            'soporte_id' => $this->integer()->notNull(),
            'userId' => $this->integer()->notNull(),
            'message' => $this->text()->notNull(),
            'updateDate' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->createIndex('soporte_id', '{{%chat}}', 'soporte_id');
        $this->createIndex('usuario_id', '{{%chat}}', 'userId');
    }

    public function down()
    {
        $this->dropTable('{{%chat}}');
    }
}
