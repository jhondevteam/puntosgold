<?php

use yii\db\Migration;

class m181207_001449_create_table_empleado extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%empleado}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer()->notNull(),
            'usuario_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('empresa_id', '{{%empleado}}', 'empresa_id');
        $this->createIndex('usuario_id_2', '{{%empleado}}', 'usuario_id', true);
        $this->createIndex('usuario_id', '{{%empleado}}', 'usuario_id');
    }

    public function down()
    {
        $this->dropTable('{{%empleado}}');
    }
}
