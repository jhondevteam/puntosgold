<?php

use yii\db\Migration;

class m181207_001450_create_table_texto extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%texto}}', [
            'id' => $this->primaryKey(),
            'glue' => $this->string()->notNull(),
            'text' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%texto}}');
    }
}
