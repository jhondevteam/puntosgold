<?php

use yii\db\Migration;

class m181207_001450_create_table_transferencia_puntos extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%transferencia_puntos}}', [
            'id' => $this->primaryKey(),
            'de_afiliado_id' => $this->integer()->notNull(),
            'para_afiliado_id' => $this->integer()->notNull(),
            'monto' => $this->integer()->notNull(),
            'fecha' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->createIndex('de_afiliado_id', '{{%transferencia_puntos}}', 'de_afiliado_id');
        $this->createIndex('para_afiliado_id', '{{%transferencia_puntos}}', 'para_afiliado_id');
        $this->addForeignKey('transferencia_puntos_ibfk_1', '{{%transferencia_puntos}}', 'de_afiliado_id', '{{%afiliado}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('transferencia_puntos_ibfk_2', '{{%transferencia_puntos}}', 'para_afiliado_id', '{{%afiliado}}', 'id', 'RESTRICT', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%transferencia_puntos}}');
    }
}
