<?php

use yii\db\Migration;

class m181207_001450_create_table_soporte extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%soporte}}', [
            'id' => $this->primaryKey(),
            'usuario_id' => $this->integer()->notNull(),
            'active' => $this->tinyInteger()->notNull(),
            'fecha' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%soporte}}', 'usuario_id');
    }

    public function down()
    {
        $this->dropTable('{{%soporte}}');
    }
}
