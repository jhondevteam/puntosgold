<?php

use yii\db\Migration;

class m181207_001449_create_table_pago_empresa extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pago_empresa}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer()->notNull(),
            'admin_zona_id' => $this->integer(),
            'comision_admin_zona' => $this->bigInteger()->notNull(),
            'fecha' => $this->dateTime()->notNull(),
            'fecha_pago' => $this->date(),
            'monto' => $this->integer()->notNull(),
            'pagado' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'url_file' => $this->string(),
        ], $tableOptions);

        $this->createIndex('admin_zona_id', '{{%pago_empresa}}', 'admin_zona_id');
        $this->createIndex('empresa_id', '{{%pago_empresa}}', 'empresa_id');
    }

    public function down()
    {
        $this->dropTable('{{%pago_empresa}}');
    }
}
