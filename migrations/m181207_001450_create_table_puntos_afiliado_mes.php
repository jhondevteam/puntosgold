<?php

use yii\db\Migration;

class m181207_001450_create_table_puntos_afiliado_mes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%puntos_afiliado_mes}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'puntos' => $this->integer()->notNull(),
            'pagado' => $this->tinyInteger()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%puntos_afiliado_mes}}', 'afiliado_id');
    }

    public function down()
    {
        $this->dropTable('{{%puntos_afiliado_mes}}');
    }
}
