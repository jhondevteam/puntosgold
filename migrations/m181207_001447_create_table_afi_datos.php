<?php

use yii\db\Migration;

class m181207_001447_create_table_afi_datos extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%afi_datos}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'nacimiento' => $this->date()->notNull(),
            'telefono' => $this->string()->notNull(),
            'direccion' => $this->string()->notNull(),
            'sexo' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%afi_datos}}', 'afiliado_id', true);
        $this->addForeignKey('afi_datos_ibfk_1', '{{%afi_datos}}', 'afiliado_id', '{{%afiliado}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%afi_datos}}');
    }
}
