<?php

use yii\db\Migration;

class m181207_001449_create_table_foto extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%foto}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer()->notNull(),
            'url_foto' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('empresa_id', '{{%foto}}', 'empresa_id');
    }

    public function down()
    {
        $this->dropTable('{{%foto}}');
    }
}
