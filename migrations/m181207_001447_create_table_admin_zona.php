<?php

use yii\db\Migration;

class m181207_001447_create_table_admin_zona extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%admin_zona}}', [
            'id' => $this->primaryKey(),
            'usuario_id' => $this->integer()->notNull(),
            'telefonos' => $this->string()->notNull(),
            'direccion' => $this->string()->notNull(),
            'zona_id' => $this->integer()->notNull(),
            'porcentaje' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('usuario_id', '{{%admin_zona}}', 'usuario_id');
        $this->createIndex('usuario_id_2', '{{%admin_zona}}', 'usuario_id', true);
        $this->createIndex('zona_id', '{{%admin_zona}}', 'zona_id');
        $this->addForeignKey('admin_zona_ibfk_1', '{{%admin_zona}}', 'usuario_id', '{{%usuario}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('admin_zona_ibfk_2', '{{%admin_zona}}', 'zona_id', '{{%zona}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%admin_zona}}');
    }
}
