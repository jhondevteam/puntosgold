<?php

use yii\db\Migration;

class m181207_001449_create_table_cobro_empresa_empleado extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cobro_empresa_empleado}}', [
            'id' => $this->primaryKey(),
            'empleado_id' => $this->integer()->notNull(),
            'fecha' => $this->date()->notNull(),
            'monto_cobrar' => $this->integer()->notNull(),
            'monto' => $this->integer()->notNull(),
            'saldo' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('empleado_id', '{{%cobro_empresa_empleado}}', 'empleado_id');
    }

    public function down()
    {
        $this->dropTable('{{%cobro_empresa_empleado}}');
    }
}
