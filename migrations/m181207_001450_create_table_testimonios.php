<?php

use yii\db\Migration;

class m181207_001450_create_table_testimonios extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%testimonios}}', [
            'id' => $this->primaryKey(),
            'titulo' => $this->string()->notNull(),
            'descripcion' => $this->string()->notNull(),
            'nombre' => $this->string()->notNull(),
            'url_video' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%testimonios}}');
    }
}
