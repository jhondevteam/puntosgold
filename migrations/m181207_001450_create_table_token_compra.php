<?php

use yii\db\Migration;

class m181207_001450_create_table_token_compra extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%token_compra}}', [
            'id' => $this->primaryKey(),
            'compra_afiliado_puntos_id' => $this->integer(),
            'afiliado_id' => $this->integer()->notNull(),
            'token' => $this->string()->notNull(),
            'fecha' => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%token_compra}}', 'afiliado_id');
        $this->createIndex('compra_afiliado_puntos_id_2', '{{%token_compra}}', 'compra_afiliado_puntos_id');
        $this->createIndex('compra_afiliado_puntos_id', '{{%token_compra}}', 'compra_afiliado_puntos_id');
    }

    public function down()
    {
        $this->dropTable('{{%token_compra}}');
    }
}
