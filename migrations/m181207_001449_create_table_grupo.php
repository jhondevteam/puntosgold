<?php

use yii\db\Migration;

class m181207_001449_create_table_grupo extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%grupo}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'numero_afiliados' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%grupo}}', 'afiliado_id');
    }

    public function down()
    {
        $this->dropTable('{{%grupo}}');
    }
}
