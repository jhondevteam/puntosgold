<?php

use yii\db\Migration;

class m181207_001449_create_table_notificacion_entrega extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notificacion_entrega}}', [
            'id' => $this->primaryKey(),
            'afiliado_id' => $this->integer()->notNull(),
            'sucursal_id' => $this->integer()->notNull(),
            'app_token' => $this->string()->notNull(),
            'resultado' => $this->string()->notNull(),
            'fecha' => $this->date()->notNull(),
        ], $tableOptions);

        $this->createIndex('afiliado_id', '{{%notificacion_entrega}}', 'afiliado_id');
        $this->createIndex('sucursal_id', '{{%notificacion_entrega}}', 'sucursal_id');
    }

    public function down()
    {
        $this->dropTable('{{%notificacion_entrega}}');
    }
}
