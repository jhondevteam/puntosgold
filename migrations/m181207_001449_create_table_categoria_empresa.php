<?php

use yii\db\Migration;

class m181207_001449_create_table_categoria_empresa extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%categoria_empresa}}', [
            'id' => $this->primaryKey(),
            'nombre' => $this->string()->notNull(),
            'url_banner' => $this->string()->notNull(),
            'url_icon' => $this->string()->notNull(),
            'activa' => $this->tinyInteger()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%categoria_empresa}}');
    }
}
