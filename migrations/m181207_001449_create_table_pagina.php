<?php

use yii\db\Migration;

class m181207_001449_create_table_pagina extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pagina}}', [
            'id' => $this->primaryKey(),
            'glue' => $this->string()->notNull(),
            'titulo' => $this->string()->notNull(),
            'contenido' => $this->text()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%pagina}}');
    }
}
