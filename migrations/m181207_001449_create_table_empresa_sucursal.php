<?php

use yii\db\Migration;

class m181207_001449_create_table_empresa_sucursal extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%empresa_sucursal}}', [
            'id' => $this->primaryKey(),
            'empresa_id' => $this->integer()->notNull(),
            'ciudad_id' => $this->integer()->notNull(),
            'descripcion' => $this->string(),
            'direccion' => $this->string()->notNull(),
            'lat' => $this->string()->notNull(),
            'lng' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createIndex('empresa_id', '{{%empresa_sucursal}}', 'empresa_id');
        $this->createIndex('ciudad_id', '{{%empresa_sucursal}}', 'ciudad_id');
    }

    public function down()
    {
        $this->dropTable('{{%empresa_sucursal}}');
    }
}
