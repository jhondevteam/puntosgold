<?php

use yii\db\Migration;

class m181207_001450_create_table_usuario extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%usuario}}', [
            'id' => $this->primaryKey(),
            'documento' => $this->string()->notNull(),
            'nombres' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'foto' => $this->string()->notNull()->defaultValue('uploads/img/nophoto.png'),
            'estado' => $this->tinyInteger()->notNull()->defaultValue('1'),
            'password' => $this->string()->notNull(),
            'password_reset_token' => $this->string(),
            'auth_key' => $this->string()->notNull(),
            'access_token' => $this->string()->notNull(),
            'facebook_id' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%usuario}}');
    }
}
