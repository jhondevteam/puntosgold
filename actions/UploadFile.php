<?php
namespace app\actions;

use app\models\Archivo;
use app\models\Imagenes;
use app\models\UrlImagen;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * User: Jhon
 * Date: 28/04/2016
 * Time: 6:49 PM
 */
class UploadFile extends \yii\base\Action
{
  public $folder;

  public function run()
  {
    $archivo = new Archivo();
    $archivo->load(Yii::$app->request->post());
    if ($archivo->file) {
      $archivo->file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $archivo->file));
    } else {
      $archivo->file = UploadedFile::getInstance($archivo, 'file');
      $archivo->scenario = 'fileUP';
    }

    if (!file_exists('uploads'))
      mkdir('uploads');
    if (!file_exists("uploads/$this->folder")) {
      $folders = explode('/', $this->folder);
      $path = 'uploads/';
      foreach ($folders as $folder) {
        if (!file_exists($path . $folder)) {
          mkdir($path . $folder);
        }
        $path .= $folder . '/';
      }
    }
    if (Yii::$app->request->isAjax) {
      Yii::$app->response->format = Response::FORMAT_JSON;
      return ActiveForm::validate($archivo);
    }
    Yii::info(Yii::$app->request->getMethod());
    if ($archivo->validate()) {
      if ($archivo->file instanceof UploadedFile) {
        $archivo->ruta = 'uploads/' . $this->folder . "/" .
          Yii::$app->security->generateRandomString(20) . "." . $archivo->file->extension;
        if ($archivo->save()) {
          $archivo->file->saveAs($archivo->ruta);
          Yii::$app->session->setFlash('success', "Archivo agregado con exito");
          return $this->controller->redirect(Yii::$app->request->referrer);
        }
      } else if ($archivo->file) {
        $archivo->ruta = 'uploads/' . $this->folder . "/" .
          Yii::$app->security->generateRandomString(20) . ".jpg";
        if ($archivo->save()) {
          file_put_contents($archivo->ruta, $archivo->file);
          return ['success' => 'ok'];
        }
      } else {
        return Html::errorSummary($archivo);
      }
    } elseif (Yii::$app->request->get('api')) {
      return [
        'success' => Html::errorSummary($archivo)
      ];
    } else {
      Yii::$app->session->setFlash('danger', Html::errorSummary($archivo));
      return $this->controller->redirect(Yii::$app->request->referrer);
    }

  }
}
