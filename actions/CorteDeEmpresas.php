<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 12/10/17
 * Time: 02:08 PM
 */

namespace app\actions;


use app\models\Empresa;
use app\models\PagoEmpresa;
use yii\base\Action;

class CorteDeEmpresas extends Action
{
  public function run()
  {

  }

  /**
   * @param $empresa Empresa
   * @return PagoEmpresa|bool
   */
  public function creaCorteEmpresa($empresa)
  {
    $saldo = $empresa->getSaldoPagar();
    $puntos = $empresa->getPendienteTotalPuntosPagar();
    if ($saldo > $puntos) {
      $ventas = $empresa->getVentasPagar();
      $corte = new PagoEmpresa([
        'fecha' => date('Y-m-d H:i:s'),
        'pagado' => 0,
        'monto' => ($saldo-$puntos),
        'empresa_id' => $empresa->id,
        'admin_zona_id' => $empresa->admin_zona_id,
        'comision_admin_zona' => $empresa->getGanaciaAdminstrador()
      ]);
      if ($corte->save()){
        foreach ($ventas as $venta) {
          $venta->pago_empresa_id = $corte->id;
          $venta->save(false);
        }
        return $corte;
      }else
        \Yii::info($corte->getErrors());
      return false;
    }
    return false;
  }
}