<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/09/17
 * Time: 04:53 PM
 */

namespace app\actions;


use app\models\Afiliado;
use app\models\PuntosAfiliadoMes;
use app\models\PuntosReferidosMes;
use yii\base\Action;

class CorteDePuntos extends Action
{
  public function run()
  {
    $dia = date('d');
//    if ($dia == '01') {
      $fecha = '2017-08-01';
      $ultimoDia = strtotime('-1 day', strtotime($fecha));
      $primerDia = strtotime('-1 month', strtotime($fecha));
      $ultimoDia = date('Y-m-d 23:59:00', $ultimoDia);
      $fechaReferidos = date('Y-m-d', $primerDia);
      $primerDia = date('Y-m-d 00:00:00', $primerDia);
      \Yii::info($ultimoDia);
      \Yii::info($primerDia);
      \Yii::info($fechaReferidos);
      $ids = Afiliado::find()->select('id')
        ->createCommand()
        ->queryColumn();
      foreach ($ids as $id) {
        $afiliado = Afiliado::findOne($id);
        /** @var PuntosAfiliadoMes $referidos */
        $referidos = $afiliado->getPuntosAfiliadoMes()
          ->where(['fecha' => $fechaReferidos])
          ->one();
        if ($referidos != null) {
          $afiliado->puntos += $referidos->puntos;
          $referidos->pagado = 1;
          $referidos->save(false);
        }
        /** @var PuntosReferidosMes $referidos */
        $referidos = $afiliado->getPuntosReferidosMes()
          ->where(['fecha' => $fechaReferidos])
          ->one();
        $puntosMes = $afiliado->getCompraAfiliados()
          ->where(['between', 'fecha', $primerDia, $ultimoDia])
          ->sum('puntos');
        if ($referidos != null and $puntosMes > 1000) {
          $afiliado->puntos += $referidos->puntos;
          $referidos->pagado = 1;
          $referidos->save(false);
        }
      }
//    }
  }
}