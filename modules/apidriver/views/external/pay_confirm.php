<?php

/* @var $this \yii\web\View */
/* @var $model \app\models\DriverPurchase|null */
?>
<section id="service" class="service-area section-big">
  <div>
    <div class="col-lg-12" style="padding: 1.5em">
      <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'attributes' => [
          'id',
          'reference',
          [
            'label' => 'Conductor',
            'attribute' => 'driver.usuario.nombres'
          ],
          [
            'label' => 'Documento',
            'attribute' => 'driver.usuario.documento'
          ],
          [
            'label' => 'Valor de la recarga',
            'attribute' => 'amount',
            'format' =>'currency'
          ],
          [
            'label' => 'Estado',
            'value' => $model->is_confirm? 'Confimado' : 'Pendiente'
          ],
          [
            'label' => 'Creado',
            'attribute' => 'date_generate'
          ],
          [
            'label' => 'Pagado',
            'attribute' => 'date_confirm'
          ]
        ]
      ]) ?>
    </div>
  </div>
</section>
<div class="clearfix"></div>
