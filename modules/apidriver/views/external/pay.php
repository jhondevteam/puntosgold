<?php
/* @var $this \yii\web\View */
/* @var $id int */
/* @var $signature string */
/* @var $email string */
/* @var $amount int */
/* @var $reference string */
/* @var $merchanId string */
/* @var $accountId string */
/* @var $name string */
?>
<form id="payu_form" method="post" action="https://checkout.payulatam.com/ppp-web-gateway-payu/">
  <input name="merchantId"    type="hidden"  value="<?= $merchanId ?>"   >
  <input name="buyerFullName"    type="hidden"  value="<?= $name ?>"   >
  <input name="accountId"     type="hidden"  value="<?= $accountId ?>" >
  <input name="description"   type="hidden"  value="Recarga de creditos"  >
  <input name="referenceCode" type="hidden"  value="<?= $reference ?>" >
  <input name="amount"        type="hidden"  value="<?= $amount ?>"   >
  <input name="tax"           type="hidden"  value="0"  >
  <input name="taxReturnBase" type="hidden"  value="0" >
  <input name="currency"      type="hidden"  value="COP" >
  <input name="signature"     type="hidden"  value="<?= $signature ?>"  >
  <input name="buyerEmail"    type="hidden"  value="<?= $email ?>" >
  <input name="responseUrl"    type="hidden"  value="https://puntosdorados.com/apidriver/external/response" >
  <input name="confirmationUrl"    type="hidden"  value="https://puntosdorados.com/apidriver/external/confirmation" >
  <input name="Submit"        type="submit"  value="Enviar" >
</form>
<script type="text/javascript">document.getElementById("payu_form").submit();</script>
<!--<div class="text-center">
  <form style="display: none">
    <script
        src="https://checkout.epayco.co/checkout.js"
        class="epayco-button"
        data-epayco-key="1b86151ee26c26b3adeaaa825986e2e2"
        data-epayco-invoice="<?= $reference ?>"
        data-epayco-amount="<?= $amount ?>"
        data-epayco-name="comprar de credito"
        data-epayco-description="compra de credito"
        data-epayco-currency="cop"
        data-epayco-country="co"
        data-epayco-test="false"
        data-epayco-external="false"
        data-epayco-response="https://puntosdorados.com/apidriver/external/response?id=<?= $id ?>"
        data-epayco-confirmation="https://puntosdorados.com/apidriver/external/epayco">
    </script>
  </form>
  <div style="height: 400px">
    <h3 style="margin: 40% 10%">Por favor espere sera redirigido para realizar el pago.</h3>
  </div>
</div>
<script type="text/javascript">
  document.getElementsByClassName('epayco-button-render')[0].click()
</script>-->

