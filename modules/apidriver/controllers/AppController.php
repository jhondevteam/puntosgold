<?php

namespace app\modules\apidriver\controllers;


use app\models\Ayuda;
use app\models\Ciudad;
use app\models\Departamento;
use app\models\DriverCategory;
use app\models\DriverPurchase;
use app\models\DriverTrip;
use app\models\Taximetro;
use app\models\Usuario;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Json;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;


/**
 * Default controller for the `apidriver` module
 */
class AppController extends DefaultController
{
  const APP_VERSION_DRIVER = '2.8.8';
  const APP_VERSION_RIDER = '2.7.6';

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
       'class' => CompositeAuth::class,
       'only' => ['save-trip', 'account', 'recharge', 'recharges', 'set-car-info'],
       'authMethods' => [
          HttpBearerAuth::class
       ],
    ];
    $behaviors['authenticator']['except'] = ['OPTIONS'];
    return $behaviors;
  }

  public function actionAccount()
  {
    return Yii::$app->user->identity->driver;
  }

  public function actionRecharge()
  {
    $driver = Yii::$app->user->identity->driver;
    $user = Yii::$app->user->identity;
    $model = new DriverPurchase([
       'driver_id' => $driver->id,
       'date_generate' => date('Y-m-d H:i:s'),
       'reference' => strtoupper(Yii::$app->security->generateRandomString(10)),
       'is_confirm' => 0,
       'amount' => Yii::$app->request->post('amount')
    ]);
    if ($model->amount >= 5000) {
      if ($model->save()) {
        return [
          'id' => $model->id,
          'buy' => $model,
          'driver' => $driver,
          'user' => $user
        ];
      }
    } else {
      throw new BadRequestHttpException('El monto minimo de la recarga es de $5,000 Pesos', 403);
    }
    throw new BadRequestHttpException();
  }

  public function actionRecharges()
  {
    $driver = Yii::$app->user->identity->driver;
    return $driver->getDriverPurchases()
       ->where(['is_confirm' => 1])->orderBy(['id' => SORT_DESC])
       ->all();
  }

  public function actionSetCarInfo()
  {
    $post = Yii::$app->request->post();
    $driver = Yii::$app->user->identity->driver;
    $driver->car_info = Json::encode($post);
    $driver->save(false);
    return $driver;
  }

  public function actionSaveTrip()
  {
    $data = Yii::$app->request->post();
    $usuario = Usuario::findIdentityByAccessToken(Yii::$app->request->post('user_token'));
    $is_points_pay = Yii::$app->request->post('is_pay_points');
    if ($usuario) {
      $trip = new DriverTrip([
         'driver_id' => Yii::$app->user->identity->driver->id,
         'usuario_id' => $usuario->id,
         'price' => $data['price'],
         'date' => date('Y-m-d H:i:s'),
         'is_pay_points' => $is_points_pay == 'true'
      ]);
      if ($trip->save()) {
        $credits = $trip->updateValues();
        return [
           'success' => 'Ok',
           'trip' => $trip,
           'credits' => $credits
        ];
      } else {
        Yii::info($trip->getErrors());
        return $trip;
      }
    }
    throw new BadRequestHttpException();
  }


  public function actionDriverCategories()
  {
    return [
       'categories' => DriverCategory::find()
          ->orderBy('id')
          ->where(['active' => true])
          ->all(),
       'states' => Departamento::find()->orderBy('nombre')
          ->all()
    ];
  }

  public
  function actionAyuda()
  {
    $query = Ayuda::find()
       ->orderBy('titulo');
    if (Yii::$app->request->get('app') == 'driver') {
      $query->where(['destino' => Ayuda::APP_TAXI_DRIVER]);
    } else {
      $query->where(['destino' => Ayuda::APP_TAXI]);
    }
    return $query->all();
  }

  /**
   * @param $v
   * @return bool
   * retornar falso para que el usuario de app sea enviado a actualizar
   */
  public function actionVersionDriver($v)
  {
    if ($v == self::APP_VERSION_DRIVER)
      return true;
    return true;
  }

  /**
   * @param $v
   * @return bool
   * retornar falso para que el usuario de app sea enviado a actualizar
   */
  public function actionVersionRider($v)
  {
    if ($v == self::APP_VERSION_RIDER)
      return true;
    return true;
  }

  public function actionInfoCliente()
  {
    $token = Yii::$app->request->post('client_token');
    $ussario = Usuario::findIdentityByAccessToken($token);
    if ($ussario) {
      return [
         'points' => $ussario->afiliado->puntos
      ];
    } else {
      throw new NotFoundHttpException('No encontrado');
    }
  }

  public function actionTaximetro()
  {
    $city = Yii::$app->request->post('city');
    $city = Ciudad::findOne(['nombre' => $city]);
    if ($city) {
      return Taximetro::find()
         ->where(['like', 'ciudades_ids', "$city->id"])
         ->one();
    }
    return null;
  }
}
