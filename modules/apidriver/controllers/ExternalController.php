<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 30/01/19
 * Time: 10:11 PM
 */

namespace app\modules\apidriver\controllers;


use app\models\DriverPurchase;
use Yii;
use yii\rest\Controller;
use yii\web\Response;

class ExternalController extends Controller
{
  public function actionResponse($id)
  {
    $model = DriverPurchase::findOne($id);
    Yii::$app->response->format = Response::FORMAT_HTML;
    return $this->render('pay_confirm', [
      'model' => $model
    ]);
  }

  public function actionConfirmation()
  {
    if (Yii::$app->request->isPost) {
      $reference = \Yii::$app->request->post('reference_sale');
      $state = \Yii::$app->request->post('state_pol');
      $model = DriverPurchase::findOne(['reference' => $reference]);
      if ($model != null) {
        if ($state == '4') {
          $this->confirmPayment($model);
        }
      }
    } else {
      return ['err' => 'No Get request allowed'];
    }
  }

  public function actionEpayco()
  {
    if (Yii::$app->request->isPost) {
      $data = Yii::$app->request->post();
      $p_cust_id_cliente = '617042';
      $p_key = '6f76e16cdda564186d4e2daeb446755dffe66582';

      $x_ref_payco = $data['x_ref_payco'];
      $x_transaction_id = $data['x_transaction_id'];
      $x_amount = $data['x_amount'];
      $x_currency_code = $data['x_currency_code'];
      $x_signature = $data['x_signature'];

      $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
      Yii::info($x_signature);
      Yii::info($signature);
      if ($x_signature == $signature) {
        $x_id_invoice = $data['x_id_invoice'];
        $x_cod_response = $data['x_cod_transaction_state'];
        $model = DriverPurchase::findOne(['reference' => $x_id_invoice]);
        if ($model != null) {
          if ((int)$x_cod_response == 1) {
            $this->confirmPayment($model);
          }
        }
      }
    } else {
      return ['err' => 'No Get request allowed'];
    }
  }

  public function actionPay($id)
  {
    Yii::$app->response->format = Response::FORMAT_HTML;
    $model = DriverPurchase::findOne($id);
    $apiKey = 'C4MledS9W9nShQqfVW0BibTt57';
    $mercadId = '789668';
    $acoountId = '796527';
    Yii::info("$apiKey~$mercadId~$model->reference~$model->amount~COP");
    return $this->render('pay', [
      'id' => $model->id,
      'signature' => md5("$apiKey~$mercadId~$model->reference~$model->amount~COP"),
      'email' => $model->driver->usuario->email,
      'amount' => $model->amount,
      'reference' => $model->reference,
      'merchanId' => $mercadId,
      'accountId' => $acoountId,
      'name' => $model->driver->usuario->nombres
    ]);
  }

  /**
   * @param DriverPurchase $model
   */
  public function confirmPayment(DriverPurchase $model)
  {
    $model->is_confirm = 1;
    $model->driver->credits += $model->amount;
    $model->date_confirm = date('Y-m-d H:i:s');
    $model->save(false);
    if ($model->driver->driverInvite) {
      $model->driver->driverInvite->credits += 500;
      $model->driver->driverInvite->save(false);
      $model->driver->driver_invite_id = null;
    }
    $model->driver->save(false);
  }
}