<?php

namespace app\modules\apidriver\controllers;

use Yii;
use yii\rest\Controller;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
      Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET');
      Yii::$app->end();
      return true;
    }
    return parent::beforeAction($action);
  }

}
