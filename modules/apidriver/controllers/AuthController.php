<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 14/01/19
 * Time: 08:22 PM
 */

namespace app\modules\apidriver\controllers;

use app\models\EmpresaTransportadora;
use app\models\SignUp;
use app\models\Usuario;
use yii\db\Expression;
use yii\web\UnauthorizedHttpException;

class AuthController extends DefaultController
{
  public function actionSignUp()
  {
    $model = new SignUp([
       'scenario' => 'driverRegister'
    ]);
    if ($model->load(\Yii::$app->request->post(), '')) {
      if ($model->validate()) {
        if (($data = $model->signUpDriver()) !== false) {
          return [
             'success' => 'ok',
             'user' => $data['user'],
             'driver' => $data['driver'],
             'category' => $data['driver']->driverCategory
          ];
        }
      } else
        return $model;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionLogin()
  {
    $model = Usuario::findIdentityByAccessToken(\Yii::$app->request->post('access_token'));
    if ($model !== null && $model->driver) {
      return [
         'success' => 'ok',
         'user' => $model,
         'driver' => $model->driver,
         'category' => $model->driver->driverCategory
      ];
    }
    throw new UnauthorizedHttpException();
  }

  public function actionGetEmpresas($departamento_id)
  {
    $ecp = new Expression("departamento_ids REGEXP \"$departamento_id\"");
    $empresas = EmpresaTransportadora::find()
       ->where($ecp)
       ->all();
    return $empresas;
  }
}