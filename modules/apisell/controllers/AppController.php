<?php

namespace app\modules\apisell\controllers;

use app\helper\AppHelper;
use app\models\Afiliado;
use app\models\Archivo;
use app\models\Ayuda;
use app\models\CompraAfiliado;
use app\models\Pagina;
use app\models\PagoEmpresa;
use app\models\Soporte;
use app\models\TokenCompra;
use DateTime;
use sintret\chat\models\Chat;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rest\Controller;
use yii\rest\Serializer;
use yii\web\UnauthorizedHttpException;

/**
 * Default controller for the `apisell` module
 */
class AppController extends Controller
{

  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
      Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET');
      Yii::$app->end();
      return true;
    }
    return parent::beforeAction($action);
  }

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' => CompositeAuth::className(),
      'authMethods' => [
        QueryParamAuth::className(),
      ],
    ];
    return $behaviors;
  }

  public function actionEstado()
  {
    $empresa = Yii::$app->user->identity->empleado->empresa;
    $totalVendido = $empresa->getPendienteTotalVendido();
    $totalPagar = $empresa->getSaldoPagar();

    $pagoPendiente = $empresa->getPagoPendiente();
    $totalPagarPuntos = $empresa->getPendienteTotalPuntosPagar();
    return [
      'ventas' => $totalVendido,
      'aPagar' => $totalPagar,
      'pagosPuntos' => $totalPagarPuntos,
      'pagoPendiente' => $pagoPendiente,
      'infoBancaria' => Pagina::findOne(['glue' => Pagina::INFO_BANCARIA]),
      'lastCorte' => $pagoPendiente != null ? $pagoPendiente : $empresa->getPagoEmpresas()
        ->orderBy(['id' => SORT_DESC])->one()
    ];
  }

  public function actionRegistrar()
  {
    $empleado = Yii::$app->user->identity->empleado;
    $compra = new CompraAfiliado([
      'empleado_id' => $empleado->id,
      'fecha' => date('Y-m-d H:i:s'),
      'pago_puntos' => 0,
    ]);
    if ($compra->load(Yii::$app->request->post(), '')) {
      if (($compra->empleado->empresa_id == 1 and $compra->afiliado_id == 1) or $compra->afiliado_id != 1) {
        if ($compra->save()) {
          $afiliado = $compra->afiliado;
          $token = Json::decode($afiliado->app_token);
          if (!$afiliado->promo) {
            $total = $afiliado->getTotalPuntosConsumidos();
            if (intval($total) >= 5000) {
              $afiliado->puntos += 10000;
              $afiliado->promo = 1;
              $afiliado->save();
              if (isset($token['token'])) {
                AppHelper::sendNotificationAndroid($token['token'], 'Felicidades',
                  'Haz alcanzado los 5000 puntos consumidos, ahora recibes 10.000 puntos disfrutalos',
                  'OK', 'OK', null, null, $afiliado->sound);
              }
            }
          }
          if (isset($token['token'])) {
            AppHelper::sendNotificationAndroid($token['token'], 'Felicidades', 'Compra registrada correctamente en la empresa ' .
              $compra->empleado->empresa->nombre . ' !', 'sigue comprando y ganando puntos.', 'OK', 'OK', null, null, $afiliado->sound);

          }
          return [
            'success' => 'ok',
            'mensaje' => 'Compra registrada correctamente !! Gracias'
          ];
        } else {
          return $compra;
        }
      }
      $compra->addError('afiliado_id', 'No esta autorizado para realizar esta venta.');
      return $compra;
    }
    throw new UnauthorizedHttpException();
  }


  public function actionRegistrarPuntos()
  {
    $empleado = Yii::$app->user->identity->empleado;
    $compra = new CompraAfiliado([
      'empleado_id' => $empleado->id,
      'fecha' => date('Y-m-d H:i:s'),
      'pago_puntos' => 1
    ]);
    $token = Yii::$app->request->post('token');
    $token = TokenCompra::find()
      ->where(['token' => $token])
      ->andWhere(['is', 'compra_afiliado_puntos_id', null])
      ->one();
    if ($token != null) {
      $fecha = new DateTime($token->fecha);
      $fechaAct = new DateTime('now');
      $diferencia = $fecha->diff($fechaAct);
      Yii::info($diferencia->format('%i %h %d'));
      $m = $diferencia->format('%i');
      $h = $diferencia->format('%h');
      $d = $diferencia->format('%d');
      if ($d == 0 and $h == 0 and $m <= 15) {
        if ($compra->load(Yii::$app->request->post(), '')) {
          if ($compra->afiliado->app_token !== null) {
            if (($compra->empleado->empresa_id == 1 and $compra->afiliado_id == 1) or $compra->afiliado_id != 1) {
              if ($compra->save()) {
                $token->compra_afiliado_puntos_id = $compra->id;
                $token->save();
                $afiliado = $compra->afiliado;
                $afiliado->puntos -= $compra->monto;
                $afiliado->save(false);
                $token = Json::decode($afiliado->app_token);
                if (isset($token['token'])) {
                  AppHelper::sendNotificationAndroid($token['token'], 'Compra con puntos', 'La empresa: ' . $compra->getEmpresa()->nombre . " ha cobrado de tus puntos"
                    . Yii::$app->formatter->asCurrency($compra->monto)
                    , 'sigue comprando y ganando puntos.', null, 'OK', null, null, $afiliado->sound);
                }
                return [
                  'success' => 'ok',
                  'mensaje' => 'Compra registrada correctamente !! Gracias'
                ];
              } else {
                Yii::info($compra->getErrors());
                return [
                  'success' => 'error',
                  'mensaje' => 'Fallo el registro de la compra !!'
                ];
              }
            } else {
              return [
                'success' => 'error',
                'mensaje' => 'Desautorizado para realizar esta venta.'
              ];
            }
          } else {
            return [
              'success' => 'error',
              'mensaje' => 'El usuario no tiene movil registrado no se puede completar la operación'
            ];
          }
        }
      } else {
        return [
          'success' => 'error',
          'mensaje' => 'El codigo del afiliado ha caducado.'
        ];
      }
    } else {
      return [
        'success' => 'error',
        'mensaje' => 'El codigo es invalido, o ya fue usado.'
      ];
    }

    throw new UnauthorizedHttpException();
  }

  public function actionVerPuntos()
  {
    $afiliado = Afiliado::findOne(Yii::$app->request->post('afiliado_id'));
    return ['puntos' => $afiliado->puntos];
  }

  public function actionFindDocument()
  {
    $doc = Yii::$app->request->post('documento');
    $afiliado = Afiliado::find()
      ->innerJoinWith('usuario')
      ->where(['usuario.documento' => $doc])
      ->one();
    if ($afiliado != null and $afiliado->id !== 1) {
      return ['success' => 'ok', 'afiliado' => [
        'afiliado_id' => $afiliado->id,
        'nombres' => $afiliado->usuario->nombres,
        'puntos' => $afiliado->puntos,
        'foto' => Url::to("@web/" . $afiliado->usuario->foto, true)
      ]];
    }
    //condicion especial para el afiliado y empresa de prueba
    $empresa = Yii::$app->user->identity->empleado->empresa;
    if ($afiliado != null and $afiliado->id == 1 and $empresa->id == 1) {
      return ['success' => 'ok', 'afiliado' => [
        'afiliado_id' => $afiliado->id,
        'nombres' => $afiliado->usuario->nombres,
        'puntos' => $afiliado->puntos,
        'foto' => Url::to("@web/" . $afiliado->usuario->foto, true)
      ]];
    }
    return ['success' => 'error', 'mensaje' => "Afiliado no encontrado"];
  }

  public function actionVentas()
  {
    $vendedor = Yii::$app->user->identity->empleado;
    $mes = Yii::$app->request->get('mes', 'actual');
    if ($mes == 'actual') {
      $initMes = date('Y-m-01 00:00:00');
      $finMes = date('Y-m-d 23:59:59');
    } else {
      $initMes = date('Y-m-01 00:00:00', strtotime('-1 month', strtotime(date('Y-m-01 00:00:00'))));
      $lastDay = new DateTime();
      $lastDay->modify('last day of ' . date('F Y', strtotime($initMes)));
      $finMes = $lastDay->format('Y-m-d 23:59:59');
    }
    $serialice = new Serializer();
    $query = $vendedor->getCompraAfiliados()
      ->where(['between', 'fecha', $initMes, $finMes])
      ->orderBy(['fecha' => SORT_DESC]);
    $c = clone $query;
    $puntos = $c->andWhere(['pago_puntos' => true])
      ->sum('monto');
    return [
      'ventas' => $serialice->serializeModels($query->all()),
      'montoTotal' => $query->sum('monto'),
      'comision' => $query->sum('monto_cobra_empresa'),
      'ventaPuntos' => $puntos != null ? $puntos : 0,
    ];
  }


  public function actionAyuda()
  {
    $q = Ayuda::find()
      ->where(['destino' => Ayuda::APP_VENDEDOR]);
    if (Yii::$app->user->can(AppHelper::ADMIN_EMPRESA))
      $q->orWhere(['destino' => Ayuda::WEB_EMPRESA]);
    $q->orderBy('titulo');
    return $q->all();
  }

  public function actionChat()
  {
    $user = Yii::$app->user->identity;
    return Soporte::findOne([
      'usuario_id' => $user->id,
      'active' => true
    ]);
  }

  public function actionSendChat()
  {
    $user = Yii::$app->user->identity;
    $soporte = Soporte::findOne([
      'usuario_id' => $user->id,
      'active' => true
    ]);
    if ($soporte == null) {
      $soporte = new Soporte([
        'usuario_id' => $user->id,
        'active' => true,
        'fecha' => date('Y-m-d'),
      ]);
      $soporte->save();
    }
    $mensaje = Yii::$app->request->post('mesaje');
    $chat = new Chat([
      'message' => $mensaje,
      'userId' => $user->id,
      'soporte_id' => $soporte->id
    ]);
    if (!$chat->save()) {
      Yii::info($chat->getErrors());
    }
    return $soporte;
  }

  public function actionRecibo($id)
  {
    $archivo = new Archivo();
    $archivo->load(Yii::$app->request->post(), '');
    if ($archivo->file) {
      $archivo->file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $archivo->file));
    }
    $archivo->ruta = AppHelper::obtenerDirectorio('recibo') .
      Yii::$app->security->generateRandomString() . ".jpg";
    if ($archivo->file) {
      file_put_contents($archivo->ruta, $archivo->file);
      $model = PagoEmpresa::findOne(['id' => $id]);
      $model->url_file = $archivo->ruta;
      $model->fecha_pago = date('Y-m-d');
      $model->pagado = 0;
      $model->save();
      return ['success' => 'ok', 'pago' => $model];
    } else {
      $archivo->addError('file', 'La foto esta vacia');
    }
    return $archivo;
  }

  public function actionMiempresa()
  {
    return Yii::$app->user->identity->empleado->empresa;
  }
}
