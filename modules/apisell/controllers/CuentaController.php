<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 26/07/17
 * Time: 03:10 PM
 */

namespace app\modules\apisell\controllers;


use app\models\Archivo;
use app\models\Newpass;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use yii\rest\Controller;
use yii\web\UnauthorizedHttpException;

class CuentaController extends Controller
{
  private $folder = 'img/perfil/';

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
      'class' => CompositeAuth::className(),
      'authMethods' => [
        QueryParamAuth::className(),
      ],
    ];
    return $behaviors;
  }

  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, POST');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    return parent::beforeAction($action);
  }

  public function actionPass()
  {
    $model = new Newpass([
      'scenario' => 'uptpass'
    ]);
    if ($model->load(Yii::$app->request->post(), '')) {
      if ($model->validate()) {
        $user = Yii::$app->user->identity;
        $user->setPassword($model->password);
        $user->save(false);
        return ['success' => 'ok'];
      }
      return $model;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionFoto()
  {
    $archivo = new Archivo();
    $archivo->load(Yii::$app->request->post(), '');
    if ($archivo->file) {
      $archivo->file = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $archivo->file));
    }
    if (!file_exists('uploads'))
      mkdir('uploads');
    if (!file_exists("uploads/$this->folder")) {
      $folders = explode('/', $this->folder);
      $path = 'uploads/';
      foreach ($folders as $folder) {
        if (!file_exists($path . $folder)) {
          mkdir($path . $folder);
        }
        $path .= $folder . '/';
      }
    }

    $user = Yii::$app->user->identity;
    $archivo->ruta = 'uploads/' . $this->folder . "/" .
      Yii::$app->security->generateRandomString(20) . ".jpg";
    if ($archivo->file) {
      file_put_contents($archivo->ruta, $archivo->file);
      $user->foto = $archivo->ruta;
      $user->save();
      return ['success' => 'ok', 'user' => $user];
    } else {
      $archivo->addError('file', 'La foto esta vacia');
    }
    return $archivo;
  }

  public function actionDatos()
  {
    $user = Yii::$app->user->identity;
    if ($user->load(Yii::$app->request->post(), '')) {
      if ($user->save())
        return ['success' => 'ok', 'user' => $user];
    }
    return $user;
  }
}