<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 6/06/17
 * Time: 12:23 AM
 */

namespace app\modules\apisell\controllers;


use app\helper\AppHelper;
use app\models\LoginForm;
use app\models\Usuario;
use Yii;
use yii\rest\Controller;
use yii\web\UnauthorizedHttpException;

class AuthController extends Controller
{
  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    return parent::beforeAction($action);
  }

  public function actionLogin()
  {
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post(), '')) {
      if ($model->login()) {
        if (Yii::$app->user->can(AppHelper::EMPLEADO) or
          Yii::$app->user->can(AppHelper::ADMIN_EMPRESA))
          return [
            'success' => 'ok',
            'user' => $model->getUser(),
            'isAdmin'=>Yii::$app->user->can(AppHelper::ADMIN_EMPRESA)
          ];
        else
          $model->addError('password', 'No tienes permiso para usar esta aplicación.');
      }
      return $model;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionPassword()
  {
    $email = Yii::$app->request->post('email');
    $usuario = Usuario::findOne(['email' => $email]);
    if ($usuario != null) {
      $usuario->password_reset_token = Yii::$app->security->generateRandomString();
      $usuario->save(false);
      $sendGrid = Yii::$app->mailer;
      $mensaje = $sendGrid->compose('recordar_contrasena', ['model' => $usuario]);
      $mensaje->setFrom('soporte@puntosdorados.com')
        ->setTo($usuario->email)
        ->setSubject('Recuperación de contraseña Puntos Dorados')
        ->send($sendGrid);
      return [
        'success' => 'ok'
      ];
    } else
      return [
        'success' => 'error',
        'mensaje' => 'Usuario no encontrado verifique su email.'
      ];
  }
}