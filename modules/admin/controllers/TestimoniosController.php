<?php

namespace app\modules\admin\controllers;

use app\models\search\TestimoniosSearch;
use app\models\Testimonios;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TestimoniosController implements the CRUD actions for Testimonios model.
 */
class TestimoniosController extends Controller
{
 /**
  * @inheritdoc
  */
 public function behaviors()
 {
  return [
   'verbs' => [
    'class' => VerbFilter::className(),
    'actions' => [
     'delete' => ['POST'],
    ],
   ],
   'access' => [
    'class' => \yii\filters\AccessControl::className(),
    'rules' => [
     [
      'allow' => true,
      'actions' => ['index', 'view', 'create', 'update', 'delete'],
      'roles' => ['SuperAdministrador']
     ],
     [
      'allow' => false
     ]
    ]
   ]
  ];
 }
 
 /**
  * Lists all Testimonios models.
  * @return mixed
  */
 public function actionIndex()
 {
  $searchModel = new TestimoniosSearch();
  $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
  
  return $this->render('index', [
   'searchModel' => $searchModel,
   'dataProvider' => $dataProvider,
  ]);
 }
 
 /**
  * Displays a single Testimonios model.
  * @param integer $id
  * @return mixed
  */
 public function actionView($id)
 {
  return $this->render('view', [
   'model' => $this->findModel($id),
  ]);
 }
 
 /**
  * Creates a new Testimonios model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
 public function actionCreate()
 {
  $model = new Testimonios();
  
  if ($model->load(Yii::$app->request->post()) && $model->save()) {
   Yii::$app->session->setFlash('success', 'Información guardada correctamente');
   return $this->redirect(['index']);
  } else {
   return $this->render('create', [
    'model' => $model,
   ]);
  }
 }
 
 /**
  * Updates an existing Testimonios model.
  * If update is successful, the browser will be redirected to the 'view' page.
  * @param integer $id
  * @return mixed
  */
 public function actionUpdate($id)
 {
  $model = $this->findModel($id);
  
  if ($model->load(Yii::$app->request->post()) && $model->save()) {
   Yii::$app->session->setFlash('success', 'Información actualizada correctamente');
   return $this->redirect(['index']);
  } else {
   return $this->render('update', [
    'model' => $model,
   ]);
  }
 }
 
 /**
  * Deletes an existing Testimonios model.
  * If deletion is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  */
 public function actionDelete($id)
 {
  $this->findModel($id)->delete();
  Yii::$app->session->setFlash('success', 'Información eliminada correctamente');
  return $this->redirect(['index']);
 }
 
 /**
  * Finds the Testimonios model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Testimonios the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
 protected function findModel($id)
 {
  if (($model = Testimonios::findOne($id)) !== null) {
   return $model;
  } else {
   throw new NotFoundHttpException('The requested page does not exist.');
  }
 }
}
