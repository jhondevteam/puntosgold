<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\base\Usuario;
use app\models\Newpass;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class CuentaController extends \yii\web\Controller
{
 
 public function behaviors()
 {
  return [
   'access' => [
    'class' => AccessControl::className(),
    'rules' => [
     [
      'allow' => true,
      'actions' => ['index', 'cambiar-contrasena'],
      'roles' => [AppHelper::SUPER_ADMIN,AppHelper::ADMIN_ZONA]
     ],
     [
      'allow' => false
     ]
    ]
   ]
  ];
 }
 
 public function actionIndex()
 {
  $usuario = Usuario::find()->where(['id' => Yii::$app->user->identity->id])->one();
  if ($usuario->load(Yii::$app->request->post())) {
   $usuario->picture = UploadedFile::getInstance($usuario, 'picture');
   if ($usuario->picture != null && $usuario->picture != "") {
    $usuario->foto = 'uploads/img/' . $usuario->picture->baseName . '.' . $usuario->picture->extension;
    if ($usuario->save()) {
     $usuario->picture->saveAs($usuario->foto);
     Yii::$app->session->setFlash('success', 'Información actualizada correctamente');
    }
   } else {
    $usuario->foto = $usuario->picture;
    if ($usuario->save()) {
     Yii::$app->session->setFlash('success', 'Información actualizada correctamente');
    }
   }
  }
  return $this->render('index', [
   'usuario' => $usuario,
   'contrasena' => new Newpass()
  ]);
 }
 
 public function actionCambiarContrasena()
 {
  $newpass = new Newpass();
  if ($newpass->load(Yii::$app->request->post()) and $newpass->validate()) {
   $user = Usuario::findOne(Yii::$app->user->identity->id);
   if ($user != null) {
    $user->password = Yii::$app->security->generatePasswordHash($newpass->password);
    $user->password_reset_token = null;
    if ($user->save(false)) {
     Yii::$app->session->setFlash('success', '¡su contraseña ha sido actualizada exitosamente!');
    } else {
     return var_dump($user->getErrors());
    }
   }
  }
  return $this->redirect(['index']);
 }
 
}
