<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\PreguntasRespuestas;
use app\models\search\PreguntasRespuestasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PreguntasRespuestasController implements the CRUD actions for PreguntasRespuestas model.
 */
class PreguntasRespuestasController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
         'access' => [
          'class' => \yii\filters\AccessControl::className(),
          'rules' => [
           [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete'],
            'roles' => ['SuperAdministrador']
           ],
           [
            'allow' => false
           ]
          ]
         ]
        ];
    }
 
    

    /**
     * Lists all PreguntasRespuestas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PreguntasRespuestasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PreguntasRespuestas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PreguntasRespuestas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PreguntasRespuestas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
         Yii::$app->session->setFlash('success','Información guardada correctamente');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PreguntasRespuestas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
         Yii::$app->session->setFlash('success','Información actualizada correctamente');
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PreguntasRespuestas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
     Yii::$app->session->setFlash('success','Información eliminada correctamente');
        return $this->redirect(['index']);
    }

    /**
     * Finds the PreguntasRespuestas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PreguntasRespuestas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PreguntasRespuestas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
