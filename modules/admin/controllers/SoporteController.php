<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\Soporte;
use app\models\SoporteSearch;
use sintret\chat\ChatRoom;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SoporteController implements the CRUD actions for Soporte model.
 */
class SoporteController extends Controller
{
  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'cerrar','send-chat'],
            'roles' => [AppHelper::SUPER_ADMIN]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Soporte models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new SoporteSearch([
      'active' => 1
    ]);
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Soporte model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  public function actionCerrar($id){
    $model = $this->findModel($id);
    $model->active = 0;
    $model->save(false);
    return $this->redirect(['index']);
  }

  /**
   * Deletes an existing Soporte model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Soporte model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Soporte the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Soporte::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionSendChat($id)
  {
    if (!empty($_POST)) {
      return ChatRoom::sendChat($_POST);
    } else
      return var_dump('no post');
  }
}
