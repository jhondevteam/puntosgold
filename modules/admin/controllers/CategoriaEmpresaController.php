<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\CategoriaEmpresa;
use app\models\search\CategoriaEmpresaSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CategoriaEmpresaController implements the CRUD actions for CategoriaEmpresa model.
 */
class CategoriaEmpresaController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-empresa'],
            'roles' => ['@']
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all CategoriaEmpresa models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new CategoriaEmpresaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single CategoriaEmpresa model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerEmpresa = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empresas,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerEmpresa' => $providerEmpresa,
    ]);
  }

  /**
   * Creates a new CategoriaEmpresa model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new CategoriaEmpresa();

    if ($model->load(Yii::$app->request->post())) {
      $model->pic = UploadedFile::getInstance($model, 'pic');
      $model->pic_ico = UploadedFile::getInstance($model, 'pic_ico');
      if ($model->validate(['pic', 'pic_ico', 'nombre'])) {
        $folder = AppHelper::obtenerDirectorio("categoria-empresas");
        $folder_icon = AppHelper::obtenerDirectorio("categoria-empresas/icon");
        $model->url_banner = $folder . Yii::$app->security->generateRandomString(10) . "." . $model->pic->extension;
        $model->url_icon = $folder_icon . Yii::$app->security->generateRandomString(10) . "." . $model->pic_ico->extension;
        if ($model->save()) {
          $model->pic->saveAs($model->url_banner);
          $model->pic_ico->saveAs($model->url_icon);
        }
        return $this->redirect(['view', 'id' => $model->id]);
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);

  }

  /**
   * Updates an existing CategoriaEmpresa model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $model->scenario = 'update';
    if ($model->load(Yii::$app->request->post())) {
      $model->pic = UploadedFile::getInstance($model, 'pic');
      $model->pic_ico = UploadedFile::getInstance($model, 'pic_ico');
      if ($model->validate(['pic','pic_ico', 'nombre'])) {
        $folder = AppHelper::obtenerDirectorio("categoria-empresas");
        $folder_ico = AppHelper::obtenerDirectorio("categoria-empresas/icon");
        if ($model->pic)
          $model->url_banner = $folder . Yii::$app->security->generateRandomString(10) . "." . $model->pic->extension;
        if ($model->pic_ico)
          $model->url_icon = $folder_ico . Yii::$app->security->generateRandomString(10) . "." . $model->pic_ico->extension;
        if ($model->save()) {
          if ($model->pic)
            $model->pic->saveAs($model->url_banner);
          if ($model->pic_ico)
            $model->pic_ico->saveAs($model->url_icon);
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing CategoriaEmpresa model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }


  /**
   * Finds the CategoriaEmpresa model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return CategoriaEmpresa the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = CategoriaEmpresa::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for Empresa
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @return mixed
   */
  public function actionAddEmpresa()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('Empresa');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formEmpresa', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
