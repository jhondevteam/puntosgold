<?php
/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 15/02/19
 * Time: 11:31 AM
 */

namespace app\modules\admin\controllers;


use app\models\AppHelper;
use app\models\Driver;
use app\models\DriverCategory;
use app\models\DriverCategoryState;
use app\models\Usuario;
use Kreait\Firebase\Factory;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

class TaxiAppController extends Controller
{
  public function actionList()
  {
    $modelSearch = new Driver([
       'scenario' => 'search'
    ]);
    $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);
    if (Yii::$app->request->post('hasEditable')) {
      $model = Driver::findOne(Yii::$app->request->post('editableKey'));
      $model->load(Yii::$app->request->post($model->formName())[Yii::$app->request->post('editableIndex')], '');
      $model->save();
      $attr = Yii::$app->request->post('editableAttribute');
      if ($attr == 'active')
        return Json::encode(['output' => Yii::$app->formatter->asBoolean($model->{$attr})]);
      else if ($attr == 'porcent')
        return Json::encode(['output' => Yii::$app->formatter->asPercent($model->{$attr})]);
      else
        return Json::encode(['output' => $model->{$attr}]);
    }
    return $this->render('list', [
       'data' => $dataProvider,
       'modelSearch' => $modelSearch
    ]);
  }

  public function actionUpdateTelefono()
  {
    $data = Yii::$app->request->post('data');
    foreach ($data as $key => $datum) {
      if ($key == 0){
        Yii::info($datum);
      }
      $user = Usuario::findOne(['access_token' => $datum['uid']]);
      if ($user and $user->driver) {
        //if ($user->driver->telefono == null) {
          $user->driver->telefono = $datum['telefono'];
          $user->driver->save(false);
        //}
      }
    }
  }

  public function actionViewDriver($id)
  {
    $driver = Driver::findOne($id);
    if ($driver->load(Yii::$app->request->post())) {
      if ($driver->save()) {
        Yii::$app->session->setFlash('success', 'Guardado correctamente');
      }
    }
    return $this->render('view-driver', [
       'model' => $driver
    ]);
  }

  public function actionCategories()
  {
    if (Yii::$app->request->post('hasEditable')) {
      $attr = Yii::$app->request->post('editableAttribute');
      $model = DriverCategory::findOne(Yii::$app->request->post('editableKey'));
      if ($model != null) {
        $model->load(Yii::$app->request->post($model->formName())[Yii::$app->request->post('editableIndex')], '');
        $model->save();
        Yii::info(strpos('porcent', $attr));
        if ($attr == 'porcent' || $attr == 'porcent_affiliate') {
          return Json::encode(['output' => Yii::$app->formatter->asPercent($model->{$attr})]);
        } else {
          return Json::encode(['output' => $model->name]);
        }
      }
    }
    return $this->render('categories', [
       'data' => new ActiveDataProvider([
          'query' => DriverCategory::find()
       ])
    ]);
  }

  public function actionUpdateCategoryFile()
  {
    $model = DriverCategory::findOne(Yii::$app->request->post('editableKey'));
    $attr = Yii::$app->request->post('editableAttribute');
    $files = UploadedFile::getInstancesByName('DriverCategory[' . Yii::$app->request->post('editableIndex') . '][' . $attr . ']');
    if ($model and !empty($files)) {
      $file = $files[0];
      $folder = AppHelper::obtenerDirectorio('driver-category/icon');
      $model->{$attr} = "$folder/" . Yii::$app->security->generateRandomString(5)
         . '.' . $file->extension;
      $model->save(false);
      $file->saveAs($model->{$attr});
      return Json::encode(['output' => Html::img(Url::to("@web/" . $model->{$attr}))]);
    }
    throw new BadRequestHttpException();
  }

  public function actionStateCategories()
  {
    if (Yii::$app->request->post('hasEditable')) {
      $attr = Yii::$app->request->post('editableAttribute');
      $model = DriverCategoryState::findOne(Yii::$app->request->post('editableKey'));
      if ($model != null) {
        $model->load(Yii::$app->request
           ->post($model->formName())[Yii::$app->request->post("editableIndex")], '');
        $model->save();
        Driver::updateAll(['porcent' => $model->porcent], ['departamento_id' => $model->departamento_id]);
        return Json::encode(['output' => Yii::$app->formatter->asPercent($model->{$attr})]);
      }
    }
    $model = new DriverCategoryState();
    $query = DriverCategoryState::find();
    if ($model->load(Yii::$app->request->post())) {
      $query->andFilterCompare('driver_category_id', $model->driver_category_id)
         ->andFilterCompare('departamento_id', $model->departamento_id);
    }
    return $this->render('category_states', [
       'model' => $model,
       'data' => new ActiveDataProvider([
          'query' => $query
       ])
    ]);
  }

  public function actionAddStateCategory()
  {
    $model = new DriverCategoryState();
    if ($model->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = 'json';
        return ActiveForm::validate($model);
      }
      $model->save();
      return $this->redirect(['taxi-app/state-categories']);
    }
    throw new BadRequestHttpException();
  }

  public function actionDetail()
  {
    if (($key = Yii::$app->request->post('expandRowKey')) !== false) {
      return $this->renderPartial('_detail_list', ['model' => Driver::findOne($key)]);
    }
    throw new BadRequestHttpException();
  }

  public function actionUpdatePassword($id){
    $driver = Driver::findOne($id);
    Yii::info(file_get_contents('clave.json'));
    $firebase = (new Factory())
//       ->withHttpClientConfig([
//          'debug' => true
//       ])
       ->withServiceAccount('clave.json')
       ->create();
    $auth = $firebase->getAuth();
    $user = $auth->getUser('oN40rBOFdzYk89XiX9GH2CuhQbW2');
    Yii::info($user);
    //$auth->changeUserPassword('oN40rBOFdzYk89XiX9GH2CuhQbW2', '123456');
    //return $this->redirect(['taxi-app/view-driver', 'id' => $driver->id]);
  }

  public function actionActive()
  {

  }
}