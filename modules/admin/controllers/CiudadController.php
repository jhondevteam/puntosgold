<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 02:04 AM
 */

namespace app\modules\admin\controllers;


use app\models\Ciudad;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\widgets\ActiveForm;

class CiudadController extends Controller
{
  public function behaviors()
  {
    return [
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'save'],
            'roles' => ['SuperAdministrador']
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  public function actionIndex(){
    $ciudad = Ciudad::find();
    $city = new Ciudad([
      'scenario' => 'buscar'
    ]);
    if ($city->load(\Yii::$app->request->get())){
      $ciudad->andFilterWhere([
        'departamento_id'=>$city->departamento_id
      ])
        ->andOnCondition(['LIKE','nombre',$city->nombre]);
    }
    $model = new Ciudad();
    $id = \Yii::$app->request->get('id',false);
    if ($id)
      $model = Ciudad::findOne($id);
    $data = new ActiveDataProvider([
      'query' => $ciudad
    ]);
    return $this->render('index',[
      'model'=>$model,
      'city'=>$city,
      'data'=>$data
    ]);
  }

  public function actionSave(){
    $model = new Ciudad();
    $id = \Yii::$app->request->get('id',false);
    if ($id)
      $model = Ciudad::findOne($id);
    if ($model->load(\Yii::$app->request->post())){
      if (\Yii::$app->request->isAjax){
        \Yii::$app->response->format = 'json';
        return ActiveForm::validate($model);
      }
      if ($model->save()){
        \Yii::$app->session->setFlash('success','Guardado correctamente');
        return $this->redirect(['index']);
      }
    }
    return $this->redirect(['index']);
  }

}