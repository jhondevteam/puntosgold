<?php


namespace app\modules\admin\controllers;


use app\models\EmpresaTransportadora;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class EmpresaTransportadoraController extends Controller
{
  public function actionIndex(){
    $data = new ActiveDataProvider([
       'query' => EmpresaTransportadora::find()
    ]);
    return $this->render('index',[
       'data'=>$data
    ]);
  }

  public function actionCreate(){
    $model = new EmpresaTransportadora();
    if ($model->load(\Yii::$app->request->post())){
      if ($model->save()){
        return $this->redirect(['empresa-transportadora/view','id'=>$model->id]);
      }
    }
    return $this->render('create',[
       'model'=>$model
    ]);
  }

  public function actionView(){

  }

  public function actionUpdate($id){

  }

  protected function findModel($id){

  }
}