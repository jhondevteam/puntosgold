<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\Accionista;
use app\models\AccionistaSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AccionistasController implements the CRUD actions for Accionista model.
 */
class AccionistasController extends Controller
{
  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'confirm'],
            'roles' => [AppHelper::SUPER_ADMIN]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }
  
  /**
   * Lists all Accionista models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new AccionistaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }
  
  /**
   * Displays a single Accionista model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }
  
  public function actionConfirm($id){
    $model = $this->findModel($id);
    $model->pagado = 1;
    $model->save(false);
    return $this->redirect(['index']);
  }
  
  /**
   * Creates a new Accionista model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Accionista();
    
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    }
    
    return $this->render('create', [
      'model' => $model,
    ]);
  }
  
  /**
   * Updates an existing Accionista model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    }
    
    return $this->render('update', [
      'model' => $model,
    ]);
  }
  
  /**
   * Deletes an existing Accionista model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->delete();
    
    return $this->redirect(['index']);
  }
  
  /**
   * Finds the Accionista model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Accionista the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Accionista::findOne($id)) !== null) {
      return $model;
    }
    
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
