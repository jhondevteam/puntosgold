<?php

namespace app\modules\admin\controllers;

use app\models\Ciudad;
use app\models\Departamento;
use app\models\Zona;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ZonaController implements the CRUD actions for Zona model.
 */
class ZonaController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'get-locale'],
            'roles' => ['SuperAdministrador']
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Zona models.
   * @return mixed
   */
  public function actionIndex()
  {
    $dataProvider = new ActiveDataProvider([
      'query' => Zona::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Zona model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerAdminZona = new \yii\data\ArrayDataProvider([
      'allModels' => $model->adminZonas,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerAdminZona' => $providerAdminZona,
    ]);
  }

  /**
   * Creates a new Zona model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Zona();
    if ($model->load(Yii::$app->request->post())) {
      if ($model->save())
        return $this->redirect(['index']);
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing Zona model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Zona model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }


  /**
   * Finds the Zona model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Zona the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Zona::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionGetLocale($type)
  {
    Yii::$app->response->format = Response::FORMAT_JSON;
    $params = Yii::$app->request->post('depdrop_all_params');
    if ($type == 'D') {
      return [
        'output' => Departamento::find()
          ->select(['id', 'name' => 'nombre'])
          ->where(['pais_id' => $params['pais_id']])
          ->orderBy('nombre')
          ->asArray()
          ->all()
      ];
    } elseif ($type == 'C') {
      return [
        'output' => Ciudad::find()
          ->select(['id', 'name' => 'nombre'])
          ->where(['departamento_id' => $params['departamento_id']])
          ->orderBy('nombre')
          ->asArray()
          ->all()
      ];
    }
  }
}
