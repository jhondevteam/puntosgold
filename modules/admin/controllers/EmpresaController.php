<?php

namespace app\modules\admin\controllers;

use app\actions\CorteDeEmpresas;
use app\helper\AppHelper;
use app\models\base\Video;
use app\models\Empleado;
use app\models\Empresa;
use app\models\Foto;
use app\models\PagoEmpresa;
use app\models\search\EmpresaSearch;
use app\models\Usuario;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;

/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf', 'add-empleado', 'add-empresa-sucursal', 'video-action',
              'upload-foto', 'delete-foto', 'estado', 'corte-empresa', 'send-password', 'ress-password', 'emp-edit', 'new-video'],
            'roles' => [AppHelper::SUPER_ADMIN]
          ],
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'upload-foto', 'update'],
            'roles' => [AppHelper::ADMIN_ZONA]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Empresa models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new EmpresaSearch();
    if (Yii::$app->user->can(AppHelper::ADMIN_ZONA))
      $searchModel->admin_zona_id = Yii::$app->user->identity->adminZona->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Empresa model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    return $this->render('view', [
      'model' => $model,
    ]);
  }

  public function actionEstado($pago_id, $estado)
  {
    $model = PagoEmpresa::findOne($pago_id);
    $model->pagado = $estado;
    if ($model->pagado == '2') {
      $model->url_file = null;
    }
    $model->save(false);
    Yii::info($model->getErrors());
    Yii::$app->session->setFlash('success', 'Proceso realizado correctamente');
    return $this->redirect(['view', 'id' => $model->empresa_id]);
  }

  /**
   * Creates a new Empresa model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Empresa();
    $user = new Usuario();
    if ($user->load(Yii::$app->request->post()) and $model->loadAll(Yii::$app->request->post())) {
      $tr = Yii::$app->db->beginTransaction();
      if (Yii::$app->user->can(AppHelper::ADMIN_ZONA))
        $model->admin_zona_id = Yii::$app->user->identity->adminZona->id;
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validateMultiple([$user, $model]);
      }
      $user->setPassword($user->password);
      $user->generateKeys();
      $model->pic = UploadedFile::getInstance($model, 'pic');
      $folder = AppHelper::obtenerDirectorio('empresas-logo');
      if ($model->validate() and $user->validate()) {
        $user->save();
        $model->url_logo = $folder . Yii::$app->security->generateRandomString(10) . "." . $model->pic->extension;
        if ($model->saveAll(['empleados', 'fotos'])) {
          $model->pic->saveAs($model->url_logo);
          $emp = new Empleado([
            'usuario_id' => $user->id,
            'empresa_id' => $model->id
          ]);
          $emp->save();
          Yii::$app->authManager
            ->assign(Yii::$app->authManager->getRole(AppHelper::ADMIN_EMPRESA), $user->id);
          $tr->commit();
          return $this->redirect(['view', 'id' => $model->id]);
        } else {
          $tr->rollBack();
        }
      }
    }
    $user->password = '';
    return $this->render('create', [
      'model' => $model,
      'user' => $user
    ]);
  }

  /**
   * Updates an existing Empresa model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $model->scenario = 'update';
    $model->toEdit();
    if ($model->loadAll(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
      }
      $model->pic = UploadedFile::getInstance($model, 'pic');
      $folder = AppHelper::obtenerDirectorio('empresas-logo');
      if ($model->validate()) {
        if ($model->pic)
          $model->url_logo = $folder . Yii::$app->security->generateRandomString(10) . "." . $model->pic->extension;
        if ($model->saveAll(['empleados', 'fotos', 'videos'])) {
          if ($model->pic)
            $model->pic->saveAs($model->url_logo);
          return $this->redirect(['view', 'id' => $model->id]);
        }
      }
    }
    return $this->render('update', [
      'model' => $model,
    ]);
  }

  public function actionNewVideo($id)
  {
    $model = new Video([
      'empresa_id' => $id,
    ]);
    if ($model->load(Yii::$app->request->post()) and $model->save()) {
      Yii::$app->session->setFlash('success', 'Registrado correctamente');
    }
    return $this->redirect(['view', 'id' => $id]);
  }

  public function actionVideoAction($id,$action){
    $model = Video::findOne($id);
    if ($action == 'borrar') {
      $model->delete();
    }
    if ($action == 'visibilidad') {
      $model->activo = $model->activo? 0 : 1;
      $model->save();
    }
    Yii::$app->session->setFlash('success','Acción realizada con exito.');
    return $this->redirect(['view','id'=>$model->empresa_id]);
  }

  public function actionSendPassword($id)
  {
    $user = Usuario::findOne($id);
    $user->password_reset_token = Yii::$app->security->generateRandomString();
    $user->save(false);
    $sendGrid = Yii::$app->mailer;
    $mensaje = $sendGrid->compose('recordar_contrasena', ['model' => $user]);
    $mensaje->setFrom('soporte@puntosdorados.com')
      ->setTo($user->email)
      ->setSubject('Recuperación de contraseña Puntos Dorados')
      ->send($sendGrid);
    Yii::$app->session->setFlash('success', 'Se ha enviado un correo al usuario');
    return $this->redirect(['/empresa']);
  }

  public function actionRessPassword($id)
  {
    $user = Usuario::findOne($id);
    $user->password = Yii::$app->security->generatePasswordHash(123456);
    $user->save(false);
    return $this->redirect(Yii::$app->request->referrer);
  }

  public function actionEmpEdit($id)
  {
    $user = Usuario::findOne($id);
    $user->scenario = 'empRegister';
    if ($user->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = 'json';
        return ActiveForm::validate($user);
      }
      if ($user->save()) {
        Yii::$app->session->setFlash('success', 'Datos actualizados correctamente');
        $this->redirect(['view', 'id' => $user->empleado->empresa_id]);
      }
    }
    return $this->render('edit-empleado', [
      'user' => $user,
      'model' => $user->empleado->empresa
    ]);
  }

  public function actionCorteEmpresa($id)
  {
    $empresa = $this->findModel($id);
    $corteEmpresa = new CorteDeEmpresas('empresa-corte', $this);
    if (($pago = $corteEmpresa->creaCorteEmpresa($empresa)) !== false) {
      Yii::$app->session->setFlash('success', 'Corte de empresa Creado correctamente, 
      el monto a cobrar es de: ' . Yii::$app->formatter->asCurrency($pago->monto));
    } else {
      Yii::$app->session->setFlash('danger', 'No se puede crear el corte porque el saldo a cobrar seria negativo.');
    }
    return $this->redirect(['view', 'id' => $empresa->id]);
  }

  public function actionUploadFoto($id)
  {
    $foto = new Foto([
      'empresa_id' => $id
    ]);
    $foto->file = UploadedFile::getInstance($foto, 'file');
    if ($foto->validate(['file'])) {
      $folder = AppHelper::obtenerDirectorio('empresas/banner');
      $foto->url_foto = $folder . Yii::$app->security->generateRandomString(10) . "." . $foto->file->extension;
      $foto->save();
      $foto->file->saveAs($foto->url_foto);
      return $this->redirect(['view', 'id' => $id]);
    } else {
      Yii::$app->session->setFlash('danger', $foto->getFirstError('file'));
    }
    return $this->redirect(['index']);
  }

  public function actionDeleteFoto($id)
  {
    $imagen = Foto::findOne($id);
    $imagen->delete();
    return $this->redirect(['view', 'id' => $imagen->empresa_id]);
  }

  /**
   * Deletes an existing Empresa model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();
    return $this->redirect(['index']);
  }

  /**
   *
   * Export Empresa information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerEmpleado = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empleados,
    ]);
    $providerEmpresaSucursal = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empresaSucursals,
    ]);

    $content = $this->renderAjax('_pdf', [
      'model' => $model,
      'providerEmpleado' => $providerEmpleado,
      'providerEmpresaSucursal' => $providerEmpresaSucursal,
    ]);

    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => Pdf::FORMAT_A4,
      'orientation' => Pdf::ORIENT_PORTRAIT,
      'destination' => Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }


  /**
   * Finds the Empresa model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Empresa the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Empresa::findOne($id)) !== null) {
      if (Yii::$app->user->can(AppHelper::ADMIN_ZONA) and
        Yii::$app->user->identity->adminZona->id == $model->admin_zona_id)
        return $model;
      else if (Yii::$app->user->can(AppHelper::SUPER_ADMIN))
        return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }

  /**
   * Action to load a tabular form grid
   * for EmpresaSucursal
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   * @return mixed
   * @throws NotFoundHttpException
   */
  public function actionAddEmpresaSucursal()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('EmpresaSucursal');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formEmpresaSucursal', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
