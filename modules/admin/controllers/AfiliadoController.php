<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\Afiliado;
use app\models\Grupo;
use app\models\search\AfiliadoSearch;
use app\models\search\CompraAfiliadoSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AfiliadoController implements the CRUD actions for Afiliado model.
 */
class AfiliadoController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'add-compra-afiliado',
              'add-grupo', 'add-puntos-afiliado-mes', 'add-puntos-referidos-mes','expand','expand-grupo'],
            'roles' => [AppHelper::SUPER_ADMIN]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Afiliado models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new AfiliadoSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Afiliado model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $search = new CompraAfiliadoSearch([
      'afiliado_id' => $model->id,
      'fecha' => date('Y-m-01'),
      'fecha_hasta' => date('Y-m-d')
    ]);
    $search->prepareCompraAfiliado(Yii::$app->request->queryParams);
    $compras = $search->search();
    return $this->render('view', [
      'model' => $model,
      'compras' => $compras,
      'search'=>$search
    ]);
  }

  public function actionExpand(){
    return $this->renderAjax('_expand',[
      'model'=>$this->findModel(Yii::$app->request->post('expandRowKey'))
    ]);
  }

  public function actionExpandGrupo(){
    $grupo = Grupo::findOne(Yii::$app->request->post('expandRowKey'));
    return $this->renderAjax('_grupo',[
      'model'=>$grupo
    ]);
  }

  /**
   * Creates a new Afiliado model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Afiliado();

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Afiliado model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Afiliado model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }


  /**
   * Finds the Afiliado model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Afiliado the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Afiliado::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for CompraAfiliado
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @return mixed
   */
  public function actionAddCompraAfiliado()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('CompraAfiliado');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formCompraAfiliado', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for Grupo
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @return mixed
   */
  public function actionAddGrupo()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('Grupo');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formGrupo', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for PuntosAfiliadoMes
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @return mixed
   */
  public function actionAddPuntosAfiliadoMes()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('PuntosAfiliadoMes');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formPuntosAfiliadoMes', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  /**
   * Action to load a tabular form grid
   * for PuntosReferidosMes
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   *
   * @return mixed
   */
  public function actionAddPuntosReferidosMes()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('PuntosReferidosMes');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formPuntosReferidosMes', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
