<?php

namespace app\modules\admin\controllers;

use app\helper\AppHelper;
use app\models\AdminZona;
use app\models\search\AdminZonaSearch;
use app\models\Usuario;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * AdminZonaController implements the CRUD actions for AdminZona model.
 */
class AdminZonaController extends Controller
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => \yii\filters\AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'pdf', 'acceso'],
            'roles' => [AppHelper::SUPER_ADMIN]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all AdminZona models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new AdminZonaSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single AdminZona model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $providerEmpresa = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empresas,
    ]);
    return $this->render('view', [
      'model' => $this->findModel($id),
      'providerEmpresa' => $providerEmpresa,
    ]);
  }

  /**
   * Creates a new AdminZona model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new AdminZona();
    $modelUser = new Usuario();
    if ($model->load(Yii::$app->request->post()) and $modelUser->load(Yii::$app->request->post())) {
      $trans = Yii::$app->db->beginTransaction();
      $modelUser->setPassword($modelUser->documento);
      $modelUser->generateKeys();
      if ($modelUser->save())
        $model->usuario_id = $modelUser->id;
      if ($model->save()) {
        $trans->commit();
        Yii::$app->authManager->assign(Yii::$app->authManager
          ->getRole(AppHelper::ADMIN_ZONA), $modelUser->id);
        return $this->redirect(['view', 'id' => $model->id]);
      } else
        $trans->rollBack();
    }
    return $this->render('create', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  /**
   * Updates an existing AdminZona model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $modelUser = $model->usuario;
    if ($model->load(Yii::$app->request->post()) and $modelUser->load(Yii::$app->request->post())) {
      $trans = Yii::$app->db->beginTransaction();
      if ($modelUser->save())
        if ($model->save()) {
          $trans->commit();
          return $this->redirect(['view', 'id' => $model->id]);
        } else
          $trans->rollBack();
    }
    return $this->render('update', [
      'model' => $model,
      'modelUser' => $modelUser
    ]);
  }

  public function actionAcceso($id, $control)
  {
    $model = $this->findModel($id);
    if ($control == 'assignar'){
      Yii::$app->authManager->assign(Yii::$app->authManager
        ->getRole(AppHelper::ADMIN_ZONA), $model->usuario_id);
    }else{
      Yii::$app->authManager->revoke(Yii::$app->authManager->getRole(AppHelper::ADMIN_ZONA),$model->usuario_id);
    }
    Yii::$app->session->setFlash('success','Proceso realizado correctamente');
    return $this->redirect(['view','id'=>$model->id]);
  }

  /**
   * Deletes an existing AdminZona model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }

  /**
   *
   * Export AdminZona information into PDF format.
   * @param integer $id
   * @return mixed
   */
  public function actionPdf($id)
  {
    $model = $this->findModel($id);
    $providerEmpresa = new \yii\data\ArrayDataProvider([
      'allModels' => $model->empresas,
    ]);

    $content = $this->renderAjax('_pdf', [
      'model' => $model,
      'providerEmpresa' => $providerEmpresa,
    ]);

    $pdf = new \kartik\mpdf\Pdf([
      'mode' => \kartik\mpdf\Pdf::MODE_CORE,
      'format' => \kartik\mpdf\Pdf::FORMAT_A4,
      'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
      'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
      'content' => $content,
      'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
      'cssInline' => '.kv-heading-1{font-size:18px}',
      'options' => ['title' => \Yii::$app->name],
      'methods' => [
        'SetHeader' => [\Yii::$app->name],
        'SetFooter' => ['{PAGENO}'],
      ]
    ]);

    return $pdf->render();
  }


  /**
   * Finds the AdminZona model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return AdminZona the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = AdminZona::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
