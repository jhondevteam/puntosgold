<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Testimonios */

$this->title = Yii::t('app', 'Create Testimonios');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Testimonios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Formulario para ingresar testimonios</h3>
      </div>
      <div class="panel-body">
       <?= $this->render('_form', [
        'model' => $model,
       ]) ?>
      </div>
    </div>