<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Testimonios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
 
 <?php $form = ActiveForm::begin(); ?>
 
 <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>
 <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
 <?= $form->field($model, 'descripcion')->textarea(['rows'=>6]) ?>
 <?= $form->field($model, 'url_video') ?>

  <div class="form-group">
   <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Guardar') : Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
 
 <?php ActiveForm::end(); ?>

</div>
