<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TestimoniosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Testimonios');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Administración de testimonios</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a(Yii::t('app', 'Nuevo testimonio'), ['create'], ['class' => 'btn btn-success']) ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php Pjax::begin(); ?>    <?= GridView::widget([
         'dataProvider' => $dataProvider,
         'filterModel' => $searchModel,
         'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
   
          'id',
          'titulo',
          'descripcion',
          'nombre',
          ['class' => 'yii\grid\ActionColumn'],
         ],
        ]); ?>
        <?php Pjax::end(); ?>
       </div>
      </div>
    </div>