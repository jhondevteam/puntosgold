<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Testimonios */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Testimonios',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Testimonios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Formulaio para actualizar testimonios</h3>
      </div>
      <div class="panel-body">
       <?= $this->render('_form', [
        'model' => $model,
       ]) ?>
      </div>
    </div>