<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Testimonios */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Testimonios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Detalles del testimonio</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->id], [
         'class' => 'btn btn-danger',
         'data' => [
          'confirm' => Yii::t('app', '¿Seguro desea eliminar el item?'),
          'method' => 'post',
         ],
        ]) ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= DetailView::widget([
         'model' => $model,
         'attributes' => [
          'id',
          'titulo',
          'descripcion',
          'nombre',
         ],
        ]) ?>
       </div>
      </div>
    </div>