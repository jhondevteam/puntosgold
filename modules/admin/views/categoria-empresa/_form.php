<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriaEmpresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categoria-empresa-form">

  <?php $form = ActiveForm::begin(); ?>
  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>
  <?= $form->field($model, 'pic')
    ->fileInput(['placeholder' => 'Banner'])
    ->label('Imagen banner')
    ->hint('La imagen debe medir (320px * 180px)') ?>

  <?= $form->field($model, 'pic_ico')
    ->fileInput(['placeholder' => 'Icono'])
    ->label('Imagen banner')
    ->hint('La imagen debe medir (24px * 41px)') ?>
  <?= $form->field($model, 'activa')->checkbox() ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
