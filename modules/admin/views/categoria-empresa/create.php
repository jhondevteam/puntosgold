<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoriaEmpresa */

$this->title = 'Agregar Categoria de Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Categorias de Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoria-empresa-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
