<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CategoriaEmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Categorias de Empresa';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="categoria-empresa-index">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Agregar Categoria', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Buscar', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:none">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    'nombre',
    [
      'label' => 'Icono',
      'value' => function (\app\models\CategoriaEmpresa $model) {
        return Html::img("@web/$model->url_icon");
      },
      'format'=>'raw'
    ],
    [
      'label' => 'Imagen',
      'value' => function (\app\models\CategoriaEmpresa $model) {
        return Html::img("@web/$model->url_banner");
      },
      'format'=>'raw'
    ],
    'activa:boolean',
    [
      'class' => 'yii\grid\ActionColumn',
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-categoria-empresa']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Full',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Export All Data</li>',
          ],
        ],
        'exportConfig' => [
          ExportMenu::FORMAT_PDF => false
        ]
      ]),
    ],
  ]); ?>

</div>
