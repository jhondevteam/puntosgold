<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriaEmpresa */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Categoria Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">DETALLES DE LA CATEGORIA</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
         'class' => 'btn btn-danger pull-right',
         'data' => [
          'confirm' => '¿Seguro desea eliminar este item?',
          'method' => 'post',
         ],
        ])
        ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        $gridColumn = [
         'nombre',
         [
          'label' => 'Banner',
          'value' => Html::img("@web/$model->url_banner"),
          'format' => 'raw'
         ],
         [
          'label' => 'icon',
          'value' => Html::img("@web/$model->url_icon"),
          'format' => 'raw'
         ],
         'activa',
        ];
        echo DetailView::widget([
         'model' => $model,
         'attributes' => $gridColumn
        ]);
        ?>
        <?php
        if ($providerEmpresa->totalCount) {
         $gridColumnEmpresa = [
          ['class' => 'yii\grid\SerialColumn'],
          [
           'attribute' => 'adminZona.usuario.nombres',
           'label' => 'Administrador asignado'
          ],
          'nombre',
          'nit',
          'estado:boolean',
         ];
         echo Gridview::widget([
          'dataProvider' => $providerEmpresa,
          'pjax' => true,
          'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empresa']],
          'panel' => [
           'type' => GridView::TYPE_PRIMARY,
           'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Empresa'),
          ],
          'columns' => $gridColumnEmpresa
         ]);
        }
        ?>
       </div>
      </div>
    </div>