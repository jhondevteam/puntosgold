<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoriaEmpresa */

$this->title = 'Actualizar Categoria: ' . ' ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Categorias de Empresa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="categoria-empresa-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
