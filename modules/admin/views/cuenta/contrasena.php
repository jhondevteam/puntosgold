<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/** @var \app\models\Newpass $model */
?>


<section class="why-choose-area section-big">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <h4>Recuperación de contraseña</h4>
       <?php $form = ActiveForm::begin([
           'action'=>['cambiar-contrasena']
       ]); ?>
        <p>Nueva contraseña</p>
       <?= $form->field($model, 'password')->passwordInput()->label(false) ?>
        <p>Confirmar contraseña</p>
       <?= $form->field($model, 'password2')->passwordInput()->label(false) ?>
        <div class="box-footer">
         <?= Html::submitButton(Yii::t('app', 'Aceptar'), ['class' => 'btn btn-primary btn-block']) ?>
        </div>
      </div>
     <?php ActiveForm::end(); ?>
      <div class="clear"></div>
      </div>
    </div>
</section>