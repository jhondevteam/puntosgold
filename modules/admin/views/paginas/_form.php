<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pagina-form">

  <?php if($model->glue == \app\models\Pagina::TERMINOS_AFILIADOS): ?>
    <div class="alert-alert-info">
      <h3>Variables</h3>
      <p>(nombre afiliado)</p>
      <p>(cedula afiliado)</p>
      <p>()</p>
    </div>
  <?php endif; ?>
  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'contenido')
    ->widget(\dosamigos\ckeditor\CKEditor::className()) ?>
  <?php if ($model->isNewRecord): ?>
    <?= $form->field($model, 'glue') ?>
  <?php endif; ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar',
      ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
