<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Paginas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagina-view">
  <p>
    <?= Html::a('actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
  </p>

  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
      'titulo',
      'contenido:raw',
    ],
  ]) ?>

</div>
