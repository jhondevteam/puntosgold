<?php

/* @var $this yii\web\View */
/* @var $model app\models\Pagina */

$this->title = 'Actualizar Pagina: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Paginas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="pagina-update">
  <?= $this->render('_form', [
    'model' => $model,
  ]) ?>
</div>
