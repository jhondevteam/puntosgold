<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaginaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Paginas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagina-index">
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'id',
      'titulo',
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => "{view} {update}"
      ],
    ],
  ]); ?>
</div>
