<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SoporteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Soportes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soporte-index">
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <?php Pjax::begin(); ?>    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      'id',
      'usuario.nombres',
      'active:boolean',
      'fecha:datetime',

      [
        'class' => 'yii\grid\ActionColumn',
        'template' => "{view}"
      ],
    ],
  ]); ?>
  <?php Pjax::end(); ?>
</div>
