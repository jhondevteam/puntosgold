<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Soporte */

$this->title = 'Create Soporte';
$this->params['breadcrumbs'][] = ['label' => 'Soportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soporte-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
