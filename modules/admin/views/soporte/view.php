<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Soporte */

$this->title = "Soporte N°".$model->id." Usuario : ".$model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Soportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="soporte-view">
  <div class="col-lg-3">
    <h2>Detalles del soporte</h2>
    <?= DetailView::widget([
      'model' => $model,
      'attributes' => [
        'id',
        'usuario.nombres',
        'active:boolean',
        'fecha:date',
      ],
    ]) ?>
    <?= Html::a('Cerrar chat',['cerrar','id'=>$model->id],['class'=>'btn btn-danger btn-block']) ?>
  </div>
  <div class="col-lg-9">
    <div id="chat">
      <?= \sintret\chat\ChatRoom::widget([
        'userModel' => \app\models\Usuario::className(),
        'userField' => 'foto',
        'url' => Url::to(['/admin/soporte/send-chat', 'id' => $model->id]),
        'room_id' => $model->id,
      ]) ?>
    </div>
  </div>



</div>
