<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\EmpresaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-empresa-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => ['options' => ['class' => 'col-md-4 form-group']]
  ]); ?>
  <?php if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)): ?>
    <?= $form->field($model, 'admin_zona_id')->widget(\kartik\widgets\Select2::classname(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\AdminZona::find()->orderBy('id')
        ->joinWith('usuario')
        ->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'Administrador zona'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
  <?php endif; ?>
  <?= $form->field($model, 'categoria_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\CategoriaEmpresa::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Categoria de empresa'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

  <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true, 'placeholder' => 'Descripcion']) ?>

  <?= $form->field($model, 'nit')->textInput(['maxlength' => true, 'placeholder' => 'Nit']) ?>

  <?= $form->field($model, 'estado')->dropDownList(['' => 'Todo', '1' => 'Activas', '0' => 'In-Activas']) ?>

  <div class="form-group">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
