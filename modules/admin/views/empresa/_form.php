<?php

use app\models\Empresa;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $user app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos' => \yii\web\View::POS_END,
  'viewParams' => [
    'class' => 'EmpresaSucursal',
    'relID' => 'empresa-sucursal',
    'value' => \yii\helpers\Json::encode($model->empresaSucursals),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
  ]
]);
?>

<div class="empresa-form">

  <?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true
  ]); ?>
  <?php if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)): ?>
    <?= $form->field($model, 'admin_zona_id')->widget(\kartik\widgets\Select2::classname(), [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\AdminZona::find()
        ->joinWith('usuario')
        ->orderBy('usuario.nombres')->all(), 'id', 'usuario.nombres'),
      'options' => ['placeholder' => 'Seleccione un administrador encargado'],
      'pluginOptions' => [
        'allowClear' => true
      ],
    ]); ?>
  <?php endif; ?>
  <?= $form->field($model, 'categoria_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\CategoriaEmpresa::find()->orderBy('nombre')
      ->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'seleccione Categoria de empresa'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>

  <?= $form->field($model, 'pic')
    ->fileInput(['placeholder' => 'Logo'])
    ->label('Imagen Logo')
    ->hint('La imagen debe medir (800px * 500px)') ?>

  <?= $form->field($model, 'descripcion')
    ->widget(\dosamigos\ckeditor\CKEditor::className(), [
      'options' => ['rows' => 6],
      'preset' => ''
    ]) ?>

  <?= $form->field($model, 'nit')->textInput(['maxlength' => true, 'placeholder' => 'Nit']) ?>

  <?= $form->field($model, 'estado')->checkbox() ?>

  <?php if ($model->isNewRecord or Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)): ?>
    <?= $form->field($model, 'mode')
      ->dropDownList([
        Empresa::MODE_PORCENTAJE => Empresa::MODE_PORCENTAJE,
        Empresa::MODE_PPTRANSACCION => Empresa::MODE_PPTRANSACCION
      ], ['prompt' => 'Seleccione']) ?>

    <?= $form->field($model, 'porcentaje')
      ->dropDownList(\app\helper\AppHelper::getPorcentajes())
      ->hint('Requerido si el modo de empresa es : ' . Empresa::MODE_PORCENTAJE)
    ?>
    <?= $form->field($model, 'porcentaje_afiliado')
      ->dropDownList(\app\helper\AppHelper::getPorcentajes())
      ->hint('Requerido si el modo de empresa es : ' . Empresa::MODE_PORCENTAJE)
    ?>

    <?= $form->field($model, 'puntos')
      ->hint('Requerido si el modo de empresa es : ' . Empresa::MODE_PPTRANSACCION)
    ?>
    <?= $form->field($model, 'puntos_afiliado')
      ->hint('Requerido si el modo de empresa es : ' . Empresa::MODE_PPTRANSACCION)
    ?>
  <?php endif; ?>
  <?php if ($model->isNewRecord): ?>
    <h4 class="text-primary">Usuario Administrador.</h4>
    <?= $form->field($user, 'nombres') ?>
    <?= $form->field($user, 'documento') ?>
    <?= $form->field($user, 'email') ?>
    <?= $form->field($user, 'password') ?>
  <?php endif; ?>
  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('EmpresaSucursal'),
      'content' => $this->render('_formEmpresaSucursal', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->empresaSucursals),
      ]),
    ],
  ];
  echo kartik\tabs\TabsX::widget([
    'items' => $forms,
    'position' => kartik\tabs\TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false,
    ],
  ]);
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
