<?php

use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-view">
  <div class="form-group">
    <?php if (!Yii::$app->user->can(\app\helper\AppHelper::ADMIN_ZONA)): ?>
      <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php
      $pago = $model->getPagoPendiente();
      ?>
      <?php if ($pago == null): ?>
        <?= Html::a('Crear cobro', ['corte-empresa', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?php endif; ?>
    <?php endif; ?>
  </div>
  <div class="form-group">
    <div class="col-md-8 clearfix">
      <?php
      $gridColumn = [
        [
          'attribute' => 'adminZona.usuario.nombres',
          'label' => 'Administrador asignado',
        ],
        [
          'attribute' => 'categoria.nombre',
          'label' => 'Categoria',
        ],
        'nombre',
        'descripcion:html',
        'nit',
        'estado:boolean',
        'mode',
        'porcentaje:percent',
        'porcentaje_afiliado:percent',
        'puntos:currency',
        'puntos_afiliado:currency'
      ];
      echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
      ]);
      ?>
    </div>
    <div class="col-md-4">
      <?= Html::img("@web/$model->url_logo", ['alt' => $model->nombre, 'class' => 'img-thumbnail']) ?>
    </div>
  </div>
  <div class="clearfix"></div>
  <?php $items = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Estado de la empresa'),
      'content' => $this->render('_dataEstado', [
        'model' => $model,
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Banner de la empresa'),
      'content' => $this->render('_dataBanner', [
        'model' => $model,
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Empleados'),
      'content' => $this->render('_dataEmpleado', [
        'model' => $model,
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Sucursales (ubicaciones)'),
      'content' => $this->render('_dataEmpresaSucursal', [
        'model' => $model,
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Historial de pagos'),
      'content' => $this->render('_dataPagoEmpresa', [
        'model' => $model,
      ]),
    ],
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Videos'),
      'content' => $this->render('empresa_videos',
        ['model' => new \app\models\base\Video(),'empresa_id'=>$model->id,'videos'=>$model->videos])
    ],
  ] ?>
  <?= TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
      'bordered' => true,
      'sideways' => true,
      'enableCache' => false
    ],
  ]); ?>
</div>