<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 24/11/17
 * Time: 11:51 AM
 * @var $this \yii\web\View
 * @var $model \app\models\Empresa
 */
$dataProvider = new \yii\data\ActiveDataProvider([
  'query' => $model->getPagoEmpresas()
    ->orderBy(['id' => SORT_DESC]),
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  [
    'attribute' => 'fecha',
    'label' => 'Fecha generado',
    'format' => 'date'
  ],
  [
    'attribute' => 'monto',
    'label' => 'Valor del pago',
    'format' => 'currency'
  ],
  [
    'attribute' => 'fecha_pago',
    'label' => 'Fecha de pago',
    'format' => 'date'
  ],
  [
    'attribute' => 'adminZona.usuario.nombres',
    'label' => 'Administrador de zona'
  ],
  [
    'attribute' => 'comision_admin_zona',
    'label' => 'Comisión Administrador',
    'format' => 'currency'
  ],
  [
    'header'=>'Foto',
    'value'=>function(\app\models\PagoEmpresa $model){
      return \yii\helpers\Html::a(\yii\helpers\Html::img("@web/$model->url_file",
        ['class'=>'img-responsive']),\yii\helpers\Url::to("@web/$model->url_file"),['data-pjax'=>0,'target'=>'_blank']);
    },
    'format'=>'raw'
  ]
];

echo \kartik\grid\GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
?>
