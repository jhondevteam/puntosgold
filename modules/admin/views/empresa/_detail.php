<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

?>
<div class="empresa-view">

  <div class="row">
    <div class="col-sm-9">
      <h2><?= Html::encode($model->nombre) ?></h2>
    </div>
  </div>

  <div class="col-md-8">
    <?php
    $gridColumn = [
      [
        'attribute' => 'adminZona.usuario.nombres',
        'label' => 'Administrador asignado',
      ],
      [
        'attribute' => 'categoria.nombre',
        'label' => 'Categoria',
      ],
      'descripcion:raw',
      'nit',
      'estado:boolean',
      'mode',
      'porcentaje:percent',
      'porcentaje_afiliado:percent',
      'puntos:currency',
      'puntos_afiliado:currency'
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="col-md-4">
    <?= Html::img("@web/$model->url_logo", ['alt' => $model->nombre, 'class' => 'img-thumbnail']) ?>
  </div>
</div>