<?php
/**
 * @var $model \app\models\Empresa
 */
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->empleados,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  [
    'attribute' => 'usuario.nombres',
    'label' => 'Usuario'
  ],
  [
    'attribute' => 'usuario.documento',
    'label' => 'Documento'
  ],
  [
    'attribute' => 'usuario.email',
    'label' => 'Email'
  ],
  [
    'label'=>'Opciones',
    'value'=>function(\app\models\Empleado $model){
      return \yii\helpers\Html::a('Enviar restablecer contraseña',
        ['/admin/empresa/send-password','id'=>$model->usuario_id],['class'=>'btn btn-xs btn-default btn-block']).
         \yii\helpers\Html::a('Restablecer por defecto',
        ['/admin/empresa/ress-password','id'=>$model->usuario_id],['class'=>'btn btn-xs btn-info btn-block']).
         \yii\helpers\Html::a('Editar datos',
        ['/admin/empresa/emp-edit','id'=>$model->usuario_id],['class'=>'btn btn-xs btn-primary btn-block']);
    },
    'format'=>'raw'
  ],
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
