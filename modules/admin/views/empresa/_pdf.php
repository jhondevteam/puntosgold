<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Empresa */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Empresa'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
                'attribute' => 'adminZona.id',
                'label' => 'Admin Zona'
            ],
        [
                'attribute' => 'categoria.nombre',
                'label' => 'Categoria'
            ],
        'nombre',
        'descripcion',
        'nit',
        'estado',
        'porcentaje',
        'porcentaje_afiliado',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerEmpleado->totalCount){
    $gridColumnEmpleado = [
        ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'usuario.id',
                'label' => 'Usuario'
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmpleado,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Empleado'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmpleado
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerEmpresaSucursal->totalCount){
    $gridColumnEmpresaSucursal = [
        ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'ciudad.nombre',
                'label' => 'Ciudad'
            ],
        'direccion',
        'descripcion',
        'lng',
        'lat',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmpresaSucursal,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Empresa Sucursal'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmpresaSucursal
    ]);
}
?>
    </div>
</div>
