<?php
use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalles'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Estado de la empresa'),
    'content' => $this->render('_dataEstado', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Empleados'),
    'content' => $this->render('_dataEmpleado', [
      'model' => $model,
      'row' => $model->empleados,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Sucursales (ubicaciones)'),
    'content' => $this->render('_dataEmpresaSucursal', [
      'model' => $model,
      'row' => $model->empresaSucursals,
    ]),
  ],
];
?>
<div class="panel panel-default">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]); ?>
  </div>
</div>
