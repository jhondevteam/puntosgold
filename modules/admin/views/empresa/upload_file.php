<?php
/**
 * User: jhon
 * Date: 15/12/16
 * Time: 04:17 PM
 * @var $action array
 * @var $empresa \app\models\Empresa
 * @var $model \app\models\Foto
 */
?>
<?php $form = \kartik\form\ActiveForm::begin([
  'action' => ['upload-foto','id'=>$empresa->id],
  'options' => ['enctype' => 'multipart/form-data'],
  'enableClientValidation' => false,
  'enableClientScript' => false
]) ?>
<h2 class="text-danger">Subir foto para el banner</h2>
<?= $form->field($model, 'file',['inputOptions'=>['id'=>'file'.$model->id]])
  ->widget(\kartik\file\FileInput::className(), [
    'options'=>['id'=>uniqid()],
    'language'=>'es'
  ])->hint('La imagen debe medir 800px * 500px') ?>
<?php \kartik\form\ActiveForm::end() ?>