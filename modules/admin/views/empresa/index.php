<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EmpresaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Empresas';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="empresa-index">
  <p>
    <?= Html::a('Agregar Empresa', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:<?= Yii::$app->request->get($searchModel->formName())? 'block' : 'none' ?>">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <div class="clearfix"></div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    [
      'attribute' => 'admin_zona_id',
      'label' => 'Administrador asignado',
      'value' => function (\app\models\base\Empresa $model) {
        return $model->adminZona? $model->adminZona->usuario->nombres : null;
      }
    ],
    [
      'attribute' => 'categoria_id',
      'label' => 'Categoria',
      'value' => function ($model) {
        return $model->categoria->nombre;
      }
    ],
    'nombre',
    'estado:boolean',
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '{view} {update}',
      'buttonOptions'=>[
          'class'=>'btn btn-primary btn-xs'
      ]
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empresa']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Full',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Export All Data</li>',
          ],
        ],
      ]),
    ],
  ]); ?>

</div>
