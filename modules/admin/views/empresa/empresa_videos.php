<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 01:02 PM
 * @var $this \yii\web\View
 * @var $model \app\models\base\Video
 * @var $videos \app\models\base\Video[]
 */
$data = new \yii\data\ArrayDataProvider([
  'allModels' => $videos
]);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="panel-title">Agregar nuevo video</h2>
    </div>
    <div class="panel-body">
      <?php $form = \kartik\form\ActiveForm::begin([
        'action' => ['new-video','id' => $empresa_id]
      ]) ?>
      <?= $form->field($model, 'descripcion') ?>
      <?= $form->field($model, 'url_video')
        ->hint('https://www.youtube.com/watch?v=<strong>vTjZ58Vo86g</strong> , el ID de toutube es el texto en negrilla.')?>
      <?= $form->field($model,'activo')->checkbox()->label('Visible') ?>
      <?= \yii\helpers\Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
      <?php \kartik\form\ActiveForm::end() ?>
    </div>
</div>

<?= \app\widgets\TPListView::widget([
  'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $videos]),
  'itemView' => '_empresa_videomuestra',
  'numberGroup' => 3
]) ?>


