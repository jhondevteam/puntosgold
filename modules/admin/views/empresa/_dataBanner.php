<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 03:00 PM
 * @var $model \app\models\Empresa
 */
?>
<div class="clearfix col-lg-12">
  <?= $this->render('upload_file', [
    'model' => new \app\models\Foto(),
    'empresa' => $model
  ]) ?>
</div>
<div class="form-group col-lg-12">
  <?php foreach ($model->fotos as $item): ?>
    <div class="col-xs-4">
      <?= \yii\helpers\Html::img("@web/$item->url_foto", ['class' => 'img-thumbnail img-responsive']) ?>
      <?= \yii\helpers\Html::a('<i class="glyphicon glyphicon-trash"></i> borrar', ['delete-foto', 'id' => $item->id], ['class' => 'btn btn-danger btn-xs']) ?>
    </div>
  <?php endforeach; ?>
</div>
<div class="clearfix"></div>
