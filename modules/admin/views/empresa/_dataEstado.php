<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 3/10/17
 * Time: 03:11 PM
 * @var $this \yii\web\View
 * @var $model \app\models\Empresa
 */

$pago = $model->getPagoPendiente()
?>
<div class="col-lg-6 <?= $pago == null? 'col-lg-offset-3' : '' ?>">
  <div class="alert alert-info text-center">
    <h5>Monto vendido:</h5>
    <hr>
    <h4><?= Yii::$app->formatter->asCurrency($model->getPendienteTotalVendido()) ?></h4>
  </div>
  <div class="alert alert-info text-center">
    <h5>Monto comision publicitaria:</h5>
    <hr>
    <h4><?= Yii::$app->formatter->asCurrency($model->getSaldoPagar()) ?></h4>
  </div>
  <div class="alert alert-info text-center">
    <h5>Monto pagos con puntos:</h5>
    <hr>
    <h4><?= Yii::$app->formatter->asCurrency($model->getPendienteTotalPuntosPagar()) ?></h4>
  </div>
  <div class="alert alert-info text-center">
    <h5>Total disponible para cobrar:</h5>
    <hr>
    <h4><?= Yii::$app->formatter->asCurrency($model->getSaldoPagar() - $model->getPendienteTotalPuntosPagar()) ?></h4>
  </div>
  <div class="alert alert-success text-center">
    <h5>Ganancia del administrador:</h5>
    <hr>
    <h4><?= Yii::$app->formatter->asCurrency($model->getGanaciaAdminstrador()) ?></h4>
  </div>
</div>
<div class="col-lg-6">
  <?php
  if ($pago != null): ?>
    <div class="alert alert-danger">
      <H3>La empresa tiene un pago pendiente por efectuar</H3>
      <p>Monto: <?= Yii::$app->formatter->asCurrency($pago->monto) ?></p>
      <p>Fecha generado: <?= Yii::$app->formatter->asDatetime($pago->fecha) ?></p>
    </div>
    <?php if ($pago->url_file != null): ?>
      <?= \yii\helpers\Html::img("@web/$pago->url_file", ['class' => 'img-responsive img-thumbnail']) ?>
      <p class="text-primary">
        La empresa ha enviado el siguente recibo para confirmar su pago.
      </p>
      <p>Fecha Enviado: <?= Yii::$app->formatter->asDate($pago->fecha_pago) ?></p>
    <?php endif; ?>

    <p>
      <?php if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN) and $pago->url_file): ?>
        Si considera que el pago fue enviado correctamente:
        <?= \yii\helpers\Html::a('Confirmar pago', ['/admin/empresa/estado', 'pago_id' => $pago->id, 'estado' => 1], ['class' => 'btn btn-info', 'data-confirm' => 'Seguro desea confirmar el pago']) ?>
        <?= \yii\helpers\Html::a('Rechazar pago', ['/admin/empresa/estado', 'pago_id' => $pago->id, 'estado' => 2], ['class' => 'btn btn-danger', 'data-confirm' => 'Seguro desea rechazar el pago']) ?>
      <?php endif; ?>
    </p>

  <?php endif; ?>
</div>