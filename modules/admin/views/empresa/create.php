<?php


/* @var $this yii\web\View */
/* @var $model app\models\Empresa */
/* @var $user app\models\Usuario */

$this->title = 'Agregar Empresa';
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-create">
  <?= $this->render('_form', [
    'model' => $model,
    'user'=>$user
  ]) ?>
</div>
