<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 15/01/18
 * Time: 12:19 PM
 * @var $user \app\models\Usuario
 * @var $model \app\models\Empresa
 * @var $this \yii\web\View
 */

use yii\bootstrap\Html;
use yii\widgets\DetailView;

?>
<div class="col-lg-6">
  <?= Html::img("@web/$model->url_logo",['alt'=>$model->nombre,'class'=>'img-thumbnail']) ?>
  <?php
  $gridColumn = [
    'nombre',
    'descripcion:html',
    'nit',
    'estado:boolean',
    'mode',
    'porcentaje:percent',
    'porcentaje_afiliado:percent',
    'puntos:currency',
    'puntos_afiliado:currency'
  ];
  echo DetailView::widget([
    'model' => $model,
    'attributes' => $gridColumn
  ]);
  ?>
</div>
<div class="col-lg-6">
  <?php $form = \kartik\form\ActiveForm::begin([
    'enableAjaxValidation' => true
  ]) ?>
  <?= $form->field($user,'nombres') ?>
  <?= $form->field($user,'documento') ?>
  <?= $form->field($user,'email') ?>
  <?= Html::submitButton('Guardar',['class'=>'btn btn-success']) ?>
  <?php \kartik\form\ActiveForm::end() ?>
</div>
