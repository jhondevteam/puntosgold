<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->empresaSucursals,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  [
    'attribute' => 'ciudad.nombre',
    'label' => 'Ciudad'
  ],
  'descripcion',
  'direccion',
  'lat',
  'lng',
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
