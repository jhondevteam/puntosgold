<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Zona */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">DETALLES DE LAS ZONAS</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary pull-right']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
         'class' => 'btn btn-danger pull-right',
         'data' => [
          'confirm' => '¿Seguro desea eliminar este item?',
          'method' => 'post',
         ],
        ])
        ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        $gridColumn = [
         'ciudad_id',
         'nombre',
        ];
        echo DetailView::widget([
         'model' => $model,
         'attributes' => $gridColumn
        ]);
        ?>
        <?php
        if($providerAdminZona->totalCount){
         $gridColumnAdminZona = [
          ['class' => 'yii\grid\SerialColumn'],
          [
           'attribute' => 'usuario.id',
           'label' => 'Usuario'
          ],
          'telefonos',
          'direccion',
         ];
         echo Gridview::widget([
          'dataProvider' => $providerAdminZona,
          'pjax' => true,
          'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-admin-zona']],
          'panel' => [
           'type' => GridView::TYPE_PRIMARY,
           'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Admin Zona'),
          ],
          'columns' => $gridColumnAdminZona
         ]);
        }
        ?>
       </div>
       
       
      </div>
    </div>