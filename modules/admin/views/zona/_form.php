<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Zona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zona-form">

  <?php $form = ActiveForm::begin(); ?>
  <div class="form-group">
    <?= Html::label('Pais') ?>
    <?= \kartik\widgets\Select2::widget([
      'name' => 'pais',
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Pais::find()
        ->orderBy('nombre')->all(), 'id', 'nombre'),
      'options' => [
        'id' => 'pais_id',
        'placeholder' => 'Seleccione'
      ]
    ]) ?>
  </div>
  <div class="form-group">
    <?= Html::label('Departamento') ?>
    <?= \kartik\depdrop\DepDrop::widget([
      'name' => 'departamento',
      'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
      'pluginOptions' => [
        'depends' => ['pais_id'],
        'placeholder' => 'Seleccione...',
        'url' => Url::to(['get-locale', 'type' => 'D'])
      ],
      'options' => [
        'id' => 'departamento_id'
      ]
    ]) ?>
  </div>
  <?= $form->field($model, 'ciudad_id')->widget(\kartik\depdrop\DepDrop::className(), [
    'type' => \kartik\depdrop\DepDrop::TYPE_SELECT2,
    'pluginOptions' => [
      'depends' => ['departamento_id'],
      'placeholder' => 'Seleccione...',
      'url' => Url::to(['get-locale', 'type' => 'C'])
    ]
  ]) ?>
  <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Nombre']) ?>
  <?php
  $forms = [
    [
      'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('AdminZona'),
      'content' => $this->render('_formAdminZona', [
        'row' => \yii\helpers\ArrayHelper::toArray($model->adminZonas),
      ]),
    ],
  ];
  ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
