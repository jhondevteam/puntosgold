<?php


/* @var $this \yii\web\View */
/* @var $data \yii\data\ActiveDataProvider */
$this->title = 'Empresas transportadoras';
?>
<p class="text-right">
  <?= \yii\helpers\Html::a('Crear empresa transportadora',['empresa-transportadora/create'],['class'=>'btn btn-primary']) ?>
</p>
<?= \kartik\grid\GridView::widget([
   'dataProvider' => $data,
   'columns' => [
      'nombre',
      [
         'label' => 'Departamentos asignado',
         'value' => function (\app\models\EmpresaTransportadora $model) {
           return implode(', ', $model->getDetapartamentos(true));
         },
      ],
      'porcent'
   ]
]) ?>
