<?php

/* @var $this \yii\web\View */
/* @var $model \app\models\EmpresaTransportadora */
$this->title = 'Crear empresa de transporte';
?>

<?php $form = \kartik\form\ActiveForm::begin() ?>
<?= $form->field($model, 'nombre') ?>
<?= $form->field($model, 'departamento_ids[]')->widget(\kartik\select2\Select2::className(), [
   'data' => \yii\helpers\ArrayHelper::map(\app\models\Departamento::find()
      ->orderBy('nombre')->all(), 'id', 'nombre'),
   'options' => ['multiple' => true]
]) ?>
<?= $form->field($model, 'porcent')->widget(\yii\widgets\MaskedInput::className(), [
   'mask' => '0.9[9][9]'
]) ?>
<?= \yii\helpers\Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
<?php \kartik\form\ActiveForm::end() ?>
