<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AfiliadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Afiliados';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">ADMINISTRACIÓN DE AFILIADOS</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button pull-right']) ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         <div class="search-form" style="display:none">
          <?= $this->render('_search', ['model' => $searchModel]); ?>
         </div>
        <?php
        $gridColumn = [
         ['class' => 'yii\grid\SerialColumn'],
         [
          'class' => 'kartik\grid\ExpandRowColumn',
          'width' => '50px',
          'value' => function ($model, $key, $index, $column) {
           return GridView::ROW_COLLAPSED;
          },
          'detailUrl' => \yii\helpers\Url::to(['expand']),
         ],
         [
          'attribute' => 'grupo_id',
          'label' => 'Afiliador',
          'value' => function (\app\models\Afiliado $model) {
           if ($model->grupo)
            return $model->grupo->afiliado->usuario->nombres;
           return null;
          }
         ],
         [
          'attribute' => 'usuario_id',
          'label' => 'Usuario',
          'value' => function (\app\models\Afiliado $model) {
           return $model->usuario->nombres;
          }
         ],
         'fecha_registro:datetime',
         'puntos:currency',
         [
          'class' => 'yii\grid\ActionColumn',
          'template' => "{view}"
         ],
        ];
        ?>
        <?= GridView::widget([
         'dataProvider' => $dataProvider,
         'columns' => $gridColumn,
         'pjax' => true,
         'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-afiliado']],
         'export' => false,
         'toolbar' => [
          '{export}',
          ExportMenu::widget([
           'dataProvider' => $dataProvider,
           'columns' => $gridColumn,
           'target' => ExportMenu::TARGET_BLANK,
           'fontAwesome' => true,
           'dropdownOptions' => [
            'label' => 'Full',
            'class' => 'btn btn-default',
            'itemsBefore' => [
             '<li class="dropdown-header">Export All Data</li>',
            ],
           ],
           'exportConfig' => [
            ExportMenu::FORMAT_PDF => false
           ]
          ]),
         ],
        ]); ?>
       </div>
      </div>
    </div>