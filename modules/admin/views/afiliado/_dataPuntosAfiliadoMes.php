<?php
/**
 * @var $model \app\models\Afiliado
 */
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new \yii\data\ActiveDataProvider([
  'query' => $model->getPuntosAfiliadoMes()
  ->orderBy(['fecha'=>SORT_DESC]),
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute'=>'fecha','format'=>['date','php:F/Y']],
  'puntos:currency',
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
?>
<div class="alert alert-info">
  <p><strong>Nota:</strong>
    Son los puntos obtenidos de sus afiliados
    mensualmente se incluyen afiliados directos y de su segundo nivel,
    los puntos de afiliados se acumulan y se
    entregan al usuario el 1er dia de cada mes
  </p>
</div>
