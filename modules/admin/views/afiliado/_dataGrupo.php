<?php
/**
 * @var $model \app\models\Afiliado
 * @var $this \yii\web\View
 */
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
$items = [];
foreach ($model->grupos as $key => $item) {
  $items[] = [
    'label'=>"Grupo de afiliados ".($key+1).".",
    'content'=>$this->render('_grupo',[
      'model'=>$item
    ])
  ];
}
echo \kartik\tabs\TabsX::widget([
  'options'=>[
    'id'=>'tab-grupo-'.$model->id
  ],
  'items'=>$items
]);


