<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AfiliadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-afiliado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
          ->orderBy('nombres')->asArray()->all(), 'id', 'nombres'),
        'options' => ['placeholder' => 'Usuario'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'fecha_registro')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Fecha Registro',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'puntos')->textInput(['placeholder' => 'Puntos']) ?>

    <?= $form->field($model, 'code_invitacion')->textInput(['maxlength' => true, 'placeholder' => 'Code Invitacion']) ?>

    <?php /* echo $form->field($model, 'app_token')->textInput(['maxlength' => true, 'placeholder' => 'App Token']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
