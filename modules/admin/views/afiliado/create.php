<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Afiliado */

$this->title = 'Create Afiliado';
$this->params['breadcrumbs'][] = ['label' => 'Afiliados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="afiliado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
