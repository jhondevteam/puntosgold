<?php
/**
 * @var $model \app\models\Afiliado
 */
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Afiliado'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Compras Afiliado'),
    'content' => $this->render('_dataCompraAfiliado', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Afiliados'),
    'content' => $this->render('_dataGrupo', [
      'model' => $model,
      'row' => $model->grupos,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Puntos referidos por mes'),
    'content' => $this->render('_dataPuntosAfiliadoMes', [
      'model' => $model,
    ]),
  ],
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Puntos de sub-Referidos por Mes'),
    'content' => $this->render('_dataPuntosReferidosMes', [
      'model' => $model,
    ]),
  ],
];
?>
<div class="panel panel-default">
  <div class="panel-body">
    <?= \kartik\tabs\TabsX::widget([
      'options' => ['id' => 'tab-' . $model->id],
      'items' => $items,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]); ?>
  </div>
</div>