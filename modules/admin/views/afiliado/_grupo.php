<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 12/07/17
 * Time: 11:53 AM
 * @var $model \app\models\Grupo
 */
$gridColums = [
  [
    'attribute' => 'usuario_id',
    'label' => 'Usuario',
    'value' => function (\app\models\Afiliado $model) {
      return $model->usuario->nombres;
    }
  ],
  'fecha_registro:datetime',
  'puntos:currency',
];
echo \kartik\grid\GridView::widget([
  'dataProvider'=>new \yii\data\ArrayDataProvider([
    'allModels'=>$model->afiliados,
    'pagination'=>false
  ]),
  'columns'=>$gridColums
])
?>
