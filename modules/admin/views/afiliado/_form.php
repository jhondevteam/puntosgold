<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Afiliado */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'CompraAfiliado', 
        'relID' => 'compra-afiliado', 
        'value' => \yii\helpers\Json::encode($model->compraAfiliados),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Grupo', 
        'relID' => 'grupo', 
        'value' => \yii\helpers\Json::encode($model->grupos),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PuntosAfiliadoMes', 
        'relID' => 'puntos-afiliado-mes', 
        'value' => \yii\helpers\Json::encode($model->puntosAfiliadoMes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PuntosReferidosMes', 
        'relID' => 'puntos-referidos-mes', 
        'value' => \yii\helpers\Json::encode($model->puntosReferidosMes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="afiliado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'grupo_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Grupo::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Grupo'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose Usuario'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'fecha_registro')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:Y-m-d',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Fecha Registro',
                'autoclose' => true
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'puntos')->textInput(['placeholder' => 'Puntos']) ?>

    <?= $form->field($model, 'code_invitacion')->textInput(['maxlength' => true, 'placeholder' => 'Code Invitacion']) ?>

    <?= $form->field($model, 'app_token')->textInput(['maxlength' => true, 'placeholder' => 'App Token']) ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('CompraAfiliado'),
            'content' => $this->render('_formCompraAfiliado', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->compraAfiliados),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Grupo'),
            'content' => $this->render('_formGrupo', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->grupos),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PuntosAfiliadoMes'),
            'content' => $this->render('_formPuntosAfiliadoMes', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->puntosAfiliadoMes),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PuntosReferidosMes'),
            'content' => $this->render('_formPuntosReferidosMes', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->puntosReferidosMes),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
