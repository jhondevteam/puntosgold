<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CompraAfiliadoSearch */
/* @var $afiliado app\models\Afiliado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-compra-afiliado-search">

  <?php $form = ActiveForm::begin([
    'action' => ['view','id'=>$afiliado->id],
    'method' => 'get',
    'fieldConfig' => [
      'options' => ['class' => 'col-lg-3']
    ]
  ]); ?>

  <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::className(), [
    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>

  <?= $form->field($model, 'fecha_hasta')->widget(\kartik\date\DatePicker::className(), [

    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>

  <?= $form->field($model, 'empresa_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Empresa::find()
      ->orderBy('nombre')->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione una empresa'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'comentario')->label('N° factura') ?>

  <div class="form-group">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
