<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Afiliado */

?>
<div class="afiliado-view">
  <div class="row">
    <?php
    $gridColumn = [
      [
        'attribute' => 'grupo.afiliado.usuario.nombres',
        'label' => 'Afiliador',
      ],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Nombre',
      ],
      [
        'label'=>'Cedula',
        'attribute'=>'usuario.documento',
      ],
      [
        'label'=>'Email',
        'attribute'=>'usuario.email'
      ],
      'fecha_registro:datetime',
      'puntos:currency',
      'code_invitacion',
      'app_token:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>