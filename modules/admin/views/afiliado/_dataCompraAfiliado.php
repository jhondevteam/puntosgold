<?php
/**
 * @var $model \app\models\Afiliado
 */
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;
$finMes = date('Y-m-d 23:59:59');
$initMes = date('Y-m-01 00:00:00');

$query = $model->getCompraAfiliados()
  ->joinWith(['empleado.empresa','empleado.usuario'])
  ->where(['between', 'fecha', $initMes, $finMes]);

$dataProvider = new \yii\data\ActiveDataProvider([
  'query' => $query
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  'empleado.empresa.nombre',
  'fecha:datetime',
  'monto:currency',
  'puntos:currency',
  'pago_puntos:boolean'
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
echo \kartik\alert\Alert::widget([
  'title' => 'información',
  'body' => 'Se muestra las compras del actual mes',
]);