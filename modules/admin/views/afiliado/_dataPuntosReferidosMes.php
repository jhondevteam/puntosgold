<?php
/**
 * @var $model \app\models\Afiliado
 */
use kartik\grid\GridView;

$dataProvider = new \yii\data\ActiveDataProvider([
  'query' => $model->getPuntosReferidosMes()
  ->orderBy(['fecha'=>SORT_DESC]),
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  ['attribute'=>'fecha','format'=>['date','php:F/Y']],
  'puntos:currency',
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
?>
<div class="alert alert-info">
  <p><strong>Nota:</strong>
    Son los puntos obtenidos de sus indirectos
    mensualmente se incluyen afiliados del 3 al 5 nivel,
    los puntos de referidos se acumulan y se
    entregan al usuario el 1er dia de cada mes
  </p>
</div>