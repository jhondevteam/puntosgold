<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Afiliado */
/* @var $compras \yii\data\ArrayDataProvider */
/* @var $search \app\models\search\CompraAfiliadoSearch */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Afiliados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="afiliado-view">
  <div>
    <?php
    $gridColumn = [
      [
        'attribute' => 'grupo.afiliado.usuario.nombres',
        'label' => 'Afiliador',
      ],
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'label' => 'Cedula',
        'attribute' => 'usuario.documento',
      ],
      [
        'label' => 'Email',
        'attribute' => 'usuario.email'
      ],
      'fecha_registro:datetime',
      'puntos:currency',
      'code_invitacion',
      'app_token:boolean',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
    <?php if ($model->afiDatos): ?>
      <?php $gridColumn = [
        'nacimiento', 'telefono', 'direccion', 'sexo'
      ];
      echo DetailView::widget([
        'model' => $model->afiDatos,
        'attributes' => $gridColumn
      ]); ?>
    <?php endif; ?>
  </div>

  <div>
    <h1 class="text-primary">Afiliados</h1>
    <?= $this->render('_dataGrupo', [
      'model' => $model
    ])
    ?>
  </div>

  <div>
    <h1 class="text-primary">Puntos de referidos y sub-referidos</h1>
    <?php
    $items = [
      [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Puntos referidos por mes'),
        'content' => $this->render('_dataPuntosAfiliadoMes', [
          'model' => $model,
        ]),
      ],
      [
        'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Puntos de sub-Referidos por Mes'),
        'content' => $this->render('_dataPuntosReferidosMes', [
          'model' => $model,
        ]),
      ],
    ];
    echo \kartik\tabs\TabsX::widget([
      'options' => ['id' => 'tab-' . $model->id],
      'items' => $items,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]);
    ?>
  </div>
  <div>
    <h1 class="text-primary">Compras</h1>
    <?= $this->render('_search_ventas', [
        'model' => $search,
        'afiliado'=>$model
      ]) ?>
    <div class="clearfix"></div>
    <?php
    $gridColumns = [
      ['class' => 'yii\grid\SerialColumn'],
      'empleado.empresa.nombre',
      'fecha:datetime',
      'monto:currency',
      'puntos:currency',
      'pago_puntos:boolean'
    ];
    echo GridView::widget([
      'dataProvider' => $compras,
      'columns' => $gridColumns,
      'containerOptions' => ['style' => 'overflow: auto'],
      'pjax' => true,
      'beforeHeader' => [
        [
          'options' => ['class' => 'skip-export']
        ]
      ],
      'export' => [
        'fontAwesome' => true
      ],
      'bordered' => true,
      'striped' => true,
      'condensed' => true,
      'responsive' => true,
      'hover' => true,
      'showPageSummary' => false,
      'persistResize' => false,
    ]); ?>
  </div>
</div>
