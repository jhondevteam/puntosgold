<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PreguntasRespuestas */

$this->title = 'Update Preguntas Respuestas: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Preguntas Respuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Formulario para la actualización de preguntas y respuestas</h3>
      </div>
      <div class="panel-body">
       <?= $this->render('_form', [
        'model' => $model,
       ]) ?>
      </div>
    </div>