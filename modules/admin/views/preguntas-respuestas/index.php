<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PreguntasRespuestasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Preguntas Respuestas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Administración de preguntas y respuestas</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a('Nueva pregunta respuesta', ['create'], ['class' => 'btn btn-success']) ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= GridView::widget([
         'dataProvider' => $dataProvider,
         'filterModel' => $searchModel,
         'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
          'id',
          'pregunta',
          'respuesta',
          ['class' => 'yii\grid\ActionColumn'],
         ],
        ]); ?>
       </div>
      </div>
    </div>
