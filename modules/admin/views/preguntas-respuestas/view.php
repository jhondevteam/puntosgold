<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PreguntasRespuestas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Preguntas Respuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Detalles de la pregunta - respuesta</h3>
      </div>
      <div class="panel-body">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
         'class' => 'btn btn-danger',
         'data' => [
          'confirm' => '¿Desea  eliminar el item?',
          'method' => 'post',
         ],
        ]) ?>
       </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?= DetailView::widget([
         'model' => $model,
         'attributes' => [
          'id',
          'pregunta',
          'respuesta',
         ],
        ]) ?>
       </div>
      </div>
    </div>