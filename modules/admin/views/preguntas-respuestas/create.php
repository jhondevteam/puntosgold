<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PreguntasRespuestas */

$this->title = 'Create Preguntas Respuestas';
$this->params['breadcrumbs'][] = ['label' => 'Preguntas Respuestas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-primary">
      <div class="panel-heading">
        <h3 class="panel-title">Formulario para ingresar preguntas y respuestas</h3>
      </div>
      <div class="panel-body">
       <?= $this->render('_form', [
        'model' => $model,
       ]) ?>

      </div>
    </div>