<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PreguntasRespuestas */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <?= $form->field($model, 'pregunta')->textInput(['maxlength' => true]) ?>
  <div class="form-group">
   <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <?= $form->field($model, 'respuesta')->textarea(['rows' => '10']) ?>
</div>
<?php ActiveForm::end(); ?>
