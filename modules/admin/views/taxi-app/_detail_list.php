<?php

/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 15/02/19
 * Time: 11:51 AM
 */

/* @var $this \yii\web\View */
/* @var $model null|\app\models\Driver */
$data = \yii\helpers\Json::decode($model->car_info);
if ($data == null)
  $data = [];

$value = function ($property) use ($data) {
  if (isset($data[$property])) {
    Yii::info($data[$property]);
    return $data[$property];
  } else {
    return 'No Especificado';
  }
}

?>
<div class="col-sm-6">
  <?= \yii\widgets\DetailView::widget([
    'model' => $data,
    'attributes' => [
      ['label' => 'Marca', 'value' => $value('brand')],
      ['label' => 'Modelo', 'value' => $value('model')],
      ['label' => 'Placa', 'value' => $value('plate')],
      ['label' => 'Documento cargado', 'value' =>
        \yii\helpers\Html::a($value('doc_url'), $value('doc_url'), ['target' => '_black']),
        'format' => 'raw'
      ],
    ]
  ]) ?>
</div>
