<?php


/* @var $this \yii\web\View */

/* @var $model null|\app\models\Driver */

use yii\helpers\Html;
use yii\web\JqueryAsset;

?>
  <div id="driver" class="panel panel-default" data-driver_id="<?= $model->usuario->access_token ?>">
    <div class="panel-body">
      <div class="col-md-6">
        <?= \yii\widgets\DetailView::widget([
           'model' => $model,
           'attributes' => [
              'usuario.nombres',
              'driverCategory.name',
              'credits',
              'porcent:percent',
              'code',
              'active:boolean'
           ]
        ]) ?>
        <h4>Informacion del auto</h4>
        <?= $this->render('_detail_list', [
           'model' => $model
        ]) ?>
      </div>
      <div class="col-md-6">
        <?php $form = \yii\widgets\ActiveForm::begin([
           'action' => ['view-driver', 'id' => $model->id]
        ]) ?>
        <?= $form->field($model, 'departamento_id')->dropDownList(
           \yii\helpers\ArrayHelper::map(\app\models\Departamento::find()
              ->orderBy('nombre')->all(), 'id', 'nombre')
        ) ?>
        <?= \yii\helpers\Html::submitButton('Guardar', ['class' => 'btn btn-info']) ?>
        <?php \yii\widgets\ActiveForm::end() ?>
        <h1>
          Cambio de contraseña
        </h1>
        <?= Html::beginForm(['update-password', 'id' => $model->id]) ?>
        <?= Html::textInput('password', '', ['minlenght' => 6, 'required' => true, 'class' => 'form-control']) ?>
        <?= Html::submitButton('Confirmar') ?>
        <?= Html::endForm() ?>
      </div>
      <div class="col-lg-12">
        <h4>Compras de credito confirmadas</h4>
        <?= \yii\grid\GridView::widget([
           'dataProvider' => new \yii\data\ActiveDataProvider([
              'query' => $model->getDriverPurchases()
                 ->where(['driver_purchase.is_confirm' => true])
           ])
        ]) ?>
      </div>
    </div>
  </div>

<?php
$this->registerJsFile('https://www.gstatic.com/firebasejs/5.8.4/firebase.js', [
   'position' => $this::POS_HEAD
]);
$this->registerJs('
 var config = {
    apiKey: "AIzaSyAf3s1qepgZT9W6BYv5NkhXw5IMONrsDCE",
    authDomain: "taxidriver-23cbd.firebaseapp.com",
    databaseURL: "https://taxidriver-23cbd.firebaseio.com",
    projectId: "taxidriver-23cbd",
    storageBucket: "taxidriver-23cbd.appspot.com",
    messagingSenderId: "927678779355"
  };
  firebase.initializeApp(config);', $this::POS_HEAD);
$this->registerJsFile("@web/js/update-password-firebase.js", [
   'depends' => [JqueryAsset::class]
]) ?>