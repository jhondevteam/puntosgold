<?php

/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 18/02/19
 * Time: 12:24 PM
 */

/* @var $this \yii\web\View */
/* @var $data \app\models\DriverCategory[]|array */
$this->title = 'Categoria de autos';
?>
<?= \kartik\grid\GridView::widget([
  'dataProvider' => $data,
  'columns' => [
    'id',
    [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'icon',
      'editableOptions' => [
        'formOptions' => [
          'action' => ['update-category-file'],
          'options' => [
            'enctype' => 'multipart/form-data'
          ],
        ],
        'inputType' => \kartik\editable\Editable::INPUT_FILEINPUT,
        'options' => [
          'options' => ['accept' => 'image/*'],
          'pluginOptions' => [
            'showRemove' => false,
            'showUpload' => false,
          ]
        ],
      ],
      'value' => function (\app\models\DriverCategory $model) {
        return \yii\helpers\Html::img("@web/" . $model->icon);
      },
      'format' => 'raw',
      'header' => 'Icono de lista (50*50)',
    ],
    [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'icon_map',
      'editableOptions' => [
        'formOptions' => [
          'action' => ['update-category-file'],
          'options' => [
            'enctype' => 'multipart/form-data'
          ],
        ],
        'inputType' => \kartik\editable\Editable::INPUT_FILEINPUT,
        'options' => [
          'options' => ['accept' => 'image/*'],
          'pluginOptions' => [
            'showRemove' => false,
            'showUpload' => false,
          ]
        ],
      ],
      'value' => function (\app\models\DriverCategory $model) {
        return \yii\helpers\Html::img("@web/" . $model->icon_map);
      },
      'format' => 'raw',
      'header' => 'Icono del mapa (32*32)'
    ],
    [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'name',
    ],
    [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'porcent',
      'format' => 'percent',
      'enableSorting' => false,
      'editableOptions' => [
        'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
        'widgetClass' => 'yii\widgets\MaskedInput',
        'options' => [
          'mask' => '0.9[9][9]'
        ]
      ]
    ],
    [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'porcent_affiliate',
      'format' => 'percent',
      'enableSorting' => false,
      'editableOptions' => [
        'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
        'widgetClass' => 'yii\widgets\MaskedInput',
        'options' => [
          'mask' => '0.9[9][9]'
        ]
      ]
    ]
  ]
]) ?>
