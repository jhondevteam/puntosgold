<?php

/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 15/02/19
 * Time: 11:38 AM
 */

use kartik\export\ExportMenu;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $data \yii\data\ActiveDataProvider */
/* @var $modelSearch \app\models\Driver */
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
$this->title = 'Administración de taxistas';
$columns = [
   'id', [
      'class' => \kartik\grid\ExpandRowColumn::class,
      'value' => function () {
        return \kartik\grid\GridView::ROW_COLLAPSED;
      },
      'detailUrl' => \yii\helpers\Url::to(['taxi-app/detail'])
   ],
   [
      'class' => 'kartik\grid\EditableColumn',
      'attribute' => 'active',
      'format' => 'boolean',
      'editableOptions' => [
         'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
         'data' => [
            '0' => 'Inactivo',
            '1' => 'Activo'
         ]
      ]
   ],
   ['label' => 'Nombre', 'attribute' => 'usuario.nombres'],
   ['label' => 'Categoria', 'attribute' => 'driverCategory.name'],
   ['label' => 'Saldo', 'attribute' => 'credits'],
   ['class' => 'kartik\grid\EditableColumn',
      'attribute' => 'porcent',
      'format' => 'percent',
      'enableSorting' => false,
      'editableOptions' => [
         'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
         'widgetClass' => 'yii\widgets\MaskedInput',
         'options' => [
            'mask' => '0.9[9][9]'
         ]
      ]
   ],
   [
      'label' => 'Telefono',
      'value' => 'telefono',
   ],
    'credits',
    'departamento.nombre',
   [
      'label' => 'Opciones',
      'value' => function (\app\models\Driver $model) {
        return Html::a('VER', ['view-driver', 'id' => $model->id], ['class' => 'btn btn-default']);
      },
      'format' => 'raw'
   ]
];
?>
  <p>
    <?= Html::a('Busqueda', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="col-lg-12 row">
    <div class="search-form" style="display:none">
      <?php $form = ActiveForm::begin([
         'action' => ['list'],
         'method' => 'get',
         'fieldConfig' => ['options' => ['class' => 'col-md-6']]
      ]) ?>
      <?= $form->field($modelSearch, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
         'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()
            ->orderBy('nombres')->asArray()->all(), 'id', function ($model) {
           return $model['nombres'] . ' - ' . $model['documento'];
         }),
         'options' => ['placeholder' => 'Usuario'],
         'pluginOptions' => [
            'allowClear' => true
         ],
      ]); ?>
      <?= $form->field($modelSearch, 'active')->dropDownList([
         '' => 'All',
         '1' => 'Solo activos',
         '0' => 'Solo inactivos'
      ]) ?>
      <?= $form->field($modelSearch, 'driver_category_id')->dropDownList(
         \yii\helpers\ArrayHelper::map(\app\models\DriverCategory::find()->orderBy('name')
            ->asArray()->all(), 'id', 'name'),
         ['prompt' => 'Seleccione la categoria']
      ) ?>
      <?= $form->field($modelSearch, 'departamento_id')->dropDownList(
         \yii\helpers\ArrayHelper::map(\app\models\Departamento::find()->orderBy('nombre')
            ->asArray()->all(), 'id', 'nombre'),
         ['prompt' => 'Seleccione departamento']
      ) ?>
      <div class="col-lg-12 row text-right">
        <?= \kartik\helpers\Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
      </div>
      <?php ActiveForm::end() ?>
    </div>
  </div>
  <div class="clearfix"></div>
<?= ExportMenu::widget([
   'dataProvider' => $data,
   'columns' => $columns
]) ?>
  <div class="clearfix"></div>
<?= \kartik\grid\GridView::widget([
   'dataProvider' => $data,
   'pager' => [
      'maxButtonCount' => 24
   ],
   'columns' => $columns
]) ?>
<?php
$this->registerJsFile('https://www.gstatic.com/firebasejs/5.8.4/firebase.js', [
   'position' => $this::POS_HEAD
]);
$this->registerJs('
 var config = {
    apiKey: "AIzaSyAf3s1qepgZT9W6BYv5NkhXw5IMONrsDCE",
    authDomain: "taxidriver-23cbd.firebaseapp.com",
    databaseURL: "https://taxidriver-23cbd.firebaseio.com",
    projectId: "taxidriver-23cbd",
    storageBucket: "taxidriver-23cbd.appspot.com",
    messagingSenderId: "927678779355"
  };
  firebase.initializeApp(config);', $this::POS_HEAD);
$this->registerJsFile("@web/js/getDatos.js", [
   'depends' => \yii\web\JqueryAsset::class
])
?>