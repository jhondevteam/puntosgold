<?php

/**
 * Created by PhpStorm.
 * User: jhon
 * Date: 18/02/19
 * Time: 02:58 PM
 */

/* @var $this \yii\web\View */
/* @var $model \app\models\DriverCategoryState */
/* @var $data \yii\data\ActiveDataProvider */
$this->title = 'Porcentajes de categoria por departamento';
?>
<div class="panel panel-danger">
  <div class="panel-heading">
    <h2 class="panel-title">Asignar diferente porcentaje</h2>
  </div>
  <div class="panel-body">
    <?php $form = \yii\widgets\ActiveForm::begin([
      'action' => ['add-state-category'],
      'enableAjaxValidation' => true
    ]) ?>
    <?= $form->field($model, 'driver_category_id')->dropDownList(
      \yii\helpers\ArrayHelper::map(\app\models\DriverCategory::find()->all(), 'id', 'name'),
      ['prompt' => 'Seleccione']
    ) ?>
    <?= $form->field($model, 'departamento_id')->widget(\kartik\select2\Select2::class, [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Departamento::find()->all(), 'id', 'nombre'),
      'options' => ['prompt' => 'Seleccione'],
      'pluginOptions' => [
        'allowClear' => 'true'
      ]
    ]) ?>
    <?= $form->field($model, 'porcent')->widget(\yii\widgets\MaskedInput::class, [
      'mask' => '0.9[9][9]'
    ]) ?>
    <?= $form->field($model, 'porcent_affiliate')->widget(\yii\widgets\MaskedInput::class, [
      'mask' => '0.9[9][9]'
    ]) ?>
    <?= \yii\helpers\Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    <?php \yii\widgets\ActiveForm::end() ?>
  </div>
</div>
<div class="panel panel-info">
  <div class="panel-heading">
    <h2 class="panel-title">Asignados</h2>
  </div>
  <div class="panel-body">
    <div class="row col-lg-12">
      <?php $form = \yii\widgets\ActiveForm::begin([
        'fieldConfig' => ['options' => ['class' => 'col-lg-6']],
        'action' => ['state-categories'],
        'enableClientScript' => false
      ]) ?>
      <?= $form->field($model, 'driver_category_id')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\DriverCategory::find()->all(), 'id', 'name'),
        ['prompt' => 'Seleccione']
      ) ?>
      <?= $form->field($model, 'departamento_id')
        ->widget(\kartik\select2\Select2::class, [
          'id' => 'departamento',
          'data' => \yii\helpers\ArrayHelper::map(\app\models\Departamento::find()->all(), 'id', 'nombre'),
          'options' => ['prompt' => 'Seleccione', 'id' => 'departamento'],
          'pluginOptions' => [
            'allowClear' => 'true'
          ]
        ]) ?>
      <?= \yii\helpers\Html::submitButton('Buscar', ['class' => 'btn btn-success']) ?>
      <?php \yii\widgets\ActiveForm::end() ?>
    </div>
    <div class=" col-lg-12 row">
      <?= \kartik\grid\GridView::widget([
        'dataProvider' => $data,
        'columns' => [
          'id', [
            'attribute' => 'departamento_id',
            'value' => 'departamento.nombre'
          ],
          [
            'attribute' => 'category_id',
            'value' => 'driverCategory.name'
          ],
          [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'porcent',
            'format' => 'percent',
            'enableSorting' => false,
            'editableOptions' => [
              'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
              'widgetClass' => 'yii\widgets\MaskedInput',
              'options' => [
                'mask' => '0.9[9][9]'
              ]
            ]
          ],
          [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'porcent_affiliate',
            'format' => 'percent',
            'enableSorting' => false,
            'editableOptions' => [
              'inputType' => \kartik\editable\Editable::INPUT_WIDGET,
              'widgetClass' => 'yii\widgets\MaskedInput',
              'options' => [
                'mask' => '0.9[9][9]'
              ]
            ]
          ],
          [
            'header' => 'Borrar',
            'value' => function (\app\models\DriverCategoryState $model) {
              return \yii\helpers\Html::a('Borrar', ['taxi-app/delete-state-category', 'id' => $model->id],
                ['data-confirm' => 'Seguro desea eliminar esta asignación', 'class' => 'btn btn-xs btn-danger']);
            },
            'format' => 'raw'
          ]
        ]
      ]) ?>
    </div>
  </div>
</div>