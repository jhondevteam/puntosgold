<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Accionista */

$this->title = 'Create Accionista';
$this->params['breadcrumbs'][] = ['label' => 'Accionistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accionista-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
