<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AccionistaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accionistas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accionista-index">

  <h1><?= Html::encode($this->title) ?></h1>
  <?php Pjax::begin(); ?>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Create Accionista', ['create'], ['class' => 'btn btn-success']) ?>
  </p>
  
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      
      'id',
      'nombres',
      'apellidos',
      'correo',
      'telefono',
      'tipo_doc',
      'acciones',
      'total_pagar:currency',
      'pagado:boolean',
      
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => "{view} {update} {delete} {confirm}",
        'buttonOptions' => ['class' => 'btn btn-default btn-xs'],
        'buttons' => [
            'confirm' => function($url){
              return Html::a('<i class="glyphicon glyphicon-check"></i>',
                $url, ['class' => 'btn btn-default btn-xs', 'data-confirm' => 'Seguro desea confirmar el pago ?']);
            }
        ]
      ],
    ],
  ]); ?>
  <?php Pjax::end(); ?>
</div>
