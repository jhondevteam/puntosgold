<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */

$this->title = 'Agregar Ayuda';
$this->params['breadcrumbs'][] = ['label' => 'Ayuda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ayuda-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
