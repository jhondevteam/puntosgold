<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="ayuda-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->errorSummary($model); ?>

  <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'placeholder' => 'Titulo']) ?>

  <?= $form->field($model, 'url_video')->textInput(['maxlength' => true, 'placeholder' => 'Url Video']) ?>

  <?= $form->field($model, 'descripcion')
    ->textarea(['maxlength' => true, 'placeholder' => 'Descripcion'])
  ?>

  <?= $form->field($model, 'destino')
    ->dropDownList(\app\models\Ayuda::getArrayConst(),
      ['prompt' => 'Seleccione']) ?>

  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar ' : 'Actualizar',
      ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
