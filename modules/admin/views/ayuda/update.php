<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */

$this->title = 'Actualizar Ayuda: ' . ' ' . $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Ayuda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ayuda-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
