<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */

$this->title = "Ayuda N° ".$model->id." / ".$model->destino;
$this->params['breadcrumbs'][] = ['label' => 'Ayuda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ayuda-view">

  <div class="clearfix">
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Seguro desea borrar esto ?',
        'method' => 'post',
      ],
    ])
    ?>
  </div>

  <div class="clearfix">
    <?php
    $gridColumn = [
      'titulo',
      'url_video:url',
      'descripcion',
      'destino',
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
    <?php if ($model->url_video): ?>
      <div class="text-center">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $model->url_video ?>"
                frameborder="0" allowfullscreen></iframe>
      </div>
    <?php endif; ?>
  </div>
</div>