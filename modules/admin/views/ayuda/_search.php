<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AyudaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-ayuda-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig'=>[
        'options'=>['class'=>'col-lg-4']
    ]
  ]); ?>

  <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'placeholder' => 'Titulo']) ?>

  <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true, 'placeholder' => 'Descripcion']) ?>

  <?= $form->field($model, 'destino')
    ->dropDownList(\app\models\Ayuda::getArrayConst()) ?>

  <div class="form-group col-lg-12">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<div class="clearfix"></div>