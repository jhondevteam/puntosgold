<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AyudaSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Ayuda';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="ayuda-index">
  <p>
    <?= Html::a('Insertar', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Buscar', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:none">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    'titulo',
    'url_video',
    'descripcion',
    'destino',
    [
      'class' => 'yii\grid\ActionColumn',
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-ayuda']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
    // your toolbar can include the additional full export menu
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Full',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Export All Data</li>',
          ],
        ],
        'exportConfig' => [
          ExportMenu::FORMAT_PDF => false
        ]
      ]),
    ],
  ]); ?>

</div>
