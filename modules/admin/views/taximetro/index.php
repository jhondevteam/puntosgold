<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaximetroSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Taximetros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taximetro-index">

  <h1><?= Html::encode($this->title) ?></h1>
  <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

  <p>
    <?= Html::a('Agregar configuracion Taximetro', ['create'], ['class' => 'btn btn-success']) ?>
  </p>
  
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn'],
      
      'id',
      'ciudadNames',
      'con_unidades:boolean',
      'arranque',
      'minima',
      //'distancia_mts',
      //'distancia_precio',
      //'seguntos',
      //'segundos_precio',
      //'recargo_aeropuerto',
      //'recargo_terminal',
      //'recargo_nocturno',
      //'recargo_puerta_puerta',
      //'precio_unidad',
      
      ['class' => 'yii\grid\ActionColumn'],
    ],
  ]); ?>
</div>
