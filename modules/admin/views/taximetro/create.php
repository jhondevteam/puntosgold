<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Taximetro */

$this->title = 'Create Taximetro';
$this->params['breadcrumbs'][] = ['label' => 'Taximetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taximetro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
