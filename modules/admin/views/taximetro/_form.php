<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Taximetro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taximetro-form">
  
  <?php $form = ActiveForm::begin(); ?>
  
  <?= $form->field($model, 'ciudades_ids')
    ->widget(\kartik\select2\Select2::class, [
      'data' => \yii\helpers\ArrayHelper::map(\app\models\Ciudad::find()
        ->asArray()->all(), 'id', 'nombre'),
      'options' => [
        'placeholder' => 'Seleccione las ciudades',
        'multiple' => true
      ]
    ]) ?>
  
  <?= $form->field($model, 'con_unidades')->checkbox() ?>
  
  <?= $form->field($model, 'arranque')->textInput() ?>
  
  <?= $form->field($model, 'minima')->textInput() ?>
  
  <?= $form->field($model, 'distancia_mts')->textInput() ?>
  
  <?= $form->field($model, 'distancia_precio')->textInput() ?>
  
  <?= $form->field($model, 'segundos')->textInput() ?>
  
  <?= $form->field($model, 'segundos_precio')->textInput() ?>
  
  <?= $form->field($model, 'recargo_aeropuerto')->textInput() ?>
  
  <?= $form->field($model, 'recargo_terminal')->textInput() ?>
  
  <?= $form->field($model, 'recargo_nocturno')->textInput() ?>
  
  <?= $form->field($model, 'recargo_puerta_puerta')->textInput() ?>
  
  <?= $form->field($model, 'precio_unidad')->textInput() ?>

  <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>
  
  <?php ActiveForm::end(); ?>

</div>
