<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaximetroSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="taximetro-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ciudades_ids') ?>

    <?= $form->field($model, 'con_unidades') ?>

    <?= $form->field($model, 'arranque') ?>

    <?= $form->field($model, 'minima') ?>

    <?php // echo $form->field($model, 'distancia_mts') ?>

    <?php // echo $form->field($model, 'distancia_precio') ?>

    <?php // echo $form->field($model, 'seguntos') ?>

    <?php // echo $form->field($model, 'segundos_precio') ?>

    <?php // echo $form->field($model, 'recargo_aeropuerto') ?>

    <?php // echo $form->field($model, 'recargo_terminal') ?>

    <?php // echo $form->field($model, 'recargo_nocturno') ?>

    <?php // echo $form->field($model, 'recargo_puerta_puerta') ?>

    <?php // echo $form->field($model, 'precio_unidad') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
