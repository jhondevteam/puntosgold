<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Taximetro */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Taximetros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="taximetro-view">

  <h1><?= Html::encode($this->title) ?></h1>

  <p>
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
      'class' => 'btn btn-danger',
      'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
      ],
    ]) ?>
  </p>
  
  <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
      'id',
      'ciudadNames',
      'con_unidades:boolean',
      'arranque',
      'minima',
      'distancia_mts',
      'distancia_precio',
      'segundos',
      'segundos_precio',
      'recargo_aeropuerto',
      'recargo_terminal',
      'recargo_nocturno',
      'recargo_puerta_puerta',
      'precio_unidad',
    ],
  ]) ?>

</div>
