<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$dataProvider = new ArrayDataProvider([
  'allModels' => $model->empresas,
  'key' => 'id'
]);
$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  [
    'attribute' => 'categoria.nombre',
    'label' => 'Categoria'
  ],
  'nombre',
  'nit',
  'estado:boolean',
  'porcentaje:percent',
  'porcentaje_afiliado:percent',
  [
    'class' => 'yii\grid\ActionColumn',
    'controller' => 'empresa',
    'template' => '{view}'
  ],
];

echo GridView::widget([
  'dataProvider' => $dataProvider,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
