<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */

$this->title = $model->usuario->nombres . " - " . $model->zona->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Administradores de Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-zona-view">
  <p>
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    <?php if (Yii::$app->authManager->checkAccess($model->usuario_id,\app\helper\AppHelper::ADMIN_ZONA)): ?>
    <?= Html::a('Revocar permiso', ['acceso', 'id' => $model->id,'control'=>'revocar'], ['class' => 'btn btn-primary']) ?>
    <?php else: ?>
      <?= Html::a('Reasignar permiso', ['acceso', 'id' => $model->id,'control'=>'assignar'], ['class' => 'btn btn-primary']) ?>
    <?php endif; ?>
  </p>
  <div class="clearfix">
    <?php
    $gridColumn = [
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Usuario',
      ],
      [
        'attribute' => 'usuario.documento',
        'label' => 'Documento',
      ],
      'telefonos',
      'direccion',
      [
        'attribute' => 'PorcentajeFormat',
        'label' => 'Porcentaje',
        'format'=>'percent'
      ],
      [
        'attribute' => 'zona.nombre',
        'label' => 'Zona',
      ],
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>

  <div class="clearfix">
    <?php
    if ($providerEmpresa->totalCount) {
      $gridColumnEmpresa = [
        ['class' => 'yii\grid\SerialColumn'],
        [
          'attribute' => 'categoria.nombre',
          'label' => 'Categoria'
        ],
        'nombre',
        'nit',
        'estado:boolean',
      ];
      echo Gridview::widget([
        'dataProvider' => $providerEmpresa,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empresa']],
        'panel' => [
          'type' => GridView::TYPE_PRIMARY,
          'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Empresa'),
        ],
        'columns' => $gridColumnEmpresa
      ]);
    }
    ?>
  </div>
</div>
</div>