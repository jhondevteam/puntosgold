<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */
/* @var $modelUser app\models\Usuario */

$this->title = 'Actualizar Administrador de la zona: ' . $model->zona->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Administradores de Zona', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="admin-zona-update">
    <?= $this->render('_form', [
        'model' => $model,
      'modelUser'=>$modelUser
    ]) ?>
</div>
