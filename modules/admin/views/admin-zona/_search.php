<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\AdminZonaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-admin-zona-search clearfix">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => ['options' => ['class' => 'col-md-3']]
  ]); ?>

  <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('nombres')
      ->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione Usuario'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'telefonos')->textInput(['placeholder' => 'Telefonos']) ?>

  <?= $form->field($model, 'direccion')->textInput(['placeholder' => 'Direccion']) ?>

  <?= $form->field($model, 'zona_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Zona::find()
      ->orderBy('nombre')->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione Zona'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <div class="form-group">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
