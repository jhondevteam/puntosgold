<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Admin Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-zona-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Admin Zona'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
                'attribute' => 'usuario.id',
                'label' => 'Usuario'
            ],
        'telefonos',
        'direccion',
        [
                'attribute' => 'zona.nombre',
                'label' => 'Zona'
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerEmpresa->totalCount){
    $gridColumnEmpresa = [
        ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'categoria.nombre',
                'label' => 'Categoria'
            ],
        'nombre',
        'descripcion',
        'nit',
        'estado',
        'porcentaje',
        'porcentaje_afiliado',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEmpresa,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Empresa'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnEmpresa
    ]);
}
?>
    </div>
</div>
