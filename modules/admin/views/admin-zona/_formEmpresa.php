<div class="form-group" id="add-empresa">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Empresa',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'categoria_id' => [
            'label' => 'Categoria empresa',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\CategoriaEmpresa::find()->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
                'options' => ['placeholder' => 'Choose Categoria empresa'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'nombre' => ['type' => TabularForm::INPUT_TEXT],
        'descripcion' => ['type' => TabularForm::INPUT_TEXT],
        'nit' => ['type' => TabularForm::INPUT_TEXT],
        'estado' => ['type' => TabularForm::INPUT_CHECKBOX,
            'options' => [
                'style' => 'position : relative; margin-top : -9px'
            ]
        ],
        'porcentaje' => ['type' => TabularForm::INPUT_TEXT],
        'porcentaje_afiliado' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowEmpresa(' . $key . '); return false;', 'id' => 'empresa-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Empresa', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowEmpresa()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

