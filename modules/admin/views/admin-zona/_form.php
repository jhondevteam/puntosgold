<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */
/* @var $modelUser app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admin-zona-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($modelUser, 'nombres') ?>
  <?= $form->field($modelUser, 'documento') ?>
  <?= $form->field($modelUser, 'email') ?>
  <?= $form->field($model, 'telefonos')->textInput(['placeholder' => 'Telefonos']) ?>
  <?= $form->field($model, 'direccion')->textInput(['placeholder' => 'Direccion']) ?>
  <?= $form->field($model, 'porcentaje')
    ->dropDownList(\app\helper\AppHelper::getPorcentajes())
  ?>
  <?= $form->field($model, 'zona_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Zona::find()->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Seleccione la zona'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
