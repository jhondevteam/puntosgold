<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */
/* @var $modelUser app\models\Usuario */

$this->title = 'Agregar Administrador de Zona';
$this->params['breadcrumbs'][] = ['label' => 'Administradores de Zona', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-zona-create">
  <?= $this->render('_form', [
    'model' => $model,
    'modelUser' => $modelUser
  ]) ?>
</div>
