<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AdminZona */

?>
<div class="admin-zona-view">
  <div class="row">
    <?php
    $gridColumn = [
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
      [
        'label'=>'Correo',
        'attribute'=>'usuario.email'
      ],
      'telefonos',
      'direccion',
      [
        'attribute' => 'zona.ciudad.departamento.nombre',
        'label' => 'Departamento',
      ],
      [
        'attribute' => 'zona.ciudad.nombre',
        'label' => 'ciudad',
      ],
      [
        'attribute' => 'zona.nombre',
        'label' => 'Zona',
      ],
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>