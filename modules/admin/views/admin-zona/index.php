<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AdminZonaSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Administradores de Zona';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="admin-zona-index">
  <p>
    <?= Html::a('Agregar Administrador zona', ['create'], ['class' => 'btn btn-success']) ?>
    <?= Html::a('Busqueda', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:none">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    [
      'class' => 'kartik\grid\ExpandRowColumn',
      'width' => '50px',
      'value' => function ($model, $key, $index, $column) {
        return GridView::ROW_COLLAPSED;
      },
      'detail' => function ($model, $key, $index, $column) {
        return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
      },
      'headerOptions' => ['class' => 'kartik-sheet-style'],
      'expandOneOnly' => true
    ],
    [
      'attribute' => 'usuario_id',
      'label' => 'Usuario',
      'value' => function (\app\models\AdminZona $model) {
        return $model->usuario->nombres;
      },
    ],
    'telefonos',
    'direccion',
    [
      'attribute' => 'zona_id',
      'label' => 'Ciudad',
      'value' => function (\app\models\AdminZona $model) {
        return $model->zona->ciudad->nombre;
      },
    ],
    [
      'attribute' => 'zona_id',
      'label' => 'Zona',
      'value' => function ($model) {
        return $model->zona->nombre;
      },
    ],
    [
      'attribute' => 'PorcentajeFormat',
      'label' => 'Porcentaje',
      'format'=>'percent'
    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'template' => '{view} {update}',
      'buttonOptions' => [
        'class' => 'btn btn-primary btn-xs'
      ]
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-admin-zona']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
  ]); ?>

</div>
