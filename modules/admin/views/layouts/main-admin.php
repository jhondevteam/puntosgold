<?php

/* @var $this \yii\web\View */
/* @var $content string */

/* @var $this \yii\web\View */

use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="clearfix">
  <div class="col-lg-3 col-sm-3">
    <div class="profile-sidebar">
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="<?= \yii\helpers\Url::to("@web/" . Yii::$app->user->identity->foto) ?>"
             class="img-responsive" alt="">
      </div>
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">
          <?= Yii::$app->user->identity->nombres ?>
        </div>
        <div class="profile-usertitle-job">
          Administrador
        </div>
      </div>
      <div class="profile-userbuttons">
        <?= Html::a('Salir', ['/site/logout'], ['data-method' => 'post', 'class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('Editar', ['/admin/cuenta'], ['class' => 'btn btn-danger btn-sm']) ?>
      </div>
      <div class="profile-usermenu">
        <?php
        echo Nav::widget([
          'options' => ['class' => 'nav'],
          'encodeLabels' => false,
          'items' =>
            Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN) ?
              [
                ['label' => 'Empresas', 'url' => ['/admin/empresa']],
                ['label' => 'Categorias', 'url' => ['/admin/categoria-empresa']],
                ['label' => 'Afiliados', 'url' => ['/admin/afiliado']],
                ['label' => 'Administradores', 'url' => ['/admin/admin-zona']],
                ['label' => 'Zonas', 'url' => ['/admin/zona']],
                ['label' => 'Preguntas respuestas', 'url' => ['/admin/preguntas-respuestas']],
                ['label' => 'Testimonios', 'url' => ['/admin/testimonios']],
                ['label' => 'Soporte (chat)', 'url' => ['/admin/soporte']],
                ['label' => 'Video ayuda', 'url' => ['/admin/ayuda']],
                ['label' => 'Paginas', 'url' => ['/admin/paginas']],
                ['label' => 'Ciudades', 'url' => ['/admin/ciudad']],
                ['label'=>'<hr>'],
                ['label' => 'Empresas transportadoras', 'url' => ['/admin/empresa-transportadora']],
                ['label' => 'Conductores', 'url' => ['/admin/taxi-app/list']],
                ['label' => 'Servicios de conductores', 'url' => ['/admin/taxi-app/categories']],
                ['label' => 'Servicios por departamento', 'url' => ['/admin/taxi-app/state-categories']],
                ['label' => 'Taximetro', 'url' => ['/admin/taximetro']],
                ['label' => 'Accionistas', 'url' => ['/admin/accionistas']],
              ] :
              [
                ['label' => 'Empresas','url'=>['/admin/empresa']],
                ['label' => 'Mi cuenta','url'=>['/admin/cuenta']]
              ],
        ]);
        ?>
      </div>
    </div>
  </div>
  <div class="col-lg-9 col-sm-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2 class="panel-title"><?= $this->title ?></h2>
      </div>
      <div class="panel-body">
        <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?php if (($msm = Yii::$app->session->getAllFlashes()) !== null): ?>
          <?php foreach ($msm as $type => $menssage): ?>
            <div class="alert alert-<?php echo $type ?> fade in">
              <button data-dismiss="alert" class="close" type="button">
                <i class="glyphicon glyphicon-remove"></i>
              </button><?php echo $menssage ?>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
        <?= $content ?>
      </div>
    </div>
    <div>
    </div>
  </div>
</div>

<footer class="footer">
  <div class="container">
    <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>

    <p class="pull-right"><?= Yii::powered() ?></p>
  </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
