<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 02:08 AM
 * @var \yii\web\View $this
 * @var $city \app\models\Ciudad
 * @var $data \yii\data\ActiveDataProvider
 */
$this->title = 'Ciudades';
?>
<div class="col-lg-6">
  <?= $this->render('_form', ['model' => $model]) ?>
</div>
<div class="col-lg-6">
  <div class="col-lg-12 row">
    <?php $form = \kartik\form\ActiveForm::begin([
      'action' => ['index'],
      'method' => 'get'
    ]) ?>
    <?= $form->field($city,'nombre') ?>
    <?= $form->field($city,'departamento_id')
      ->dropDownList(\yii\helpers\ArrayHelper::map(
        \app\models\Departamento::find()->orderBy('nombre')->all(),'id','nombre'
      ),['prompt'=>'Seleccione'])?>
    <?= \yii\helpers\Html::submitButton('Buscar',['class'=>'btn btn-info']) ?>
    <?php \kartik\form\ActiveForm::end() ?>
  </div>
  <?= \kartik\grid\GridView::widget([
    'dataProvider' => $data,
    'columns' => [
      'nombre','departamento.nombre',
      [
        'header'=>'Actualziar',
        'value'=>function(\app\models\Ciudad $model){
          return \yii\helpers\Html::a('Editar',['index','id'=>$model->id],['class'=>'btn btn-default btn-xs']);
        },
        'format'=>'raw'
      ]
    ]
  ]) ?>
</div>
