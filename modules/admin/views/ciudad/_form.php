<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 02:09 AM
 * @var $model \app\models\Ciudad
 * @var $this \yii\web\View
 */
$id = Yii::$app->request->get('id',false);
$action = $id? ['save','id'=>$id] : ['save']
?>
<?php $form = \kartik\form\ActiveForm::begin([
  'action' => $action,
  'enableAjaxValidation' => true
]) ?>
<?= $form->field($model,'nombre') ?>
<?= $form->field($model,'departamento_id')
->dropDownList(\yii\helpers\ArrayHelper::map(
  \app\models\Departamento::find()->orderBy('nombre')->all(),'id','nombre'
),['prompt'=>'Seleccione'])?>
<?= $form->field($model,'principal')->checkbox() ?>
<?= \yii\helpers\Html::submitButton($model->isNewRecord? 'Guardar' : 'Actualizar',['class'=>'btn btn-primary']) ?>
<?php \kartik\form\ActiveForm::end() ?>
