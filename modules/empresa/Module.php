<?php

namespace app\modules\empresa;

/**
 * empresa module definition class
 */
class Module extends \yii\base\Module
{
  public $layout = 'main-admin';
  /**
   * @inheritdoc
   */
  public $controllerNamespace = 'app\modules\empresa\controllers';

  /**
   * @inheritdoc
   */
  public function init()
  {
    parent::init();

    // custom initialization code goes here
  }
}
