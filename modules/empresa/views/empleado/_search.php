<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\EmpleadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-empleado-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <?= $form->field($model, 'empresa_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Empresa::find()
      ->orderBy('id')->asArray()->all(), 'id', 'nombre'),
    'options' => ['placeholder' => 'Choose Empresa'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model, 'usuario_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Usuario::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
    'options' => ['placeholder' => 'Choose Usuario'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <div class="form-group">
    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
