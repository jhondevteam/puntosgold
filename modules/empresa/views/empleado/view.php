<?php

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\tabs\TabsX;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $searchModel \app\models\search\CompraAfiliadoSearch */
/* @var $providerCompraAfiliado \yii\data\ActiveDataProvider */
/* @var $providerCompraAfiliadoPuntos \yii\data\ActiveDataProvider */

$this->title = $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-view">

  <div class="form-group">
    <?php
    if (!Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::ADMIN_EMPRESA)) {
      $active = Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::EMPLEADO);
      echo Html::a("<i class='glyphicon glyphicon-" . ($active ? 'check' : 'remove') . "'></i> " . ($active ? 'Activo' : 'Incativo'),
        ['activate', 'id' => $model->id, 'status' => $active],
        ['class' => 'btn btn-' . ($active ? 'success' : 'danger'), 'title' => $active ?
          'El empleado tiene acceso. pulse para desactivar el acceso' : 'El empleado no tiene acceso. pulse para activar el acceso']);
    }
    ?>
    <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
  </div>

  <div>
    <?php
    $gridColumn = [
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Usuario',
      ],
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
  <div class="form-group">
    <?= $this->render('_search_ventas', [
      'model' => $searchModel,
      'modelParent' => $model
    ]) ?>
  </div>
  <div>
    <?php
    $gridColumn = [
      ['class' => 'yii\grid\SerialColumn'],
      'afiliado.usuario.nombres',
      'fecha:datetime',
      'monto:currency',
      [
        'attribute' => 'monto_cobra_empresa',
        'label' => 'Cuota publicitaria',
        'format' => 'currency'
      ],
      [
        'attribute' => 'comentario',
        'label' => 'Factura',
      ],
      'empleado.usuario.nombres',
      'pago_puntos:boolean'
    ];
    ?>
    <div class="col-lg-12">
      <div class="col-lg-4">
        <div class="alert alert-info">
          <h4>Total vendido </h4>
          <?= Yii::$app->formatter->asCurrency($searchModel->getTotalVendido()) ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="alert alert-info">
          <h4>Total comision publicitaria </h4>
          <?= Yii::$app->formatter->asCurrency($searchModel->getTotalRecaudar()) ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="alert alert-info">
          <h4>Total comision publicitaria </h4>
          <?= Yii::$app->formatter->asCurrency($searchModel->getTotalPuntos()) ?>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
    <?= GridView::widget([
      'dataProvider' => $dataProvider,
      'columns' => $gridColumn,
      'pjax' => true,
      'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-compra-afiliado']],
      'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . \yii\helpers\Html::encode($this->title),
      ],
      'export' => false,
      'toolbar' => [
        '{export}',
        ExportMenu::widget([
          'dataProvider' => $dataProvider,
          'columns' => $gridColumn,
          'target' => ExportMenu::TARGET_BLANK,
          'fontAwesome' => true,
          'dropdownOptions' => [
            'label' => 'Full',
            'class' => 'btn btn-default',
            'itemsBefore' => [
              '<li class="dropdown-header">Exportar toda la información</li>',
            ],
          ],
          'exportConfig' => [
            ExportMenu::FORMAT_PDF => false
          ]
        ]),
      ],
    ]);
    ?>
  </div>
</div>