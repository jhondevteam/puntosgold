<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */

?>
<div class="empleado-view">
  <div class="row">
    <?php
    $gridColumn = [
      [
        'attribute' => 'usuario.nombres',
        'label' => 'Nombres',
      ],
      [
        'attribute' => 'usuario.documento',
        'label' => 'Documento',
      ],
      [
        'attribute' => 'usuario.email',
        'label' => 'Email',
      ],
      [
        'attribute' => 'usuario.role',
        'label' => 'Rol'
      ]
    ];
    echo DetailView::widget([
      'model' => $model,
      'attributes' => $gridColumn
    ]);
    ?>
  </div>
</div>