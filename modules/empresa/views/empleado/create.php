<?php


/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $user app\models\Usuario */

$this->title = 'Agregar Empleado/vendedor';
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empleado-create">
  <?= $this->render('_form', [
    'model' => $model,
    'user' => $user
  ]) ?>
</div>
