<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CompraAfiliadoSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $modelParent \app\models\Empleado */
?>

<div class="form-compra-afiliado-search">
  <h2 class="text-primary">Buscar </h2>
  <?php $form = ActiveForm::begin([
    'action' => ['view', 'id' => $modelParent->id],
    'method' => 'get',
    'fieldConfig' => [
      'options' => ['class' => 'col-lg-6']
    ]
  ]); ?>
  <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::className(), [
    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>
  <?= $form->field($model, 'fecha_hasta')->widget(\kartik\date\DatePicker::className(), [
    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>
  <?= $form->field($model, 'recaudo')->dropDownList([
    '' => 'Todo',
    '0' => 'Por recaudar',
    '1' => 'Recaudado'
  ]) ?>

  <?= $form->field($model, 'pagado')->dropDownList([
    '' => 'Todo',
    '0' => 'Por pagar',
    '1' => 'Pagado'
  ]) ?>

  <div class="form-group">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
