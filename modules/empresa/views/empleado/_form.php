<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $user app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empleado-form">
  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($user, 'nombres') ?>
  <?= $form->field($user, 'documento') ?>
  <?= $form->field($user, 'email') ?>

  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?= Html::a('Cancelar', Yii::$app->request->referrer, ['class' => 'btn btn-danger']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
