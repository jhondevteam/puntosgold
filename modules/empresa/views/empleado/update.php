<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Empleado */
/* @var $user app\models\Usuario */

$this->title = 'Actualizar Empleado: ' . ' ' . $model->usuario->nombres;
$this->params['breadcrumbs'][] = ['label' => 'Empleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario->nombres, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="empleado-update">
  <?= $this->render('_form', [
    'model' => $model,
    'user' => $user
  ]) ?>
</div>
