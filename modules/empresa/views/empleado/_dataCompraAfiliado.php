<?php
/**
 * @var $model \app\models\Empleado
 * @var $data \yii\data\ActiveDataProvider
 */
use kartik\grid\GridView;

$gridColumns = [
  ['class' => 'yii\grid\SerialColumn'],
  'fecha:datetime',
  'monto:currency',
  'monto_cobra_empresa:currency',
];

echo GridView::widget([
  'dataProvider' => $data,
  'columns' => $gridColumns,
  'containerOptions' => ['style' => 'overflow: auto'],
  'pjax' => true,
  'beforeHeader' => [
    [
      'options' => ['class' => 'skip-export']
    ]
  ],
  'export' => [
    'fontAwesome' => true
  ],
  'bordered' => true,
  'striped' => true,
  'condensed' => true,
  'responsive' => true,
  'hover' => true,
  'showPageSummary' => false,
  'persistResize' => false,
]);
