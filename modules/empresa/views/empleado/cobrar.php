<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 26/09/17
 * Time: 10:53 PM
 * @var $this \yii\web\View
 * @var $data \yii\data\ActiveDataProvider
 * @var $model \app\models\CobroEmpresaEmpleado
 * @var $empleado \app\models\Empleado
 * @var $lastCobro \app\models\CobroEmpresaEmpleado
 */

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = "Registrar cobro de ".$empleado->usuario->nombres;
?>
<div class="col-lg-6">
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    'fecha:datetime',
    'monto:currency',
    'monto_cobra_empresa:currency',
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $data,
    'columns' => $gridColumn,
    'pjax' => true,
    'export' => false,
  ]); ?>
</div>
<div class="col-lg-6">
  <?php if($lastCobro !== null): ?>
    <div class="alert alert-info">
      <h2>Ultimo cobro registrado</h2>
      <p>Fecha : <?= Yii::$app->formatter->asDatetime($lastCobro->fecha) ?></p>
      <p>Monto del cobro : <?= Yii::$app->formatter->asCurrency($lastCobro->monto) ?></p>
      <p> : <?= Yii::$app->formatter->asCurrency($lastCobro->saldo) ?></p>
    </div>
  <?php endif; ?>
  <div class="alert alert-info">
    <h2>Saldo pendiente de recudar:</h2>
    <h3><?= Yii::$app->formatter->asCurrency($model->monto_cobrar) ?></h3>
  </div>
  <?php $form = \yii\widgets\ActiveForm::begin([
    'enableAjaxValidation'=>true
  ]) ?>
  <h3 class="text-primary">Registrar cobro</h3>
  <?= $form->field($model,'monto')->widget(\yii\widgets\MaskedInput::className(),[
    'clientOptions' => [
      'alias' =>  'decimal',
      'groupSeparator' => '.',
      'autoGroup' => true,
      'removeMaskOnSubmit'=>true
    ],
  ]) ?>
  <?= Html::submitButton('Guardar',['class'=>'btn btn-primary']) ?>
  <?php \yii\widgets\ActiveForm::end() ?>
</div>
