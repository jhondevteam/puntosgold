<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\EmpleadoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Empleados';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="empleado-index">
  <p>
    <?= Html::a('Agregar Empleado', ['create'], ['class' => 'btn btn-success']) ?>
  </p>
  <div class="search-form" style="display:none">
    <?= $this->render('_search', ['model' => $searchModel,'empresa'=>$empresa]); ?>
  </div>
  <?php
  $gridColumn = [[
    'class' => 'kartik\grid\ExpandRowColumn',
    'width' => '50px',
    'value' => function ($model, $key, $index, $column) {
      return GridView::ROW_COLLAPSED;
    },
    'detail' => function ($model, $key, $index, $column) {
      return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
    },
    'headerOptions' => ['class' => 'kartik-sheet-style'],
    'expandOneOnly' => true
  ],
    [
      'attribute' => 'usuario_id',
      'label' => 'Usuario',
      'value' => function ($model) {
        return $model->usuario->nombres;
      },
    ],
    [
      'label'=>'Saldo de cobro anterior',
      'attribute'=>'saldoCobrar',
      'format'=>'currency'
    ],
    [
      'label' => 'Monto a cobrar por la empresa.',
      'attribute' => 'montoCobrar',
      'format' => 'currency'
    ],
    ['class' => 'yii\grid\ActionColumn',
      'template' => "{view} {edit} {active} {cobrar}",
      'buttons' => ['active' => function ($key, \app\models\Empleado $model) {
        if (!Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::ADMIN_EMPRESA)) {
          $active = Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::EMPLEADO);
          return Html::a("<i class='glyphicon glyphicon-" . ($active ? 'check' : 'remove') . "'></i> " .( $active ? 'Activo' : 'Incativo'),
            ['activate', 'id' => $model->id, 'status' => $active],
            ['class' => 'btn btn-sm btn-' . ($active ? 'success' : 'danger'), 'title' => $active ?
              'El empleado tiene acceso. pulse para desactivar el acceso' : 'El empleado no tiene acceso. pulse para activar el acceso']);
        }
        return false;
      },
        'cobrar' => function ($key, \app\models\Empleado $model) {
          if (!Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::ADMIN_EMPRESA)) {
            if ($model->getMontoCobrar() > 0 or $model->getSaldoCobrar()) {
              return Html::a('<i class="glyphicon glyphicon-usd"></i> Registrar cobro', ['cobrar', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'title' => 'Registre la entrega de dinero del empleado']);
            }
          }
        }],
      'buttonOptions' => ['class' => 'btn btn-default btn-sm']
    ],
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-empleado']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
    ],
    'export' => false,
  ]); ?>

</div>
