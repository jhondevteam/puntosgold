<?php
/**
 * @var $model \app\models\Empleado
 */

use kartik\tabs\TabsX;
use yii\helpers\Html;

$items = [
  [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Detalles'),
    'content' => $this->render('_detail', [
      'model' => $model,
    ]),
  ],
];
if (Yii::$app->authManager->checkAccess($model->usuario_id, \app\helper\AppHelper::EMPLEADO)) {
  $items [] = [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Compras registradas'),
    'content' => $this->render('_dataCompraAfiliado', [
      'model' => $model,
      'data' => new \yii\data\ActiveDataProvider([
        'query' => $model->getCompraAfiliados()
          ->where(['is', 'pago_empresa_id', null])
      ]),
    ]),
  ];
  $items [] = [
    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Compras registradas'),
    'content' => $this->render('_dataCompraAfiliado', [
      'model' => $model,
      'data' => new \yii\data\ActiveDataProvider([
        'query' => $model->getCompraAfiliados()
          ->where(['is', 'pago_empresa_id', null])
      ]),
    ]),
  ];
}
?>
<div class="panel panel-default">
  <div class="panel-body">
    <?= TabsX::widget([
      'items' => $items,
      'position' => TabsX::POS_ABOVE,
      'encodeLabels' => false,
      'class' => 'tes',
      'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
      ],
    ]); ?>
  </div>
</div>