<?php

/* @var $this \yii\web\View */
/* @var $content string */
/* @var $this \yii\web\View */

use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="clearfix">
  <div class="col-lg-3">
    <div class="profile-sidebar">
      <!-- SIDEBAR USERPIC -->
      <div class="profile-userpic">
        <img src="<?= \yii\helpers\Url::to("@web/".Yii::$app->user->identity->foto) ?>"
             class="img-responsive" alt="">
      </div>
      <div class="profile-usertitle">
        <div class="profile-usertitle-name">
          <?= Yii::$app->user->identity->nombres ?>
        </div>
        <div class="profile-usertitle-job">
          Administrador
        </div>
      </div>
      <div class="profile-userbuttons">
        <?= Html::a('Salir',['/site/logout'],['class'=>'btn btn-danger','data-method'=>'post']) ?>
        <button type="button" class="btn btn-danger btn-sm">editar</button>
      </div>
      <div class="profile-usermenu">
        <?php
        echo Nav::widget([
          'options' => ['class' => 'nav'],
          'items' => [
            ['label' => 'Inicio', 'url' => ['/empresa/dashboard']],
            ['label' => 'Datos empresa', 'url' => ['/empresa/dashboard/perfil']],
            ['label' => 'Empleados', 'url' => ['/empresa/empleado']],
            ['label' => 'Ventas', 'url' => ['/empresa/ventas']],
            ['label' => 'Seguridad', 'url' => ['/empresa/dashboard/seguridad']],
            ['label' => 'Video ayuda', 'url' => ['/empresa/dashboard/tutoriales']],
          ],
        ]);
        ?>
      </div>
    </div>
  </div>
  <div class="col-lg-9">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2 class="panel-title"><?= $this->title ?> </h2>
      </div>
      <div class="panel-body">
        <?= Breadcrumbs::widget([
          'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \kartik\alert\AlertBlock::widget() ?>
        <?= $content ?>
      </div>
    </div>
    <div>
    </div>
  </div>
</div>

<footer class="footer">
  <div class="container">
    <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>

    <p class="pull-right"><?= Yii::powered() ?></p>
  </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
