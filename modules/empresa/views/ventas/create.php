<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CompraAfiliado */

$this->title = 'Create Compra Afiliado';
$this->params['breadcrumbs'][] = ['label' => 'Compra Afiliados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-afiliado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
