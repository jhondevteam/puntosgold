<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompraAfiliado */

$this->title = 'Update Compra Afiliado: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Compra Afiliados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compra-afiliado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
