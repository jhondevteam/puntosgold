<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CompraAfiliadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-compra-afiliado-search">

  <?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'fieldConfig' => [
      'options' => ['class' => 'col-lg-4']
    ]
  ]); ?>

  <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::className(), [
    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>

  <?= $form->field($model, 'fecha_hasta')->widget(\kartik\date\DatePicker::className(), [

    'pluginOptions' => [
      'placeholder' => 'Seleccione Fecha',
      'format' => 'yyyy-mm-dd',
      'todayHighlight' => true,
      'autoclose' => true,
    ]
  ]); ?>

  <?= $form->field($model, 'empleado_id')->widget(\kartik\widgets\Select2::classname(), [
    'data' => \yii\helpers\ArrayHelper::map(\app\models\Empleado::find()
      ->where(['empresa_id' => Yii::$app->controller->empresa->id])
      ->joinWith('usuario')
      ->orderBy('id')->all(), 'id', 'usuario.nombres'),
    'options' => ['placeholder' => 'Seleccione Empleado'],
    'pluginOptions' => [
      'allowClear' => true
    ],
  ]); ?>

  <?= $form->field($model,'comentario')->label('N° factura') ?>

  <?= $form->field($model,'tipo_compra')->label('Tipo de compra')
  ->dropDownList([''=>'Todo','1'=>'Efectivo','2'=>'Puntos'])?>

  <?= $form->field($model, 'afiliado_id', [
    'template' => "{label}\n{input}\n{auto}\n{hint}\n{error}",
    'parts' => [
      '{auto}' =>
        \yii\jui\AutoComplete::widget([
          'name' => 'cliente',
          'clientOptions' => [
            'source' => new \yii\web\JsExpression('
                                        function(request,response){
                                            var val = $("input[name=\\"cliente\\"]");
                                                $.getJSON("' . \yii\helpers\Url::to(['ventas/findcliente']) . '",{query:val.val()},function(data){
                                                response(data);
                                            });
                                        }'),
            'select' =>
              new \yii\web\JsExpression("function(event,ui){
                                            var client_info = $('#client_info');
                                            var input = $('#".Html::getInputId($model,'afiliado_id')."');
                                            input.val(ui.item.id);
                                            return false;
                                        }")
          ],
          'options' => [
            'class' => 'form-control'
          ]
        ])
    ]
  ])->hiddenInput() ?>

  <div class="form-group clearfix">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
<div class="clearfix"></div>
