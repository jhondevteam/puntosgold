<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 28/09/17
 * Time: 06:11 PM
 * @var $data \yii\data\ActiveDataProvider
 * @var $this \yii\web\View
 */

use kartik\export\ExportMenu;
use kartik\grid\GridView;

$gridColumn = [
  ['class' => 'yii\grid\SerialColumn'],
  'fecha:datetime',
  'monto:currency',
  [
    'attribute' => 'comentario',
    'label' => 'Factura',
  ],
  [
    'attribute' => 'empleado_id',
    'label' => 'Empleado',
    'value' => function (\app\models\CompraAfiliadoPuntos $model) {
      return $model->empleado->usuario->nombres;
    },
  ],
];
?>
<?= GridView::widget([
  'dataProvider' => $data,
  'columns' => $gridColumn,
  'pjax' => true,
  'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-compra-afiliado']],
  'panel' => [
    'type' => GridView::TYPE_PRIMARY,
    'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . \yii\helpers\Html::encode($this->title),
  ],
  'export' => false,
  'toolbar' => [
    '{export}',
    ExportMenu::widget([
      'dataProvider' => $data,
      'columns' => $gridColumn,
      'target' => ExportMenu::TARGET_BLANK,
      'fontAwesome' => true,
      'dropdownOptions' => [
        'label' => 'Full',
        'class' => 'btn btn-default',
        'itemsBefore' => [
          '<li class="dropdown-header">Exportar toda la información</li>',
        ],
      ],
      'exportConfig' => [
        ExportMenu::FORMAT_PDF => false
      ]
    ]),
  ],
]); ?>
