<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CompraAfiliadoSearch */
/* @var $searchModelPuntos app\models\search\CompraAfiliadoPuntosSearch */

/* @var $dataProvider yii\data\ActiveDataProvider */

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\Html;

$this->title = 'Ventas a Afiliados';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="compra-afiliado-index">
  <p>
    <?= Html::a('Busqueda avanzada', '#', ['class' => 'btn btn-info search-button']) ?>
  </p>
  <div class="search-form" style="display:none">
    <?= $this->render('_search', ['model' => $searchModel]); ?>
  </div>
  <?php
  $gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],
    'afiliado.usuario.nombres',
    'empleado.usuario.nombres',
    'fecha:datetime',
    'monto:currency',
    [
      'attribute' => 'monto_cobra_empresa',
      'label' => 'Cuota publicitaria',
      'format' => 'currency'
    ],
    'puntos:currency',
    [
      'attribute' => 'comentario',
      'label' => 'Factura',
    ],
    'pago_puntos:boolean'
  ];
  ?>
  <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumn,
    'pjax' => true,
    'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-compra-afiliado']],
    'panel' => [
      'type' => GridView::TYPE_PRIMARY,
      'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . \yii\helpers\Html::encode($this->title),
    ],
    'export' => false,
    'toolbar' => [
      '{export}',
      ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'target' => ExportMenu::TARGET_BLANK,
        'fontAwesome' => true,
        'dropdownOptions' => [
          'label' => 'Full',
          'class' => 'btn btn-default',
          'itemsBefore' => [
            '<li class="dropdown-header">Exportar toda la información</li>',
          ],
        ],
        'exportConfig' => [
          ExportMenu::FORMAT_PDF => false
        ]
      ]),
    ],
  ]); ?>
</div>
