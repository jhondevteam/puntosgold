<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CompraAfiliado */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Compra Afiliados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="compra-afiliado-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Compra Afiliado'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'fecha',
        'monto',
        'monto_cobra_empresa',
        'puntos',
        [
            'attribute' => 'afiliado.id',
            'label' => 'Afiliado',
        ],
        [
            'attribute' => 'empleado.id',
            'label' => 'Empleado',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>