<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CompraAfiliado */

?>
<div class="compra-afiliado-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'fecha',
        'monto',
        'monto_cobra_empresa',
        'puntos',
        [
            'attribute' => 'afiliado.id',
            'label' => 'Afiliado',
        ],
        [
            'attribute' => 'empleado.id',
            'label' => 'Empleado',
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>