<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 01:05 PM
 * @var $model \app\models\base\Video
 */
?>
<div class="col-sm-4 img-thumbnail">
  <p><?= $model->descripcion ?></p>
  <iframe src="https://www.youtube.com/embed/<?= $model->url_video ?>" frameborder="0" width="100%"
          height="300"
          allowfullscreen></iframe>
  <div class="btn-group-justified">
    <?= \yii\helpers\Html::a($model->activo? 'Hacer no visible' : 'Hacer visible',
      ['video-action','id'=>$model->id,'action'=>'visibilidad'],
      ['class'=>'btn btn-xs btn-'.($model->activo? 'success' : 'info')]) ?>
    <?= \yii\helpers\Html::a('Borrar',
      ['video-action','id'=>$model->id,'action'=>'borrar'],
      ['class'=>'btn btn-xs btn-danger','data-confirm'=>'Seguro desea eliminar completamente']) ?>
  </div>
</div>
