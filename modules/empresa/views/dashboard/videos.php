<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 12:24 PM
 * @var $videos \app\models\Ayuda[]
 * @var $this \yii\web\View
 */
$this->title = 'Video Ayuda';
?>
<?= \app\widgets\TPListView::widget([
  'dataProvider' => new \yii\data\ArrayDataProvider(['allModels' => $videos]),
  'itemView' => '_video',
  'numberGroup' => 3
]) ?>