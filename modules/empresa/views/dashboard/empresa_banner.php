<?php
/**
 * Created by PhpStorm.
 * User: Andres santos
 * Date: 25/01/2018
 * Time: 3:23 PM
 * @var $galeria \app\models\Foto[]
 */

use kartik\widgets\FileInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <?php $form = ActiveForm::begin([
     'action' => ['upload-banner']
 ]); ?>
 <?= $form->field($foto, 'file')->widget(FileInput::className()) ?>
  <div class="col-md-12 col-lg-12 form-group">
   <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-success']) ?>
  </div>
 <?php ActiveForm::end(); ?>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <?php foreach ($galeria as $item): ?>
   <div class="col-xs-4">
    <?= Html::img("@web/$item->url_foto", ['class' => 'img-thumbnail img-responsive']) ?>
    <?= Html::a('<i class="glyphicon glyphicon-trash"></i> borrar', ['delete-foto', 'id' => $item->id], ['class' => 'btn btn-danger btn-xs']) ?>
   </div>
 <?php endforeach; ?>
</div>
