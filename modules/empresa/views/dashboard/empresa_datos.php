<?php
/**
 * Created by PhpStorm.
 * User: Andres santos
 * Date: 25/01/2018
 * Time: 12:17 PM
 * @var $empresa \app\models\base\Empresa
 * @var $usuario \app\models\base\Usuario
 */

use app\models\CategoriaEmpresa;
use dosamigos\ckeditor\CKEditor;
use kartik\widgets\FileInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin(); ?>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <?= $form->field($empresa, 'nombre')->textInput(['maxlength' => true]) ?>
 <?= $form->field($empresa, 'nit')->textInput(['maxlength' => true]) ?>
 <?= $form->field($empresa, 'pic')->widget(FileInput::className())->label('Imagen principal') ?>
</div>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <?= Html::img(Url::to('@web/') . $empresa->url_logo, ['class'=>'img-responsive img-thumbnail']) ?>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <?= $form->field($empresa, 'descripcion')->widget(CKEditor::className(), [
  'options' => ['rows' => 6],
  'preset' => 'basic'
 ]) ?>
 <?php
 $forms = [
   [
     'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('EmpresaSucursal'),
     'content' => $this->render('_formEmpresaSucursal', [
       'row' => \yii\helpers\ArrayHelper::toArray($empresa->empresaSucursals),
     ]),
   ],
 ];
 echo kartik\tabs\TabsX::widget([
   'items' => $forms,
   'position' => kartik\tabs\TabsX::POS_ABOVE,
   'encodeLabels' => false,
   'pluginOptions' => [
     'bordered' => true,
     'sideways' => true,
     'enableCache' => false,
   ],
 ]);
 ?>
 <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-success']) ?>

</div>
<?php ActiveForm::end(); ?>
