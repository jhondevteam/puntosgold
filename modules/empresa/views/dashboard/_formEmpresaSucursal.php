<?= \kartik\alert\Alert::widget([
  'title' => 'Información: ',
  'body' => 'Consulte los tutoriales para conoser como editar u obtener coordenadas para el mapa de sucursales.'
]) ?>
<div class="form-group" id="add-empresa-sucursal">
  <?php

  use kartik\builder\TabularForm;
  use kartik\grid\GridView;
  use yii\data\ArrayDataProvider;
  use yii\helpers\Html;

  $dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
      'pageSize' => -1
    ]
  ]);
  echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'EmpresaSucursal',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
      'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
      'ciudad_id' => [
        'label' => 'Ciudad',
        'type' => TabularForm::INPUT_WIDGET,
        'widgetClass' => \kartik\widgets\Select2::className(),
        'options' => [
          'data' => \yii\helpers\ArrayHelper::map(\app\models\Ciudad::find()
            ->orderBy('nombre')->asArray()->all(), 'id', 'nombre'),
          'options' => ['placeholder' => 'Seleccione Ciudad'],
        ],
        'columnOptions' => ['width' => '200px']
      ],
      'descripcion' => ['type' => TabularForm::INPUT_TEXT],
      'direccion' => ['type' => TabularForm::INPUT_TEXT],
      'lat' => ['type' => TabularForm::INPUT_TEXT],
      'lng' => ['type' => TabularForm::INPUT_TEXT],
      'del' => [
        'type' => 'raw',
        'label' => '',
        'value' => function ($model, $key) {
          return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' => 'Delete', 'onClick' => 'delRowEmpresaSucursal(' . $key . '); return false;', 'id' => 'empresa-sucursal-del-btn']);
        },
      ],
    ],
    'gridSettings' => [
      'panel' => [
        'heading' => false,
        'type' => GridView::TYPE_DEFAULT,
        'before' => false,
        'footer' => false,
        'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Agregar Sucursal', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowEmpresaSucursal()']),
      ]
    ]
  ]);
  echo "    </div>\n\n";
  ?>

