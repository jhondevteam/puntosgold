<?php
/**
 * Created by PhpStorm.
 * User: Andres santos
 * Date: 25/01/2018
 * Time: 12:17 PM
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
 <?php
 $form = ActiveForm::begin([
  'action' => ['updatepass'],
  'enableAjaxValidation' => true
 ]); ?>
  <div class="row">
   <?= $form->field($pass, 'oldpassword')->passwordInput() ?>
   <?= $form->field($pass, 'password1')->passwordInput() ?>
   <?= $form->field($pass, 'password2')->passwordInput() ?>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-success btn-block']) ?>
    </div>
  </div>
 <?php ActiveForm::end(); ?>
</div>