<?php
/**
 * Created by PhpStorm.
 * User: Andres santos
 * Date: 25/01/2018
 * Time: 11:56 AM
 * @var $empresa \app\models\base\Empresa
 * @var $usuario \app\models\base\Usuario
 * @var $this \yii\web\View
 */

use yii\bootstrap\Tabs;

$this->title = 'Actuaizar informacón de su empresa';
?>

<?= Tabs::widget([
  'items' => [
    [
      'label' => 'Información general',
      'content' => Yii::$app->controller->renderPartial('empresa_datos', ['empresa' => $empresa]),
      'active' => true
    ],
    [
      'label' => 'Banner',
      'content' => Yii::$app->controller->renderPartial('empresa_banner', ['foto' => new \app\models\Foto(), 'galeria' => $empresa->fotos]),
    ],
    [
      'label' => 'Videos',
      'content' => Yii::$app->controller->renderPartial('empresa_videos', ['model' => new \app\models\base\Video(), 'videos' => $empresa->videos]),
    ],
  ],
]); ?>
