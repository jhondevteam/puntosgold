<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 2/10/17
 * Time: 05:26 PM
 * @var $this \yii\web\View
 * @var $totalPagar int
 * @var $totalPuntos int
 * @var $empleados \app\models\Empleado[]
 * @var $pagoPendiente \app\models\PagoEmpresa|null
 * @var $detallesBancarios \app\models\Pagina
 * @var $empresa \app\models\Empresa
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Estado general de su empresa';
?>
<div class="<?= $pagoPendiente == null? 'col-lg-offset-3' : '' ?> col-lg-6">
  <div class="alert alert-info text-center">
    <h4>Total En ventas:
      <hr>
      <strong><?= Yii::$app->formatter->asCurrency($empresa->getPendienteTotalVendido()) ?></strong></h4>
  </div>
  <div class="alert alert-info text-center">
    <h4>Comisión publicitaria:
      <hr>
      <strong><?= Yii::$app->formatter->asCurrency($totalPagar) ?></strong></h4>
  </div>
  <div class="alert alert-info text-center">
    <h4>
      Descuento por pagos en puntos:
      <hr>
      <strong><?= Yii::$app->formatter->asCurrency($totalPuntos) ?></strong></h4>
  </div>
  <div class="alert alert-info text-center">
    <h4>Total a pagar a PuntosDorados:
      <hr>
      <strong><?= Yii::$app->formatter->asCurrency($totalPagar - $totalPuntos) ?></strong></h4>
  </div>
  <hr>
  <?php $total = 0; ?>
  <table class="table table-bordered">
    <tr>
      <td>Empleado</td>
      <td>Total por recaudar</td>
      <td>Ver</td>
    </tr>
    <?php foreach ($empleados as $empleado): ?>
      <tr>
        <td><?= $empleado->usuario->nombres ?></td>
        <td><?= Yii::$app->formatter->asCurrency($empleado->getMontoCobrar()) ?></td>
        <td><?= Html::a('ver', ['/empresa/empleado/view', 'id' => $empleado->id]) ?></td>
      </tr>
      <?php $total += $empleado->getMontoCobrar() ?>
    <?php endforeach; ?>
  </table>
  <div class="alert alert-info text-center">
    <h4>Total por recaudar:
      <hr>
      <strong><?= Yii::$app->formatter->asCurrency($total) ?></strong></h4>
  </div>
</div>
<div class="col-lg-6">
  <?php if ($pagoPendiente != null): ?>
    <div class="alert alert-danger">
      <h4>Usted tiene un pago pendiente por efectuar</h4>
    </div>
    <div class="alertalert-info">
      <h2><?= $detallesBancarios->titulo ?></h2>
      <?= $detallesBancarios->contenido ?>
    </div>
    <h3 class="text-success">Detalles del pago</h3>
    <p class="text-success">Monto : <?= Yii::$app->formatter->asCurrency($pagoPendiente->monto) ?></p>
    <p class="text-success">Generado el: <?= Yii::$app->formatter->asDatetime($pagoPendiente->fecha) ?></p>
    <p class="alert alert-warning">
      Por favor realice el pago en la infomación bancaria sumistrada
      una vez hecho por favor suba el comprobante (foto) acontinuación
    </p>
    <?php if ($pagoPendiente->url_file): ?>
      <?= Html::img("@web/$pagoPendiente->url_file",['class'=>'img-responsive img-thumbnail']) ?>
      <p>Has enviado el recibo anterior por favor espere a ser verificado.</p>
    <?php endif; ?>
    <?php if($pagoPendiente->pagado == 2): ?>
      <div class="alert alert-danger">
        Su recibo ha sido verificado y la administración
        ha rechazado la veracidad del mismo, si esto es un error por favor envie una
        nueva fotografia mas clara de su recibo para ser verificado nuevamente
      </div>
    <?php endif; ?>
    <?php $form = ActiveForm::begin([
      'action' => ['upload-file', 'id' => $pagoPendiente->id],
    ]) ?>
    <?= $form->field($pagoPendiente, 'file')->fileInput() ?>
    <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
  <?php endif; ?>
</div>
