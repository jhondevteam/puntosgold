<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 27/01/18
 * Time: 12:39 PM
 * @var $model \app\models\Ayuda
 */
?>
<div class="col-sm-4">
  <h3><?= $model->titulo ?></h3>
  <iframe src="https://www.youtube.com/embed/<?= $model->url_video ?>" frameborder="0" width="100%"
          height="300"
          allowfullscreen></iframe>
  <p><?= $model->descripcion ?></p>
</div>
