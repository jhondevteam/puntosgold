<?php
/**
 * @var $this yii\web\View
 * @var $model \app\models\Usuario
 */

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;

?>
<?php $form = ActiveForm::begin(); ?>
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'documento')->textInput(['maxlength' => true]) ?>
      <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
  <div class="col-md-12 col-lg-12 form-group">
      <?= Html::submitButton('Actualizar',['class'=>'btn btn-primary']) ?>
  </div>
</div>

<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <?php if($model->foto!= null): ?>
   <?= Html::img('@web/'.$model->foto,['width'=>'200px']) ?>
 <?php else: ?>
   <?= Html::img('@web/uploads/img/nophoto.png',['width'=>'200px']) ?>
 <?php endif; ?>
 <?= $form->field($model, 'picture')->fileInput() ?>
</div>
<?php ActiveForm::end(); ?>