<?php
/**
 * @var $this yii\web\View
 * @var $usuario \app\models\Usuario
 */

use yii\bootstrap\Tabs;
$this->title = 'INFORMACIÓN PERSONAL';
?>


<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">DATOS PERSONALES</h3>
  </div>
  <div class="panel-body">
   <?= Tabs::widget([
    'items' => [
     [
      'label' => 'Información personal',
      'content' => Yii::$app->controller->renderPartial('informacion',[
          'model'=>$usuario
      ]),
      'active' => true
     ],
     [
      'label' => 'Contraseña',
      'content' => Yii::$app->controller->renderPartial('contrasena',[
       'model'=>$contrasena
      ]),
     ],
    ],
   ]); ?>
  </div>
</div>
