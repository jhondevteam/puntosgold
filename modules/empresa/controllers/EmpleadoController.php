<?php

namespace app\modules\empresa\controllers;

use app\helper\AppHelper;
use app\models\CobroEmpresaEmpleado;
use app\models\CompraAfiliado;
use app\models\Empleado;
use app\models\search\CompraAfiliadoPuntosSearch;
use app\models\search\CompraAfiliadoSearch;
use app\models\search\EmpleadoSearch;
use app\models\Usuario;
use kartik\form\ActiveForm;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EmpleadoController implements the CRUD actions for Empleado model.
 */
class EmpleadoController extends BaseEmpresaController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete', 'activate', 'cobrar'],
            'roles' => [AppHelper::ADMIN_EMPRESA]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all Empleado models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new EmpleadoSearch([
      'empresa_id' => $this->empresa->id
    ]);
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'empresa' => $this->empresa
    ]);
  }

  /**
   * Displays a single Empleado model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    $searchModel = new CompraAfiliadoSearch([
      'empleado_id' => $model->id,
      'fecha' => date('Y-m-01'),
      'fecha_hasta' => date('Y-m-d')
    ]);
    $searchModel->prepareCompraAfiliado(Yii::$app->request->queryParams);
    return $this->render('view', [
      'model' => $model,
      'dataProvider' => $searchModel->search(),
      'searchModel' => $searchModel
    ]);
  }

  public function actionCobrar($id)
  {
    $model = $this->findModel($id);
    /** @var CobroEmpresaEmpleado $lastCobro */
    $lastCobro = $model->getCobroEmpresaEmpleados()
      ->orderBy(['id' => SORT_DESC])
      ->one();
    $cobro = new CobroEmpresaEmpleado([
      'empleado_id' => $model->id,
      'monto_cobrar' => $model->getMontoCobrar(),
      'fecha'=>date('Y-m-d H:i:s')
    ]);
    if ($lastCobro != null) {
      $cobro->monto_cobrar += $lastCobro->saldo;
    }
    $data = new ActiveDataProvider([
      'query' => $model->getCompraAfiliados()
        ->where(['is', 'cobro_empresa_id', null])
    ]);
    if ($cobro->load(Yii::$app->request->post())){
      if (Yii::$app->request->isAjax){
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($cobro);
      }
      $cobro->saldo = $cobro->monto_cobrar - $cobro->monto;
      if ($cobro->save()){
        CompraAfiliado::updateAll(['cobro_empresa_id'=>$cobro->id],['empleado_id'=>$model->id]);
        Yii::$app->session->setFlash('success','Cobro de dinero registrado correctamente');
        return $this->redirect(['index']);
      }
    }
    return $this->render('cobrar', [
      'data' => $data,
      'model' => $cobro,
      'empleado' => $model,
      'lastCobro' => $lastCobro
    ]);
  }

  /**
   * Creates a new Empleado model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Empleado();
    $user = new Usuario();
    if ($user->load(Yii::$app->request->post())) {
      $user->setPassword($user->documento);
      $user->generateKeys();
      if ($user->save()) {
        $model->usuario_id = $user->id;
        $model->empresa_id = $this->empresa->id;
        $model->save();
        Yii::$app->authManager->assign(
          Yii::$app->authManager->getRole(AppHelper::EMPLEADO), $user->id);
      }
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'user' => $user,
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Empleado model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
    $user = $model->usuario;
    $user->scenario = '';
    if ($model->load(Yii::$app->request->post())) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
        'user' => $user
      ]);
    }
  }

  /**
   * Deletes an existing Empleado model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();
    return $this->redirect(['index']);
  }

  public function actionActivate($id, $status)
  {
    $model = $this->findModel($id);
    if ($status == 1) {
      Yii::$app->authManager->revoke(Yii::$app->authManager->getRole(AppHelper::EMPLEADO), $model->usuario_id);
      Yii::$app->session->setFlash('success', 'Permisos de empleado revocados, ahora no podra registrar ventas.');
    } else {
      Yii::$app->authManager->assign(Yii::$app->authManager->getRole(AppHelper::EMPLEADO), $model->usuario_id);
      Yii::$app->session->setFlash('success', 'Permisos de empleado asignados, ahora podra registrar ventas.');
    }
    $this->redirect(['index']);
  }


  /**
   * Finds the Empleado model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Empleado the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Empleado::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
