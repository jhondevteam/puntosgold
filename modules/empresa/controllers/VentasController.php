<?php

namespace app\modules\empresa\controllers;

use app\helper\AppHelper;
use app\models\Afiliado;
use app\models\CompraAfiliado;
use app\models\CompraAfiliadoPuntos;
use app\models\search\CompraAfiliadoPuntosSearch;
use app\models\search\CompraAfiliadoSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * VentasController implements the CRUD actions for CompraAfiliado model.
 */
class VentasController extends BaseEmpresaController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'view', 'create', 'update', 'delete','findcliente'],
            'roles' => [AppHelper::ADMIN_EMPRESA]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  /**
   * Lists all CompraAfiliado models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new CompraAfiliadoSearch([
      'empresa_id' => $this->empresa->id,
      'fecha' => date('Y-m-01'),
      'fecha_hasta' => date('Y-m-d'),
    ]);
    $searchModel->prepareCompraAfiliado(Yii::$app->request->queryParams);
    $dataProvider = $searchModel->search();

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single CompraAfiliado model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    $model = $this->findModel($id);
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new CompraAfiliado model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new CompraAfiliado();

    if ($model->loadAll(Yii::$app->request->post(), ['*']) && $model->saveAll(['*'])) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing CompraAfiliado model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);

    if ($model->loadAll(Yii::$app->request->post(), ['*']) && $model->saveAll(['*'])) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing CompraAfiliado model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $this->findModel($id)->deleteWithRelated();

    return $this->redirect(['index']);
  }


  /**
   * Finds the CompraAfiliado model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return CompraAfiliado the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = CompraAfiliado::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionFindcliente($query)
  {
    Yii::$app->response->format = Response::FORMAT_JSON;
    return Afiliado::find()
      ->select(['label' => 'CONCAT(usuario.documento," - ",usuario.nombres)', 'afiliado.id','usuario_id'])
      ->joinWith(['usuario'])
      ->where(['Like', 'usuario.nombres', $query])
      ->orWhere(['like', 'usuario.documento', $query])
      ->asArray()
      ->all();
  }
}
