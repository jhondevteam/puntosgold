<?php

namespace app\modules\empresa\controllers;

use app\models\Empresa;
use app\models\Usuario;
use yii\web\Controller;

/**
 * Default controller for the `empresa` module
 */
class BaseEmpresaController extends Controller
{
  /** @var $usuario Usuario */
  public $usuario;
  /** @var $empresa Empresa */
  public $empresa;

  public function beforeAction($action)
  {
    $this->empresa = \Yii::$app->user->identity->empleado->empresa;
    $this->usuario = \Yii::$app->user->identity;
    return parent::beforeAction($action);
  }
}
