<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 26/09/17
 * Time: 10:26 PM
 */

namespace app\modules\empresa\controllers;

use app\helper\AppHelper;
use app\models\Ayuda;
use app\models\base\Video;
use app\models\Foto;
use app\models\Newpass;
use app\models\Pagina;
use app\models\PagoEmpresa;
use app\models\Usuario;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DashboardController extends BaseEmpresaController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'rules' => [
          [
            'allow' => true,
            'actions' => ['index', 'upload-file', 'perfil', 'updatepass', 'seguridad', 'upload-banner', 'delete-foto',
              'add-empresa-sucursal', 'tutoriales', 'new-video', 'video-action'],
            'roles' => [AppHelper::ADMIN_EMPRESA]
          ],
          [
            'allow' => false
          ]
        ]
      ]
    ];
  }

  public function actionIndex()
  {
    $totalPagar = $this->empresa->getPendienteTotalVendido();
    $totalPagarPuntos = $this->empresa->getPendienteTotalPuntosPagar();
    $pagoPendiente = $this->empresa->getPagoPendiente();

    return $this->render('dashboard', [
      'totalPagar' => $totalPagar,
      'totalPuntos' => $totalPagarPuntos,
      'empleados' => $this->empresa->empleados,
      'pagoPendiente' => $pagoPendiente,
      'detallesBancarios' => Pagina::findOne(['glue' => Pagina::INFO_BANCARIA]),
      'empresa' => $this->empresa
    ]);
  }

  public function actionUploadFile($id)
  {
    $model = PagoEmpresa::findOne($id);
    if ($model->empresa_id == $this->empresa->id) {
      if ($model->load(Yii::$app->request->post())) {
        $model->file = UploadedFile::getInstance($model, 'file');
        if ($model->validate(['file'])) {
          $model->url_file = AppHelper::obtenerDirectorio('recibo') .
            Yii::$app->security->generateRandomString() . "." . $model->file->extension;
          $model->pagado = 0;
          $model->save(false);
          $model->file->saveAs($model->url_file);
          Yii::$app->session->setFlash('success', 'Archivo enviado correctamente');
        }
      }
    } else
      \Yii::$app->session->setFlash('danger', 'Error acceso denegado');
    return $this->redirect(['index']);
  }

  public function actionPerfil()
  {
    $empresa = $this->empresa;
    $empresa->scenario = "update";
    $empresa->toEdit();
    $islogo = false;
    if ($empresa->loadAll(Yii::$app->request->post())) {
      $empresa->pic = UploadedFile::getInstance($empresa, 'pic');
      if ($empresa->pic != null and $empresa->pic != "") {
        unlink($empresa->url_logo);
        $empresa->url_logo = 'uploads/empresas-logo/' . $empresa->pic->baseName . '.' . $empresa->pic->extension;
        $islogo = true;
      }
      if ($empresa->saveAll(['empleados', 'fotos', 'videos'])) {
        if ($islogo)
          $empresa->pic->saveAs($empresa->url_logo);
        Yii::$app->session->setFlash('success', 'Información actualizada correctamente');
        return $this->redirect('index');
      }
    }
    return $this->render('perfil', [
      'empresa' => $empresa,
    ]);
  }

  public function actionSeguridad()
  {
    return $this->render('seguridad', [
      'pass' => new Newpass(['scenario' => 'uptpass']),
    ]);
  }

  public function actionUpdatepass()
  {
    /** @var Usuario $user */
    $user = $this->usuario;
    $pass = new Newpass(['scenario' => 'uptpass']);
    if ($pass->load(Yii::$app->request->post())) {
      if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ActiveForm::validate($pass);
      }
      if ($pass->validate()) {
        $user->password = Yii::$app->security->generatePasswordHash($pass->password1);
        $user->save();
        Yii::$app->session->setFlash('success', 'CONTRASEÑA ACTUALIZADA CORRECTAMENTE');
      }
    }
    return $this->redirect(['perfil']);
  }

  public function actionNewVideo()
  {
    $video = new Video([
      'empresa_id' => $this->empresa->id
    ]);
    if ($video->load(Yii::$app->request->post()) and $video->save()) {
      Yii::$app->session->setFlash('suceess', 'Video agregado correctamente');
    }
    return $this->redirect(['perfil']);
  }

  public function actionVideoAction($id, $action)
  {
    $video = Video::findOne($id);
    if ($video->empresa_id == $this->empresa->id) {
      if ($action == 'borrar') {
        $video->delete();
      }
      if ($action == 'visibilidad') {
        $video->activo = $video->activo? 0 : 1;
        $video->save();
      }
      Yii::$app->session->setFlash('success','Acción realizada con exito.');
    }
    return $this->redirect(['perfil']);
  }

  public function actionUploadBanner()
  {
    $foto = new Foto([
      'empresa_id' => $this->empresa->id,
    ]);
    if ($foto->load(Yii::$app->request->post())) {
      $foto->file = UploadedFile::getInstance($foto, 'file');
      if ($foto->file != "" and $foto->file != null) {
        $foto->url_foto = 'uploads/empresas/banner/' . Yii::$app->security->generateRandomString() . "." . $foto->file->extension;
        if ($foto->save()) {
          $foto->file->saveAs($foto->url_foto);
          Yii::$app->session->setFlash('success', 'INFORMACIÓN GUARDADA CORRECTAMENTE');
        }
      }
    }
    return $this->redirect(['perfil']);
  }

  public function actionDeleteFoto($id)
  {
    $foto = Foto::findOne($id);
    unlink($foto->url_foto);
    if ($foto->delete()) {
      Yii::$app->session->setFlash('success', 'INFORMACIÓN ELIMINADA CORRECTAMENTE');
    }
    return $this->redirect('perfil');
  }

  public function actionTutoriales()
  {
    $videos = Ayuda::find()
      ->where(['destino' => Ayuda::WEB_EMPRESA])
      ->orWhere(['destino' => Ayuda::APP_VENDEDOR])
      ->orderBy('titulo')
      ->all();
    return $this->render('videos', [
      'videos' => $videos
    ]);
  }

  /**
   * Action to load a tabular form grid
   * for EmpresaSucursal
   * @author Yohanes Candrajaya <moo.tensai@gmail.com>
   * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
   * @return mixed
   * @throws NotFoundHttpException
   */
  public function actionAddEmpresaSucursal()
  {
    if (Yii::$app->request->isAjax) {
      $row = Yii::$app->request->post('EmpresaSucursal');
      if ((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
        $row[] = [];
      return $this->renderAjax('_formEmpresaSucursal', ['row' => $row]);
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}