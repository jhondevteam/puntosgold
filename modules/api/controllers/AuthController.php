<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 6/06/17
 * Time: 12:23 AM
 */

namespace app\modules\api\controllers;


use app\models\Afiliado;
use app\models\LoginForm;
use app\models\SignUp;
use app\models\Usuario;
use Yii;
use yii\web\UnauthorizedHttpException;

class AuthController extends DefaultController
{
  public $appVersion = '1.7.0';

  public function actionSignUp()
  {
    $model = new SignUp();
    if ($model->load(Yii::$app->request->post(), '')) {
      if ($model->validate()) {
        if (($data = $model->signUp()) !== false) {
          /** @var Afiliado $afiliado */
          $afiliado = $data['afiliado'];
          $afiliado->creaGrupoInicial();
          return [
             'success' => 'ok',
             'user' => $data['user'],
             'afiliado' => $afiliado,
             'datos' => $afiliado->afiDatos,
            'token' => $model
          ];
        }
      } else
        return $model;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionSignUpPd()
  {
    $model = new SignUp();
    if ($model->load(Yii::$app->request->post(), '')) {
      $user = $model->checkUser();
      if ($user !== false)
        return $user;
      else {
        if ($model->validate()) {
          if (($data = $model->signUp()) !== false) {
            /** @var Afiliado $afiliado */
            $afiliado = $data['afiliado'];
            $afiliado->creaGrupoInicial();
            return $data['user'];
          }
        } else
          return $model;
      }
    }
    throw new UnauthorizedHttpException();
  }

  public function actionLoginByToken()
  {
    $model = Usuario::findIdentityByAccessToken(\Yii::$app->request->post('access_token'));
    if ($model !== null && $model->afiliado) {
      $response = [
         'nombres' => $model->nombres,
         'documento' => $model->documento,
         'email' => $model->email,
         'access_token' => $model->access_token,
         'foto' => $model->getUrlFoto(),
         'urlShare' => $model->afiliado->getUrlShare()
      ];
      return $response;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionLogin()
  {
    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post(), '')) {
      if ($model->login()) {
        $rols = Yii::$app->authManager->getRolesByUser($model->user->id);
        Yii::info($rols);
        if ($model->user->afiliado)
          return [
             'success' => 'ok',
             'user' => $model->getUser(),
             'afiliado' => $model->getUser()->afiliado,
             'datos' => $model->getUser()->afiliado->afiDatos
          ];
        else
          $model->addError('password', 'El usuario no es afiliado');
      }
      return $model;
    }
    throw new UnauthorizedHttpException();
  }

  public function actionPassword()
  {
    $email = Yii::$app->request->post('email');
    $usuario = Usuario::findOne(['email' => $email]);
    if ($usuario != null) {
      $usuario->password_reset_token = Yii::$app->security->generateRandomString();
      $usuario->save(false);
      $sendGrid = Yii::$app->mailer;
      $mensaje = $sendGrid->compose('recordar_contrasena', ['model' => $usuario]);
      $mensaje->setFrom('soporte@puntosdorados.com')
         ->setTo($usuario->email)
         ->setSubject('Recuperación de contraseña Puntos Dorados')
         ->send($sendGrid);
      return [
         'success' => 'ok'
      ];
    } else
      return [
         'success' => 'error',
         'mensaje' => 'Usuario no encontrado verifique su email.'
      ];
  }

  public function actionLoginFacebook($facebook_id, $correo)
  {
    $user = Usuario::find()
       ->innerJoinWith('afiliado')
       ->where(['facebook_id' => $facebook_id])
       ->orWhere(['email' => $correo])
       ->one();
    if ($user != null and $user->afiliado) {
      if ($user->facebook_id == null) {
        $user->facebook_id = $facebook_id;
        $user->save();
      }
      return [
         'success' => 'ok',
         'user' => $user,
         'afiliado' => $user->afiliado,
         'datos' => $user->afiliado->afiDatos
      ];
    }
    return ['success' => 'Enviar al registro'];
  }

  public function actionVersion($version)
  {
    return [
       'success' => $this->appVersion == $version ? 'ok' : 'failed',
       'url' => "https://play.google.com/store/apps/details?id=com.puntosdorados.cliente"
    ];
  }
}