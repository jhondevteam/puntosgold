<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 9/06/17
 * Time: 04:53 PM
 */

namespace app\modules\api\controllers;


use app\helper\AppHelper;
use app\models\Ayuda;
use app\models\base\Afiliado;
use app\models\CategoriaEmpresa;
use app\models\Ciudad;
use app\models\Departamento;
use app\models\Empresa;
use app\models\EmpresaSucursal;
use app\models\NotificacionEntrega;
use app\models\Pagina;
use app\models\SignUp;
use app\models\Soporte;
use app\models\TokenCompra;
use app\models\TransferenciaPuntos;
use app\models\Usuario;
use Da\QrCode\Contracts\ErrorCorrectionLevelInterface;
use Da\QrCode\QrCode;
use DateTime;
use mysqli;
use sintret\chat\models\Chat;
use Yii;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rest\Serializer;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class AppController extends DefaultController
{

  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['authenticator'] = [
       'class' => CompositeAuth::class,
       'authMethods' => [
          QueryParamAuth::class,
          HttpBearerAuth::class
       ],
    ];
    return $behaviors;
  }

  public function actionIndex()
  {
    $user = \Yii::$app->user->identity;
    $afiliado = $user->afiliado; 
    $afiliadosMes = $afiliado->getCurrentPuntosAfiliadosMes();
    $nombres =$afiliado->getNombresGrupo();
    $grupo =$afiliado->getGrupoAsignado();

//    $referidosMes = $afiliado->getCurrentPuntosReferidosMes();
    $app = Yii::$app->request->get('app', false);
    if ($app === false)
      $pagina = Pagina::findOne(['glue' => Pagina::TERMINO_OFERTA]);
    else
      $pagina = Pagina::findOne(['glue' => Pagina::TERMINOS_OFERTA_PASAJEROS]);
    return [
       'numeroAfiliados' => $afiliado->getGrupos()->
       joinWith('afiliados b')->count('b.id'),
       'puntos' => \Yii::$app->formatter->asInteger($afiliado->puntos),
       'afiliados' => $afiliadosMes->puntos,
       'referidos' => "$ 10.000",
       'nombres' => $nombres,
       'grupo' => $grupo,
       'code' => $afiliado->code_invitacion,
       'isFacebookPass' => $user->facebook_id != null ? $user->validatePassword($user->facebook_id) : false,
       'terminos' => $pagina
    ];
  }

  public function actionQrcode()
  {
    \Yii::$app->response->format = Response::FORMAT_RAW;
    /** @var Afiliado $model */
    $model = Yii::$app->user->identity->afiliado;
    $token = new TokenCompra([
       'afiliado_id' => $model->id,
       'token' => Yii::$app->security->generateRandomString(20),
       'fecha' => date('Y-m-d H:i:s'),
    ]);
    $token->save();
    Yii::info(Yii::$app->basePath);
    $qrCode = (new QrCode(Json::encode([
       'afiliado_id' => $model->id,
       'nombres' => $model->usuario->nombres,
       'foto' => Url::to($model->usuario->foto, true),
       'token' => $token->token,
       'puntos' => $model->puntos
    ])))
       ->useLogo(Yii::$app->basePath . '/web/theme/img/logo.png')
       ->useForegroundColor(89, 48, 17)
       ->useEncoding('UTF-8')
       ->setErrorCorrectionLevel(ErrorCorrectionLevelInterface::HIGH)
       ->setLogoWidth(90)
       ->setSize(300)
       ->setMargin(5);
    header('Content-Type: ' . $qrCode->getContentType());
    return $qrCode->writeString();
  }

  public function actionEmpresas()
  {
    $city = \Yii::$app->request->get('address');
    $afiliado = Yii::$app->user->identity->afiliado;
    $categoria = \Yii::$app->request->get('categoria_id');
    $ciudad = null;
    if (!empty($city)) {
      if (is_numeric($city))
        $ciudad = Ciudad::findOne($city);
      else
        $ciudad = Ciudad::find()
           ->where(['like', 'nombre', $city])
           ->one();
    }
    $empresas = EmpresaSucursal::find()
       ->andFilterWhere(['empresa.categoria_id' => $categoria])
       ->orderBy('empresa.nombre')
       ->joinWith(['empresa.categoria', 'ciudad'])
       ->andWhere(['empresa.estado' => 1])
       ->groupBy('empresa_id');
    if ($ciudad != null) {
      $empresas->andWhere(['ciudad.id' => $ciudad->id]);
    }
    if ($afiliado->id != '1') {
      $empresas->andWhere(['categoria_empresa.activa' => 1]);
    }
    $serialize = new Serializer();
    return $serialize->serializeModels($empresas->all());

  }

  public
  function actionEmpresasMapa()
  {
    $positions = Yii::$app->request->post();
    $afiliado = Yii::$app->user->identity->afiliado;
    $noreste = explode('|', $positions['farLeft']);
    $suroeste = explode('|', $positions['nearRight']);
    $latNoreste = $noreste[0];
    $latsurOeste = $suroeste[0];
    $lngNoreste = $noreste[1];
    $lngSurOeste = $suroeste[1];

    $empresas = EmpresaSucursal::find()
       ->joinWith(['empresa.categoria', 'ciudad'])
       ->andWhere(['empresa.estado' => 1])
       ->andWhere(['between', 'empresa_sucursal.lat', $latsurOeste, $latNoreste])
       ->andWhere(['between', 'empresa_sucursal.lng', $lngSurOeste, $lngNoreste])
       ->orderBy(['empresa.nivel_importancia' => SORT_DESC])
       ->limit(10);
    if ($afiliado->id !== 1) {
      $empresas->andWhere(['!=', 'empresa.id', '1']);
    }

    $empresas = $empresas->all();
    $serialize = new Serializer();
    return $serialize->serializeModels($empresas);
  }

  public
  function actionEmpresa()
  {
    return Empresa::findOne(Yii::$app->request->get('id'));
  }

  public
  function actionCategorias($sort)
  {
    $afiliado = Yii::$app->user->identity->afiliado;
    $city = \Yii::$app->request->get('address', false);
    $state = \Yii::$app->request->get('state', false);
    $q = CategoriaEmpresa::find()
       ->innerJoinWith('empresas.empresaSucursals.ciudad')
       ->andWhere(['empresa.estado' => 1])
       ->andWhere(['categoria_empresa.activa' => 1])
       ->orderBy("nombre $sort");
    if ($afiliado->id == '1') {
      $q->orWhere(['categoria_empresa.activa' => 0]);
    }
    if ($city == 'undefined' and $city == null and $city == false) {
      $city = Yii::$app->user->identity->afiliado->city;
      $city = Ciudad::findOne(['nombre' => $city]);
    } else {
      $afiliado->city = $city;
      $afiliado->save(false);
      $city = Ciudad::findOne(['nombre' => $city]);
    }
    Yii::info($city);
    Yii::info($state);
    if ($state !== false and $state != 'undefined' and $state != null) {
      Yii::info($city);
      $estado = Departamento::findOne(['nombre' => $state]);
      return [
         'success' => 'ok',
         'ciudades' => $estado != null ? $estado->ciudades : [],
         'categorias' => $q->andWhere(['ciudad.departamento_id' => $estado->id])->all(),
         'ciudad_actual' => $city != null ? $city->id : $estado->getCiudades()->where(['principal' => 1])->one()->id
      ];
    } else {
      if ($city !== null and $city !== 'undefined' and $city != false) {
        return [
           'categorias' => $q->andWhere(['LIKE', 'ciudad.departamento_id', $city->departamento_id])->all()
        ];
      }
    }
    return [
       'success' => 'error',
       'departamentos' => Departamento::find()->where(['activo' => 1])
          ->orderBy('nombre')->all(),
       'categorias' => $q->all()
    ];
  }

  public
  function actionAfiliar()
  {
    $identity = \Yii::$app->user->identity;
    $user = new SignUp([
       'coderef' => $identity->afiliado->code_invitacion,
    ]);
    if ($user->load(\Yii::$app->request->post(), '')) {
      $user->password = $user->documento;
      if (($resp = $user->signUp()) !== false) {
        /** @var Usuario $modelUser */
        return [
           'success' => 'ok',
           'data' => $resp
        ];
      } else
        return $user;
    }
    throw new UnauthorizedHttpException();
  }

  public
  function actionCompras()
  {
    $afiliado = \Yii::$app->user->identity->afiliado;
    $mes = Yii::$app->request->get('mes', 'actual');
    if ($mes == 'actual') {
      $initMes = date('Y-m-01 00:00:00');
      $finMes = date('Y-m-d 23:59:59');
    } else {
      $initMes = date('Y-m-01 00:00:00', strtotime('-1 month', strtotime(date('Y-m-01 00:00:00'))));
      $lastDay = new DateTime();
      $lastDay->modify('last day of ' . date('F Y', strtotime($initMes)));
      $finMes = $lastDay->format('Y-m-d 23:59:59');
    }
    $serialice = new Serializer();

    $query = $afiliado->getCompraAfiliados()
       ->where(['between', 'fecha', $initMes, $finMes])
       ->orderBy(['id' => SORT_DESC]);
    $q = clone $query;
    return [
       'compras' => $serialice->serializeModels($query->all()),
       'montoTotal' => ($query->sum('monto')),
       'puntosTotal' => ($q->andWhere(['pago_puntos' => true])->sum('monto')),
    ];

  }


  public
  function actionToken()
  {
    $token = \Yii::$app->request->post();
    $afi = \Yii::$app->user->identity->afiliado;
    if (isset($token['app_version'])) {
      $afi->app_version = $token['app_version'];
    }
    $afi->app_token = Json::encode($token);
    $afi->save(false);
    return [
       'success' => $token
    ];
  }

  public
  function actionAyuda()
  {
    return Ayuda::find()
       ->where(['destino' => Ayuda::APP_AFILIADO])
       ->orderBy('titulo')
       ->all();
  }

  public
  function actionChat()
  {
    $user = Yii::$app->user->identity;
    $soporte = Soporte::findOne([
       'usuario_id' => $user->id,
       'active' => true
    ]);
    if ($soporte != null)
      return $soporte;
    return [];
  }

  public
  function actionSendChat()
  {
    $user = Yii::$app->user->identity;
    $soporte = Soporte::findOne([
       'usuario_id' => $user->id,
       'active' => true
    ]);
    if ($soporte == null) {
      $soporte = new Soporte([
         'usuario_id' => $user->id,
         'active' => true,
         'fecha' => date('Y-m-d'),
      ]);
      $soporte->save();
    }
    $mensaje = Yii::$app->request->post('mesaje');
    $chat = new Chat([
       'message' => $mensaje,
       'userId' => $user->id,
       'soporte_id' => $soporte->id
    ]);
    if (!$chat->save()) {
      Yii::info($chat->getErrors());
    }
    return $soporte;
  }

  public
  function actionSendPosition()
  {
    $afiliado = Yii::$app->user->identity->afiliado;
    $lat = floatval(Yii::$app->request->get('lat'));
    $lng = floatval(Yii::$app->request->get('lng'));
    Yii::info($lat . " - " . $lng);
    $cuadradoLat = [$lat - 0.01, $lat + 0.01];
    $cuadradoLng = [$lng + 0.01, $lng - 0.01];
    Yii::info($cuadradoLng);
    Yii::info($cuadradoLat);
    $empresa = EmpresaSucursal::find()
       ->where(['between', 'lat', $cuadradoLat[0], $cuadradoLat[1]])
       ->andWhere(['between', 'lng', $cuadradoLng[0], $cuadradoLng[1]])
       ->one();
    $verificacion = null;
    if ($empresa != null)
      $verificacion = $afiliado->getNotificacionEntregas()
         ->where(['sucursal_id' => $empresa->id, 'fecha' => date('Y-m-d')])
         ->one();
    if ($afiliado->app_token !== null and $verificacion == null) {
      $token = Json::decode($afiliado->app_token);

      $result = AppHelper::sendNotificationAndroid($token['token'], 'Estas cerca de ganar.',
         'Tienes una oportunidad de ganar puntos cerca de ti, gana puntos, al comprar en nuestra empresa afiliada ' . $empresa->empresa->nombre, $empresa->empresa->getInfo(), 'EmpresaPage', 'Ver',
         [
            'empresa_id' => $empresa->empresa_id
         ],
         $empresa->empresa->getImagen(), $afiliado->sound
      );
      $notificacionEntrega = new NotificacionEntrega([
         'afiliado_id' => $afiliado->id,
         'fecha' => date('Y-m-d'),
         'app_token' => $token['token'],
         'sucursal_id' => $empresa->id,
         'resultado' => $result
      ]);
      $notificacionEntrega->save();
      yii::info($notificacionEntrega->getErrors());
    }
    return [];
  }

  public
  function actionTerminos()
  {
    $user = Yii::$app->user->identity;
    $term = Pagina::findOne(['glue' => Pagina::TERMINOS_AFILIADOS]);
    $term->contenido = str_replace('(nombre afiliado)', $user->nombres, $term->contenido);
    $term->contenido = str_replace('(cedula afiliado)', $user->documento, $term->contenido);
    return $term;
  }

  public
  function actionDeleteToken()
  {
    $afiliado = Yii::$app->user->identity->afiliado;
    $afiliado->app_token = null;
    $afiliado->save(false);
  }

  public
  function actionValidatePassword()
  {
    $pass = Yii::$app->request->post('password');
    $user = Yii::$app->user->identity;
    if ($user->validatePassword($pass))
      return [
         'success' => 'ok'
      ];
    return [
       'success' => 'error',
       'mensaje' => 'Contraseña incorrecta'
    ];
  }

  public
  function actionTransferencia()
  {
    $afiliado = Yii::$app->user->identity->afiliado;
    $model = new TransferenciaPuntos([
       'de_afiliado_id' => $afiliado->id,
       'fecha' => date('Y-m-d h:i:s')
    ]);
    if ($model->load(Yii::$app->request->post(), '')) {
      if (Yii::$app->request->get('validacion', false)) {
        $model->scenario = 'validation';
        if ($model->validate()) {
          $ser = new Serializer();
          return ['success' => 'ok', 'model' => $ser->serializeModel($model)];
        } else
          return $model;
      } else {
        if ($model->save()) {
          $model->deAfiliado->puntos -= $model->monto;
          $model->deAfiliado->save(false);
          $model->paraAfiliado->puntos += $model->monto;
          $model->paraAfiliado->save(false);
          return ['success' => 'ok'];
        }
      }
    }
    throw new UnauthorizedHttpException();
  }

  public function actionFindEmpresas($query)
  {
    $serialize = new Serializer();
    $q = Empresa::find()
       ->where(['estado' => 1])
       ->andWhere(['LIKE', 'nombre', $query])
       ->orderBy('nivel_importancia');
    if (Yii::$app->user->identity->afiliado->id == '1')
      $q->orWhere(['estado' => 0]);
    return $serialize->serializeModels($q->limit(10)->all());
  }
}