<?php

return [
  'class' => 'yii\db\Connection',
  'dsn' => 'mysql:host=localhost;dbname=puntosgold',
  'username' => 'root',
  'password' => 'some_pass',
  'charset' => 'utf8',
  'attributes' => [PDO::ATTR_CASE => PDO::CASE_LOWER]
];
