<?php

$params = require(__DIR__ . '/params.php');

$config = [
  'id' => 'puntosdorados.com',
  'name' => 'Puntos Dorados',
  'language' => 'es-CO',
  'basePath' => dirname(__DIR__),
  'timeZone' => 'America/Bogota',
  'bootstrap' => ['log'],
  'components' => [
    'formatter' => [
      'class' => 'yii\i18n\Formatter',
      'thousandSeparator' => '.',
      'decimalSeparator' => ',',
      'currencyCode' => 'COP',
    ],
    'request' => [
      'cookieValidationKey' => 'RXkvCrV3nGI2IdHOD8PEic56m19FwJ95',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'app\models\Usuario',
      'enableAutoLogin' => true,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => [
      'class' => 'wadeshuler\sendgrid\Mailer',
      'apiKey' => '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'
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => 'yii\log\FileTarget',
          //'levels' => ['error', 'warning','info','all'],
        ],
      ],
    ],
    'db' => require(__DIR__ . '/host_db.php'),
    'urlManager' => [
      'enablePrettyUrl' => true,
      'showScriptName' => false,
      'rules' => [
      ],
    ],
    'authManager' => [
      'class' => 'yii\rbac\DbManager',
      'defaultRoles' => ['guest'],
    ],
  ],
  'modules' => [
    'datecontrol' => [
      'class' => 'kartik\datecontrol\Module',
    ],
    'admin' => [
      'class' => 'app\modules\admin\Module',
      'defaultRoute' => 'empresas'
    ],
    'empresa' => [
      'class' => 'app\modules\empresa\Module',
      'defaultRoute' => 'empleado'
    ],
    'api' => [
      'class' => 'app\modules\api\Module',
    ],
    'apisell' => [
      'class' => 'app\modules\apisell\Module',
    ],
    'apidriver' => [
      'class' => 'app\modules\apidriver\Module',
    ],
    'apirider' => [
      'class' => 'app\modules\apirider\Module',
    ],
    'gridview' => [
      'class' => '\kartik\grid\Module'
    ]
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    'allowedIPs' => ['*'],
  ];
  
  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];
}

return $config;
