<?php

namespace app\helper;
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 2/06/17
 * Time: 04:05 PM
 */
class AppHelper
{
  const ADMIN_EMPRESA = 'Administrador_Empresa';
  const SUPER_ADMIN = 'SuperAdministrador';
  const EMPLEADO = 'Empleado';
  const ADMIN_ZONA = "Administrador_Zona";

  /**
   * @param $folder
   * @return string
   * Esta funcion crea el directorio en caso de que no exista
   * por defecto la carpeta de retorna en formato
   * uploads/$folder/
   */
  public static function obtenerDirectorio($folder)
  {
    $path = 'uploads/';
    if (!file_exists('uploads'))
      mkdir('uploads');
    if (!file_exists("uploads/$folder")) {
      $folders = explode('/', $folder);
      $path = 'uploads/';
      foreach ($folders as $folder) {
        if (!file_exists($path . $folder)) {
          mkdir($path . $folder);
        }
        $path .= $folder . '/';
      }
    } else
      $path .= "$folder/";

    return $path;
  }

  public static function getPorcentajes()
  {
    $porcent = range(0, 100);
    $parse = [];
    foreach ($porcent as $key => $val) {
      $parse[$val] = $val;
    }
    return $parse;
  }

  public static function getPorcentajeNumber($porcentaje)
  {
    return $porcentaje / 100;
  }

  public static function getNumberPorcentaje($porcentaje)
  {
    return $porcentaje * 100;
  }

  public static function getCodeInvitacion($numerodeletras = 4)
  {
    $caracteres = "ABCDEFHIJKLMNPQRSTUVWXYZ12345789";
    $cadena = "";
    for ($i = 0; $i < $numerodeletras; $i++) {
      $cadena .= substr($caracteres, rand(0, strlen($caracteres)), 1);
    }
    if (strlen($cadena) < 4) {
      $cadena = AppHelper::getCodeInvitacion();
    }
    return $cadena;
  }

  /**
   * @param $token string de la aplicacion android
   * @param $titulo string titulo de la notificación
   * @param $contenido string cuerpo mensaje de la notificacion
   * @param null $sumary
   * @param null $go
   * @param string $gotext
   * @param null $param
   * @param null $image
   * @param bool $sound
   * @return mixed
   */
  public static function sendNotificationAndroid($token, $titulo, $contenido, $sumary = null, $go = null,
                                                 $gotext = 'Abrir', $param = null, $image = null, $sound = false)
  {
    $apiKey = "AAAAuD4VlNQ:APA91bHMO4co1T0aJIylUAZHgZIlDxC-7e0U9lJGGm0V8stfw_tx1paWgic33Q38U21WBLNDxc-C1_z_MIQxo3MxfytKRO1Esi7BquJPfWHyiuVl4FQOj-_-mDcPCJVwKlRLTxPT1haK";
    if (!is_array($token))
      $registrationIDs = array($token);
    else
      $registrationIDs = $token;
    $url = 'https://android.googleapis.com/gcm/send';
    $fields = array(
      'registration_ids' => $registrationIDs,
      'data' => [
        'title' => $titulo,
        "message" => $contenido,
        "summaryText" => $sumary,
        'payload' => [
          'image' => $image,
          'go' => $go,
          'gotext' => $gotext,
          'params' => $param,
        ],
        'soundname' => $sound ? "sound" : false,
      ],
    );
    $headers = array(
      'Authorization: key=' . $apiKey,
      'Content-Type: application/json'
    );

    // Open connection
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    \Yii::info($result);
    curl_close($ch);
    return $result;
  }
}