<?php
/**
 * @var $this \yii\web\View
 * @var $accionista \app\models\Accionista
 * @var $config array
 */
?>
<P class="nombre">
  <?= ucwords(strtolower($accionista->nombres)) ?> <?= ucwords(strtolower($accionista->apellidos)) ?>
</P>
<p style="text-align: center">
  <?= $accionista->tipo_doc ?> <?= $accionista->documento ?>
</p>
<table style="width: 100%">
  <tr>
    <td>
      <p class="info">
        Cantidad de acciones <span class="text-line"><?= $accionista->acciones ?></span><br>
        Valor de cada acción
        <span class="text-line"><?= Yii::$app->formatter->asCurrency($config['precio']) ?></span>, <br>
        Valor total de las acciones: <span
            class="text-line"><?= Yii::$app->formatter->asCurrency($accionista->total_pagar) ?> </span><br>
        Fecha de compra: <span class="text-line"><?= Yii::$app->formatter->asDate($accionista->fecha_registro) ?></span>
      </p>
    </td>
    <td style="text-align: right">
      <img src="<?= $accionista->getQr() ?>" class="qr">
    </td>
  </tr>
</table>

