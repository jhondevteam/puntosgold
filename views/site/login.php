<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="why-choose-area section-big">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3"
           style="overflow: hidden">
       <?php $form = ActiveForm::begin([
        'id' => 'login-form',
       ]); ?>
       <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
       <?= $form->field($model, 'password')->passwordInput() ?>
       <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div class="form-group">
         <?= Html::submitButton('Iniciar sesión', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
        </div>
        <?= Html::a('¿Olvidaste la contraseña?',['contrasena']) ?>
       <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</section>

