<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 15/09/17
 * Time: 02:17 AM
 * @var $model \app\models\SignUp
 * @var $this \yii\web\View
 */

use app\models\Texto;
$this->title = 'Registro - Puntos Dorados "club"';
?>
<section id="contact" class="contact-area section-big">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section-title">
          <?= Texto::printText('titulo_registro', 'h2') ?>
          <?= Texto::printText('descripcion_registro', 'p') ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6" style="overflow: hidden">
        <div class="contact-form" style="overflow: hidden">
          <?php $form = \yii\bootstrap\ActiveForm::begin([
            'action' => ['registro']
          ]); ?>
          <div class="form-group in_ref">
            <?= $form->field($model, 'coderef')
              ->textInput(['placeholder' => 'Codigo de referido','readonly'=>(Yii::$app->session->get('code') != null)]) ?>
          </div>
          <div class="form-group in_name">
            <?= $form->field($model, 'nombres')->textInput(['placeholder' => 'Nombre completo']) ?>
          </div>
          <div class="form-group in_email">
            <?= $form->field($model, 'email')->textInput(['placeholder' => 'Ingrese su email']) ?>
          </div>
          <div class="form-group in_id_card">
            <?= $form->field($model, 'documento')->textInput(['placeholder' => 'Ingrese su Documento']) ?>
          </div>
          <div class="form-group in_lock">
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Contraseña']) ?>
          </div>
          <div class="col-md-12 col-lg-12 form-group">
            <?= \yii\helpers\Html::submitButton('Completar', ['class' => 'btn']) ?>
          </div>
          <?php \yii\bootstrap\ActiveForm::end(); ?>
          <div id="form-messages"></div>
        </div>
      </div>
      <div class="col-md-6">
        <?= Texto::printText('text_registro_right','div') ?>
      </div>
    </div>
  </div>
</section>
<?php
if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)) {
  \dosamigos\ckeditor\CKEditorAsset::register($this);
  $this->registerJsFile("@web/js/inline-edit.js", [
    'depends' => \yii\web\JqueryAsset::className()
  ]);
} ?>
