<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="why-choose-area section-big">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3"style="overflow: hidden">
      <h4>Por favor ingresa un corréo  electrónico (email)</h4>
       <?php $form = ActiveForm::begin(); ?>
       <?= $form->field($usuario, 'email')->textInput(['maxlength' => true])->label(false) ?>
        <div class="col-md-12 col-lg-12 form-group">
         <?= Html::submitButton('Enviar', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
       <?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</section>

