<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 13/10/17
 * Time: 10:36 AM
 * @var $model \app\models\Pagina
 */
?>
<div class="container section-big">
  <h2 class="section-title text-center"><?= $model->titulo ?></h2>
  <div class="">
    <?= $model->contenido ?>
  </div>
</div>
