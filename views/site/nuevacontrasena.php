<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var \app\models\Newpass $model */
/** @var $this \yii\web\View */
?>


<section class="why-choose-area section-big">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3"
           style="overflow: hidden">
        <h4>Recuperación de contraseña</h4>
        <?php $form = ActiveForm::begin(); ?>
        <p>Nueva contraseña</p>
        <?= $form->field($model, 'password')->passwordInput(['class'=>'password form-control'])->label(false) ?>
        <p>Confirmar contraseña</p>
        <?= $form->field($model, 'password2')->passwordInput(['class'=>'password form-control'])->label(false) ?>
        <div class="form-group">
          <?= Html::button('<i class="glyphicon glyphicon-eye-open"></i> Ver Caracteres',['class'=>'btn btn-default btn-xs','id'=>'view-pass']) ?>
        </div>
        <div class="box-footer">
          <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn-block']) ?>
        </div>
      </div>
      <?php ActiveForm::end(); ?>
      <div class="clear"></div>
    </div>
  </div>
</section>
<?php
$this->registerJs("
var type = 'password';
$('#view-pass').click(function(){
  var inputs = $('.password');
  console.log(inputs);
    if(type == 'password'){
      type = 'text';
    }else{
      type = 'password';
    }
      inputs.each(function(i,el){
      console.log(type,el);
        $(el).attr('type',type);
      })
})
")
?>