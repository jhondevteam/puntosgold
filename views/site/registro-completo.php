<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 15/09/17
 * Time: 12:16 PM
 */

use app\models\Texto;

?>
<section id="contact" class="contact-area section-big">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section-title">
          <?= Texto::printText('titulo_registro_completo', 'h2') ?>
          <?= Texto::printText('descripcion_registro_completo', 'p') ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <?= Texto::printText('titulo_registro_bienvenido', 'h2') ?>
        <?= Texto::printText('texto_registro_bienvenido', 'p') ?>
      </div>
      <div class="col-md-6">
        <h3 class="text-center">Ya !.</h3>
        <a href="https://play.google.com/store/apps/details?id=com.puntosdorados.cliente" target="_blank"><img
              src="<?= \yii\helpers\Url::to('@web/theme/img/google-play-button.png') ?>" alt="">
        </a>
        <hr>
        <h3 class="text-center">Pronto !.</h3><img
            src="<?= \yii\helpers\Url::to('@web/theme/img/apple-app-store.png') ?>" alt="">

      </div>
    </div>
  </div>
</section>
<section id="service" class="service-area section-big">
  <div class="container">

    <div class="row">
      <div class="col-md-12 text-center">
        <div class="section-title">
          <?= Texto::printText('inicio_caracteristicas_titulo', 'h2') ?>
          <?= Texto::printText('inicio_caracteristicas_subtitulo', 'p') ?>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col-sm-4">
        <div class="choose-box left">
          <i class="fa fa-tv"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica1_title', 'h3') ?>
            <?= Texto::printText('caracteristica1_descripcion', 'p') ?>
          </div>
        </div>
        <div class="choose-box left">
          <i class="fa fa-lightbulb-o"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica2_title', 'h3') ?>
            <?= Texto::printText('caracteristica2_descripcion', 'p') ?>
          </div>
        </div>
        <div class="choose-box left">
          <i class="fa fa-eye"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica3_title', 'h3') ?>
            <?= Texto::printText('caracteristica3_descripcion', 'p') ?>
          </div>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="feture-img">
          <img src="<?= \yii\helpers\Url::to('@web/theme/img/feature/1.jpg') ?>" alt="">
        </div>
      </div>

      <div class="col-sm-4">
        <div class="choose-box right">
          <i class="fa fa-bolt"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica4_title', 'h3') ?>
            <?= Texto::printText('caracteristica4_descripcion', 'p') ?>
          </div>
        </div>
        <div class="choose-box right">
          <i class="fa fa-hand-pointer-o"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica5_title', 'h3') ?>
            <?= Texto::printText('caracteristica5_descripcion', 'p') ?>
          </div>
        </div>
        <div class="choose-box right">
          <i class="fa fa-shopping-bag"></i>
          <div class="choose-content">
            <?= Texto::printText('caracteristica6_title', 'h3') ?>
            <?= Texto::printText('caracteristica6_descripcion', 'p') ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php
if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)) {
  \dosamigos\ckeditor\CKEditorAsset::register($this);
  $this->registerJsFile("@web/js/inline-edit.js", [
    'depends' => \yii\web\JqueryAsset::className()
  ]);
} ?>
