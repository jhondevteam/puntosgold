<?php

use app\models\Texto;
use dosamigos\ckeditor\CKEditorAsset;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

/**
 * @var $testimonios \app\models\Testimonios[]
 * @var $preguntas \app\models\PreguntasRespuestas[]
 */
$this->title = Yii::$app->name;
?>
  <section id="slider" class="slider-area">
    <div class="table">
      <div class="table-cell">
        <div class="intro-text">
          <div class="container">
            <div class="row">

              <!-- intro image -->
              <div class="col-md-6 col-sm-12 intro-img">
                <img src=" <?= Url::to('@web/theme/img/slider/2.png') ?>" alt="">
              </div>

              <!-- intro text -->
              <div class="col-md-6 col-sm-12">
                <div class="intro-text-box clearfix">
                 <?= Texto::printText('titulo_banner', 'h1') ?>
                 <?= Texto::printText('texto_banner', 'p') ?>
                  <a href="#" class="dwnld-btn playstore"><img
                        src="<?= Url::to('@web/theme/img/google-play-button.png') ?>" alt=""></a>
                  <a href="#" class="dwnld-btn appstore"><img
                        src="<?= Url::to('@web/theme/img/app-store-button.svg') ?>"
                        alt=""></a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section id="service" class="service-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12 text-center">
          <div class="section-title">
           <?= Texto::printText('inicio_caracteristicas_titulo', 'h2') ?>
           <?= Texto::printText('inicio_caracteristicas_subtitulo', 'p') ?>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-sm-4">
          <div class="choose-box left">
            <i class="fa fa-tv"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica1_title', 'h3') ?>
             <?= Texto::printText('caracteristica1_descripcion', 'p') ?>
            </div>
          </div>
          <div class="choose-box left">
            <i class="fa fa-lightbulb-o"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica2_title', 'h3') ?>
             <?= Texto::printText('caracteristica2_descripcion', 'p') ?>
            </div>
          </div>
          <div class="choose-box left">
            <i class="fa fa-eye"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica3_title', 'h3') ?>
             <?= Texto::printText('caracteristica3_descripcion', 'p') ?>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="feture-img">
            <img src="<?= Url::to('@web/theme/img/feature/1.jpg') ?>" alt="">
          </div>
        </div>

        <div class="col-sm-4">
          <div class="choose-box right">
            <i class="fa fa-bolt"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica4_title', 'h3') ?>
             <?= Texto::printText('caracteristica4_descripcion', 'p') ?>
            </div>
          </div>
          <div class="choose-box right">
            <i class="fa fa-hand-pointer-o"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica5_title', 'h3') ?>
             <?= Texto::printText('caracteristica5_descripcion', 'p') ?>
            </div>
          </div>
          <div class="choose-box right">
            <i class="fa fa-shopping-bag"></i>
            <div class="choose-content">
             <?= Texto::printText('caracteristica6_title', 'h3') ?>
             <?= Texto::printText('caracteristica6_descripcion', 'p') ?>
            </div>
          </div>
        </div>

      </div>


    </div>
  </section>
  <section id="action" class="action-area section-small">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
         <?= Texto::printText('fondo_azul', 'h2') ?>
        </div>
        <div class="col-md-3"><a href="" class="btn btn-lg btn-white">GET IT NOW</a></div>
      </div>
    </div>
  </section>
  <section id="about" class="about-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12 text-center">
          <div class="section-title">
           <?= Texto::printText('titulo_pantallazos', 'h2') ?>
           <?= Texto::printText('descripcion_pantallazos', 'p') ?>
          </div>
        </div>

        <!-- screenshots starts -->
        <div class="screenshots">
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/1.png') ?>" alt=""></div>
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/2.png') ?>" alt=""></div>
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/3.png') ?>" alt=""></div>
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/4.png') ?>" alt=""></div>
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/5.png') ?>" alt=""></div>
          <div class="screen"><img src="<?= Url::to('@web/theme/img/screenshots/6.png') ?>" alt=""></div>
        </div>
        <!-- screenshots ends -->

      </div>
    </div>
  </section>


  <div class="fun-fact-area section-small">
    <div class="container">
      <!-- Counter starts -->
      <div class="row fun-fact-area">
        <div class="col-sm-6 col-md-3 col-xs-6 col-xxs-12">
          <div class="fun-fact tab-margin-bottom">
            <i class="fa fa-heart-o"></i>
            <h3><span class="cp-counter">290</span></h3>
            <p>Downloads</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 col-xxs-12">
          <div class="fun-fact tab-margin-bottom sm-no-border xs-no-border">
            <i class="fa fa-user-o"></i>
            <h3><span class="cp-counter">550</span></h3>
            <p>Happy Clients</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 col-xxs-12">
          <div class="fun-fact">
            <i class="fa fa-envelope-o"></i>
            <h3><span class="cp-counter">924</span></h3>
            <p>Conversation</p>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 col-xs-6 col-xxs-12">
          <div class="fun-fact last">
            <i class="fa fa-comment-o"></i>
            <h3><span class="cp-counter">1656</span></h3>
            <p>Comments</p>
          </div>
        </div>
      </div>
      <!-- Counter ends -->
    </div>
  </div>

  <section id="testimonial" class="testimonial-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12 text-center">
          <div class="section-title">
           <?= Texto::printText('titulo_testimonios', 'h2') ?>
           <?= Texto::printText('descripcion_testimonios', 'p') ?>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-md-12">
          <div class="testimonial-list">
           <?php if ($testimonios != null): ?>
            <?php foreach ($testimonios as $testimonio): ?>
               <div class="single-testimonial">
                 <i class="fa fa-quote-left"></i>
                 <h3 class="subtitle"><?= $testimonio->titulo ?></h3>
                 <p><?= $testimonio->descripcion ?></p>
                 <div class="clearfix"></div>
                 <h4><?= $testimonio->nombre ?></h4>
               </div>
            <?php endforeach; ?>
           <?php endif; ?>
          </div>
        </div>
      </div>

    </div>
  </section>
  <section id="why-choose" class="why-choose-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
           <?= Texto::printText('titulo_elegirnos', 'h2') ?>
           <?= Texto::printText('descripcion_elegirnos', 'p') ?>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-sm-4">
          <img src="<?= Url::to('@web/theme/img/choose/choose.jpg') ?>" alt="">
        </div>

        <div class="col-sm-8">
          <div class="choose-box right">
            <div class="choose-content">
             <?= Texto::printText('titulo2_elegirnos', 'h3') ?>
             <?= Texto::printText('descripcion2_elegirnos', 'p') ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-sm-4 col-sm-push-8">
          <img src="<?= Url::to('@web/theme/img/choose/left.jpg') ?>" alt="">
        </div>

        <div class="col-sm-8 col-sm-pull-4">
          <div class="choose-box left">
            <div class="choose-content">
             <?= Texto::printText('titulo3_elegirnos', 'h3') ?>
             <?= Texto::printText('descripcion3_elegirnos', 'p') ?>
            </div>
          </div>
        </div>

      </div>

    </div>
  </section>

  <div id="faq" class="faq-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>PREGUNTAS FRECUENTES</h2>
          </div>
        </div>
      </div>

      <div class="row">

        <div class="col-md-8 col-md-offset-2">
          <div class="accordion panel-group" id="accordion" role="tablist">
           <?php if ($preguntas != null): ?>
            <?php foreach ($preguntas as $key => $pregunta): ?>
               <div class="panel clearfix">
                 <div class="panel-heading" role="tab">
                   <a href="#accordion02" class="collapsed" data-toggle="collapse" data-parent="#accordion"
                      aria-expanded="false">
                     <i class="fa fa-minus"></i>
                     <h4 class="panel-title"><?= $pregunta->pregunta ?></h4>
                   </a>
                 </div>
                 <div id="accordion02" class="panel-collapse collapse" role="tabpanel" aria-expanded="false"
                      style="height: 0px;">
                   <div class="panel-body">
                    <?= $pregunta->respuesta ?>
                   </div>
                 </div>
               </div>
            
            <?php endforeach; ?>
           <?php endif; ?>
          </div>
        </div>

      </div>

    </div>
  </div>

  <div id="client" class="client-area section-small">
    <div class="container">
      <!-- client carousel -->
      <div class="owl-client">
       <?php if ($empresas != null): ?>
        <?php /** @var \app\models\Empresa[] $empresas */
        foreach ($empresas as $empresa): ?>
          <div class="item active text-center">
          <?= Html::img('@web/'.$empresa->url_logo) ?>
          </div>
        <?php endforeach; ?>
       <?php endif; ?>
      </div>
      <!-- / client carousel -->

    </div>
  </div>

  <section id="contact" class="contact-area section-big">
    <div class="container">

      <div class="row">
        <div class="col-md-12 text-center">
          <div class="section-title">
           <?= Texto::printText('titulo_contacto', 'h2') ?>
           <?= Texto::printText('descripcion_contacto', 'p') ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6" style="overflow: hidden">
          <div class="contact-form" style="overflow: hidden">
           <?php $form = ActiveForm::begin([
            'action' => ['contact']
           ]); ?>
            <div class="form-group in_name">
             <?= $form->field($contacto, 'name')->textInput(['placeholder' => 'Ingrese su nombre'])->label(false) ?>
            </div>
            <div class="form-group in_email">
             <?= $form->field($contacto, 'email')->textInput(['placeholder' => 'Ingrese su email'])->label(false) ?>
            </div>
            <div class="form-group in_message">
             <?= $form->field($contacto, 'body')->textarea(['rows' => 5, 'placeholder' => 'Ingrese su mensaje'])->label(false) ?>
            </div>
            <div class="col-md-12 col-lg-12 form-group">
             <?= Html::submitButton('Enviar mensaje', ['class' => 'btn']) ?>
            </div>
           <?php ActiveForm::end(); ?>
            <div id="form-messages"></div>
          </div>
        </div>

        <div class="col-md-6">

          <div class="address">
           <?= Texto::printText('contacto_titulo', 'h3') ?>
            <div class="address-box clearfix">
             <?= Texto::printText('contacto_texto1', 'p') ?>
            </div>
            <div class="address-box clearfix">
             <?= Texto::printText('contacto_texto2', 'p') ?>
            </div>
            <div class="address-box clearfix">
             <?= Texto::printText('contacto_texto3', 'p') ?>
            </div>
            <div class="address-box clearfix">
             <?= Texto::printText('contacto_texto4', 'p') ?>
            </div>

            <ul class="social-links">
              <li><a href=""><i class="fa fa-facebook"></i></a></li>
              <li><a href=""><i class="fa fa-twitter"></i></a></li>
              <li><a href=""><i class="fa fa-google-plus"></i></a></li>
              <li><a href=""><i class="fa fa-youtube"></i></a></li>
              <li><a href=""><i class="fa fa-pinterest"></i></a></li>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </section>
<?php
if (Yii::$app->user->can(\app\helper\AppHelper::SUPER_ADMIN)) {
 CKEditorAsset::register($this);
 $this->registerJsFile("@web/js/inline-edit.js", [
  'depends' => \yii\web\JqueryAsset::className()
 ]);
} ?>