<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->registerJs('var baseUrl = "' . \yii\helpers\Url::base() . '"', $this::POS_HEAD);
\app\assets\AppsiteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="menu-area navbar-fixed-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="mainmenu">
          <div class="navbar navbar-nobg">
            <div class="navbar-header">
              <a class="navbar-brand hidden-xs" href=""><?= Html::img('@web/theme/img/logo.png') ?></a>
              <a class="navbar-brand show-xs hidden-lg hidden-md hidden-sm"
                 href=""><?= Html::img('@web/theme/img/logo_xs.png') ?></a>
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <div class="navbar-collapse collapse">
              <nav>
                <ul class="nav navbar-nav navbar-right">
                  <li class="active">
                    <?= Html::a('INICIO',
                      (Yii::$app->controller->action->id != 'index') ? ['/site/index/#slider'] : '#slider',
                      ['class' => (Yii::$app->controller->action->id == 'index') ? 'smooth_scroll' : '',
                      ]) ?>
                  </li>
                  <li>
                    <?= Html::a('CARACTERISTICAS',
                      (Yii::$app->controller->action->id != 'index') ? ['/site/index/#service'] : '#service',
                      ['class' => (Yii::$app->controller->action->id == 'index') ? 'smooth_scroll' : '',
                      ]) ?>
                  </li>
                  <li>
                    <?= Html::a('TESTIMONIOS',
                      (Yii::$app->controller->action->id != 'index') ? ['/site/index/#testimonial'] : '#testimonial',
                      ['class' => (Yii::$app->controller->action->id == 'index') ? 'smooth_scroll' : '',
                      ]) ?>
                  </li>
                  <li>
                    <?= Html::a('FAQ',
                      (Yii::$app->controller->action->id != 'index') ? ['/site/index/#faq'] : '#faq',
                      ['class' => (Yii::$app->controller->action->id == 'index') ? 'smooth_scroll' : '',
                      ]) ?>
                  </li>
                  <li>
                    <?= Html::a('CONTACTO',
                      (Yii::$app->controller->action->id != 'index') ? ['/site/index/#contact'] : '#contact',
                      ['class' => (Yii::$app->controller->action->id == 'index') ? 'smooth_scroll' : '',
                      ]) ?>
                  </li>
                  <?php if (Yii::$app->user->isGuest): ?>
                    <li>
                      <a href="<?= \yii\helpers\Url::to('@web/site/login') ?>">INICIAR SESIÓN</a>
                    </li>
                  <?php else: ?>
                    <li>
                      <?= Html::a('Administración', ['dash'], ['class' => '']) ?>
                    </li>
                    <li>
                      <?= Html::beginForm(['/site/logout'], 'post')
                      . Html::submitButton(
                        'CERRAR SESIÓN',
                        ['class' => 'btn btn-link logout']
                      )
                      . Html::endForm() ?>
                    </li>
                  <?php endif; ?>
                </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= Breadcrumbs::widget([
  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?php if ((count(Yii::$app->session->getAllFlashes())) > 0): ?>
  <div class="col-lg-6 col-lg-offset-3">
    <?php foreach (Yii::$app->session->getAllFlashes() as $type => $menssage): ?>
      <div class="alert alert-<?php echo $type ?> fade in">
        <button data-dismiss="alert" class="close" type="button">
          <i class="glyphicon glyphicon-remove"></i>
        </button><?php echo $menssage ?>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

<?= $content ?>
<footer class="footer-area">
  <div class="container">
    <p class="pull-left">&copy; <?= Yii::$app->name ?> <?= date('Y') ?></p>
    <p class="pull-right"> <?= Html::a('Desarrollado por Greensoftw', 'https://www.facebook.com/greensoftw', ['style' => 'color:white', 'target' => '_blank']) ?></p>
  </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
