<?php


namespace app\controllers;


use app\helper\AppHelper;
use app\models\Accionista;
use app\models\DriverPurchase;
use kartik\base\Config;
use kartik\mpdf\Pdf;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rest\Controller;
use app\models\ConfigAcciones;
use yii\web\Response;

class PidCarController extends Controller
{
  public function beforeAction($action)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
    if (Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
      Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST GET');
      Yii::$app->end();
      return true;
    }
    return parent::beforeAction($action);
  }
  
  public function actionConfig(){
    return ArrayHelper::map(ConfigAcciones::find()->all(), 'clave' , 'valor');
  }
  
  public function actionRegister(){
    $config = ConfigAcciones::findOne(['clave' => 'precio']);
    $model = new Accionista([
      'fecha_registro' => date('Y-m-d H:i:s'),
      'reference' => AppHelper::getCodeInvitacion(8)
    ]);
    if ($model->load(Yii::$app->request->post(), '')){
      $find = Accionista::findOne(['documento' => $model->documento, 'tipo_doc' => $model->tipo_doc]);
      if ($find and !$find->pagado){
        $find->delete();
      }
      $model->total_pagar = $config->valor * $model->acciones;
      if ($model->save()){
        //$apiKey = 'C4MledS9W9nShQqfVW0BibTt57';
        $apiKey = '4Vj8eK4rloUd272L48hsrarnUA';
        //$mercadId = '789668';
        $mercadId = '508029';
        //$acoountId = '796527';
        $acoountId = '512321';
        return  [
          'signature' => md5("$apiKey~$mercadId~$model->reference~$model->total_pagar~COP"),
          'email' => $model->correo,
          'amount' => $model->total_pagar,
          'reference' => $model->reference,
          'merchanId' => $mercadId,
          'accountId' => $acoountId,
          'name' => $model->nombres." ".$model->apellidos,
          'url' => 'https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu'
        ];
      }
    }
    return  $model;
  }
  
  public function actionConfirmacion(){
    $reference = \Yii::$app->request->post('reference_sale');
    $state = \Yii::$app->request->post('state_pol');
    $accionista = Accionista::findOne(['reference' => $reference]);
    if ($accionista and $state == '4'){
      $accionista->info_pago = Json::encode($_POST);
      $accionista->pagado = 1;
      $accionista->save();
      
      $mail = Yii::$app->mailer->compose('pago_completo', [
        'accionista' => $accionista
      ])->setSubject("Accionista - PidCar");
      FileHelper::createDirectory('certs');
  
      $css = '
      body{
        background: url("' . Url::to("@web/img/certificado.png", true) . '") top center;
        background-size: 100% 100%
      }';
      $pdf = new Pdf([
        'mode' => Pdf::MODE_CORE,
        'format' => [140, 99],
        'content' => $this->renderPartial('_certificado', [
          'accionista' => $model,
          'config' => ArrayHelper::map(ConfigAcciones::find()->all(),'clave', 'valor')
        ]),
        'cssInline' => $css,
        'cssFile' => Yii::$app->basePath . "/web/css/certificado.css",
        'destination' => Pdf::DEST_FILE,
        'filename' => "certs/$accionista->tipo_doc-$accionista->documento.pdf"
      ]);
      $pdf->render();
      
      $mail->attach($pdf->filename);
      
      $mail->setFrom("pidcar@pidcar.com")
      ->setTo($accionista->correo)
      ->send();
    }
  }
  
  public function actionPagoEstado(){
    $reference = \Yii::$app->request->get('referenceCode');
    return Accionista::findOne(['reference' => $reference]);
  }

  public function actionCertificado($id){
    Yii::$app->response->format = Response::FORMAT_HTML;
    $model = Accionista::findOne($id);
    $css = '
    body{
      background: url("' . Url::to("@web/img/certificado.png", true) . '") top center;
      background-size: 100% 100%
    }';
    $pdf = new Pdf([
      'mode' => Pdf::MODE_CORE,
      'format' => [140, 99],
      'content' => $this->renderPartial('_certificado', [
        'accionista' => $model,
        'config' => ArrayHelper::map(ConfigAcciones::find()->all(),'clave', 'valor')
      ]),
      'cssInline' => $css,
      'cssFile' => Yii::$app->basePath . "/web/css/certificado.css",
    ]);
    return $pdf->render();
  }
}