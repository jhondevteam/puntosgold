<?php

namespace app\controllers;

use app\helper\AppHelper;
use app\models\Afiliado;
use app\models\ContactForm;
use app\models\Empresa;
use app\models\LoginForm;
use app\models\Newpass;
use app\models\Pagina;
use app\models\PreguntasRespuestas;
use app\models\SignUp;
use app\models\Testimonios;
use app\models\Usuario;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\Controller;

class SiteController extends Controller
{
 /**
  * @inheritdoc
  */
 public function behaviors()
 {
  return [
   'access' => [
    'class' => AccessControl::className(),
    'only' => ['logout'],
    'rules' => [
     [
      'actions' => ['logout'],
      'allow' => true,
      'roles' => ['@'],
     ],
    ],
   ],
   'verbs' => [
    'class' => VerbFilter::className(),
    'actions' => [
     'logout' => ['post'],
    ],
   ],
  ];
 }
 
 /**
  * @inheritdoc
  */
 public function actions()
 {
  return [
   'error' => [
    'class' => 'yii\web\ErrorAction',
   ],
   'captcha' => [
    'class' => 'yii\captcha\CaptchaAction',
    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
   ],
  ];
 }
 
 /**
  * Displays homepage.
  *
  * @return string
  */
 public function actionIndex()
 {
  $con = count(Testimonios::find()->all());
  $testimonios = Testimonios::find()->limit(5)->all();
  $preguntas = PreguntasRespuestas::find()->limit(5)->all();
  $empresas = Empresa::find()->activa()->all();
  return $this->render('index', [
   'testimonios' => $testimonios,
   'preguntas' => $preguntas,
   'contacto' => new ContactForm(),
   'empresas'=> $empresas
  ]);
 }
 
 /**
  * Login action.
  *
  * @return string
  */
 public function actionLogin()
 {
  if (!Yii::$app->user->isGuest) {
    return $this->redirect(['dash']);
  }
  
  $model = new LoginForm();
  if ($model->load(Yii::$app->request->post()) && $model->login()) {
   return $this->redirect(['dash']);
  }
  return $this->render('login', [
   'model' => $model,
  ]);
 }

 public function actionDash(){
   if (Yii::$app->user->can(AppHelper::SUPER_ADMIN) or Yii::$app->user->can(AppHelper::ADMIN_ZONA)) {
     return $this->redirect(['/admin/empresa']);
   } elseif (Yii::$app->user->can(AppHelper::ADMIN_EMPRESA))
     return $this->redirect(['/empresa']);

   else {
     Yii::$app->user->logout();
     return $this->goHome();
   }
 }

 public function actionRef($id){
   $afiliado = Afiliado::findOne($id);
   Yii::$app->session->set('code',$afiliado->code_invitacion);
   $this->redirect(['registro']);
 }

 public function actionRegistro(){
   $signUp = new SignUp([
     'coderef'=>Yii::$app->session->get('code')
   ]);
   if ($signUp->load(Yii::$app->request->post())){
     if ($signUp->signUp()){
       Yii::$app->session->setFlash('success','Registrado Correctamente');
       return $this->redirect(['registro-completo']);
     }
   }
   return $this->render('registro',[
     'model'=>$signUp
   ]);
 }

 public function actionRegistroCompleto(){
   return $this->render('registro-completo');
 }
 
 /**
  * Logout action.
  *
  * @return string
  */
 public function actionLogout()
 {
  Yii::$app->user->logout();
  return $this->goHome();
 }
 
 /**
  * Displays contact page.
  *
  * @return string
  */
 public function actionContact()
 {
  $model = new ContactForm();
  if ($model->load(Yii::$app->request->post())) {
   $sendGrid = Yii::$app->mailer;
   $mensaje = $sendGrid->compose('contacto', ['model' => $model]);
   $mensaje->setFrom($model->email);
   $mensaje->setTo('info@puntosgold.com');
   $mensaje->setSubject('CONSULTA WEB - ' . $model->subject)
    ->send($sendGrid);
   Yii::$app->session->setFlash('success', 'Corréo electrónico enviado correctamente');
  }
  return $this->redirect(['index']);
 }
 
 /**
  * Displays about page.
  *
  * @return string
  */
 public function actionContrasena()
 {
  $usuario = new Usuario();
  if ($usuario->load(Yii::$app->request->post())) {
   $usuario = Usuario::find()->where('email = :email', [':email' => $usuario->email])->one();
   if ($usuario != null) {
    $usuario->password_reset_token = Yii::$app->security->generateRandomString();
    $usuario->save(false);
    $sendGrid = Yii::$app->mailer;
    $mensaje = $sendGrid->compose('recordar_contrasena', ['model' => $usuario]);
    $mensaje->setFrom('soporte@puntosdorados.com')
     ->setTo($usuario->email)
     ->setSubject('Recuperación de contraseña Puntos Dorados')
     ->send($sendGrid);
    Yii::$app->session->setFlash('success', 'Se ha enviado un correo a la dirección email ingresada');
   } else {
    $usuario = new Usuario();
    Yii::$app->session->setFlash('warning', 'No se encuentran coincidencias');
   }
   return $this->redirect(['index']);
  }
  
  return $this->render('contrasena', [
   'usuario' => $usuario
  ]);
 }
 
 public function actionRestarpass($token)
 {
  /** @var Usuario $user */
  $user = Usuario::findOne(['password_reset_token' => $token]);
  if ($user != null) {
   $newpass = new Newpass();
   if ($newpass->load(Yii::$app->request->post()) and $newpass->validate()) {
    $user->password = Yii::$app->security->generatePasswordHash($newpass->password);
    $user->password_reset_token = null;
    if ($user->save(false)) {
     Yii::$app->session->setFlash('success', '¡su contraseña ha sido actualizada exitosamente!');
     return $this->redirect(['index']);
    }
   }
   return $this->render('nuevacontrasena', [
    'model' => $newpass
   ]);
  } else
   Yii::$app->session->setFlash('danger', 'el token de recuperación no coincide');
  
  return $this->redirect(['login']);
 }

 public function actionTest(){
   Yii::$app->response->format = 'json';
   $afiliado = Afiliado::findOne(1);
   $token = Json::decode($afiliado->app_token);
   $result = AppHelper::sendNotificationAndroid($token['token'],
     "Test de notificación Test de notificación Test de notificación","Contenido de la notificacion Contenido de la notificacion Contenido de la notificacion Contenido de la notificacion","Sigue comprando y ganando!",'EmpresaPage',"Ver",[
     "empresa_id"=> 1
   ],"http://puntosdorados.com/uploads/empresas-logo/Z3MbtNBD5C.png",true);
   return $result;
 }

 public function actionInit(){
   Yii::$app->response->format = 'json';
   $usuario = new Usuario([
     'documento' => '100000',
     'nombres' => 'Administrador del sistema',
     'estado' => 10,
     'email' => 'greensoftw@gmail.com',
   ]);
   $usuario->setPassword('1');
   $usuario->generateKeys();
   $usuario->save();
   $auth = Yii::$app->authManager;
   $auth->assign($auth->getRole(AppHelper::SUPER_ADMIN),$usuario->id);
   return $usuario;
 }

 public function actionTerminos(){
   $page = Yii::$app->request->get('page', Pagina::TERMINOS_AFILIADOS);
   $terminos = Pagina::findOne(['glue' => $page]);
   return $this->render('terminos',[
     'model'=>$terminos
   ]);
 }

 public function actionPass(){
   $referido = Afiliado::find()
     ->joinWith(['grupos'])
     ->where(['grupo.numero_afiliados' => 0])
     ->orderBy('fecha_registro')
     ->one();
   Yii::$app->response->format = 'json';
   return $referido;
 }

  public function actionTestNew()
  {
    $afiliado = Afiliado::findOne(1);
    return $afiliado->getTotalPuntosConsumidos();
 }
}
