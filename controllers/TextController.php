<?php

namespace app\controllers;

use app\models\Texto;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TextosController implements the CRUD actions for Textos model.
 */
class TextController extends Controller
{
  public $layout = "admin";

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'update' => ['POST'],
        ],
      ],
    ];
  }

  public function actionUpdate($glue)
  {
    $model = Texto::findOne(['glue'=>$glue]);
    if ($model == null){
      $model = new Texto([
        'glue'=>$glue
      ]);
    }
    if ($model->load(Yii::$app->request->post())) {
      if ($model->save(false))
        return $model->text;
      else Yii::info($model->getErrors());
    }
    return false;
  }

  /**
   * Finds the Textos model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Texto the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Texto::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
