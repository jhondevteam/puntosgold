<?php
/**
 * Created by Jhon J Toloza.
 * User: jhon
 * Date: 12/10/17
 * Time: 03:45 PM
 */

namespace app\controllers;


use app\actions\CorteDePuntos;
use yii\web\Controller;

class CronController extends Controller
{

  public function actions()
  {
    return [
      'corte-afiliados'=>CorteDePuntos::className()
    ];
  }
}