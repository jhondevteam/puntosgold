<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppsiteAsset extends AssetBundle
{
 public $basePath = '@webroot';
 public $baseUrl = '@web';
 public $css = [
//        'css/site.css',
  'theme/css/font-awesome.min.css',
  'theme/css/normalize.css',
  'theme/css/owl.carousel.min.css',
  'theme/css/owl.transitions.css',
  'theme/css/owl.transitions.css',
  'theme/css/magnific-popup.css',
  'theme/css/style.css',
  'theme/css/responsive.css',
 ];
 public $js = [
  'theme/js/bootstrap.min.js',
  'theme/js/owl.carousel.min.js',
  'theme/js/jquery.mixitup.js',
  'theme/js/jquery.magnific-popup.min.js',
  'theme/js/jquery.waypoints.min.js',
  'theme/js/jquery.ajaxchimp.min.js',
  'theme/js/main_script.js',
 ];
 public $depends = [
  'yii\web\YiiAsset',
  'yii\bootstrap\BootstrapAsset',
  'yii\web\JqueryAsset',
 ];
}
